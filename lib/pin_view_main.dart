import 'package:flutter/material.dart';
import 'package:ihomework/screens/exercise_exam_alpha_screen.dart';
import 'package:ihomework/widgets/pin_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    int startChar = 'a'.codeUnitAt(0);
    int endChar = 'z'.codeUnitAt(0);
    String s = 'They\'re going to a concert';
    List<AlphaInputItem> inputs = List();
    for(int i = 0; i < s.length; i++)  {
      if(s[i].toLowerCase().codeUnitAt(0) >= startChar && s[i].toLowerCase().codeUnitAt(0) <= endChar) {
        inputs.add(AlphaInputItem(type: AlphaInputType.PROMPT));
      } else {
        inputs.add(AlphaInputItem(type: AlphaInputType.CHARACTER, question: s[i]));
      }
    }
    return Scaffold (
      resizeToAvoidBottomPadding: false,
      body: Container (
        padding: EdgeInsets.all(10.0),
        child: Column (
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Flexible (
              child: Column (
                children: <Widget>[
                  Container(margin: EdgeInsets.symmetric(vertical: 15.0)),
                  Icon(Icons.phonelink_ring, size: 100.0, color: Theme.of(context).primaryColor),
                  Container(margin: EdgeInsets.symmetric(vertical: 15.0)),
                  Container (
                    width: MediaQuery.of(context).size.width * 4/5,
                    child: Text (
                      "Waiting to automatically detect an SMS sent to your mobile number.",
                      textAlign: TextAlign.center,
                      style: TextStyle (
                        fontSize: 17.0,
                        fontWeight: FontWeight.w400,
                        color: Colors.teal
                      ),
                    ),
                  ),
                  Container (
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: PinView (
                      inputs: inputs,
                      autoFocusFirstField: true,
                      submit: (String pin) {
                        showDialog (
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog (
                              title: Text("Pin received successfully."),
                              content: Text("Entered pin is: $pin")
                            );
                          }
                        );
                      } // gets triggered when all the fields are filled
                    ),
                  ),
                ],
              )
            )
          ],
        ),
      )
    );
  }
}
