import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ihomework/screens/exercise_exam_alpha_screen.dart';

class PinView extends StatefulWidget {
  final Function submit;
//  final int count;
  final bool obscureText;
  final bool autoFocusFirstField;
  final bool enabled;
//  final List<int> dashPositions;
  final TextStyle style;
//  final TextStyle dashStyle;
  final InputDecoration inputDecoration;
  final EdgeInsetsGeometry margin;
  final List<AlphaInputItem> inputs; // haimt add
  final Function onTypeCharResult;
  final Function onChangeFocus;

  PinView({
    @required this.submit,
    @required this.onTypeCharResult,
    @required this.onChangeFocus,
//    @required this.count,
    @required this.inputs,
    this.obscureText: false,
    this.autoFocusFirstField: true,
    this.enabled: true,
//    this.dashPositions: const [],
//    this.dashStyle: const TextStyle(fontSize: 30.0, color: Colors.grey),
    this.style: const TextStyle(
      fontSize: 16.0,
      fontWeight: FontWeight.bold,
    ),
    this.inputDecoration: const InputDecoration(border: UnderlineInputBorder()),
    this.margin: const EdgeInsets.all(1.0),
  });

  @override
  _PinViewState createState() => _PinViewState();
}

class _PinViewState extends State<PinView> {
  List<TextEditingController> _controllers;
  List<FocusNode> _focusNodes;
  List<String> _pin;

  int focusDirection = 1;

  @override
  void initState() {
    super.initState();
    _pin = List<String>.generate(widget.inputs.length, (int index) => "");
    _focusNodes = List.generate(widget.inputs.length, (int index) {
      FocusNode fn = FocusNode();
      fn.addListener(() => _onFocusChange(index));
      return fn;
    });
    _controllers = List.generate(widget.inputs.length, (int index) {
      var ctrl = TextEditingController();
      AlphaInputItem input = widget.inputs[index];
      if(input.type == AlphaInputType.CHARACTER) {
        ctrl.text = input.question;
        _pin[index] = input.question;
      }
      return ctrl;
    });
  }

  void _onFocusChange(int index) {
    AlphaInputItem input = widget.inputs[index];
    if(input.type == AlphaInputType.CHARACTER) {
      if((index + 1) < _controllers.length && focusDirection == 1) {
        FocusScope.of(context).requestFocus(_focusNodes[index + 1]);
      } else if((index == _controllers.length - 1) || (index != 0 && focusDirection == 2)) {
        FocusScope.of(context).requestFocus(_focusNodes[index - 1]);
      }
    }
  }

  bool isValidAnswer(int index, String answerChar, AlphaInputItem input) {
    bool result = answerChar.toLowerCase() == input.question.toLowerCase();
    widget.onTypeCharResult(index, result);
    return result;
  }

  void focusNext(int index) {
    if (index < _focusNodes.length - 1) {
      _focusNodes[index].unfocus();
      FocusScope.of(context).requestFocus(_focusNodes[index + 1]);
      focusDirection = 1;
    }
    widget.onChangeFocus();
  }

  void focusPrev(int index) {
    if (index > 0) {
      _focusNodes[index].unfocus();
      FocusScope.of(context).requestFocus(_focusNodes[index - 1]);
      focusDirection = 2;
    }
    widget.onChangeFocus();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> pins = List<Widget>(widget.inputs.length);

    for(int index = 0; index < widget.inputs.length; index++) {
      AlphaInputItem input = widget.inputs[index];
      pins[index] = _singlePinView(index, input);
    }

    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: pins,
      ),
//      child: Wrap(
//        direction: Axis.horizontal,
//        alignment:  WrapAlignment.center,
//        crossAxisAlignment: WrapCrossAlignment.center,
//        children: pins,
//      ),
    );
  }

  Widget _singlePinView(int index, AlphaInputItem input) {
    if(input.type == AlphaInputType.PROMPT) {
      return Container(
        margin: widget.margin,
        width: 18,
        child: TextField(
          enabled: widget.enabled,
          controller: _controllers[index],
          obscureText: widget.obscureText,
          autofocus: widget.autoFocusFirstField ? index == 0 : false,
          focusNode: _focusNodes[index],
          keyboardType: TextInputType.text,
          textAlign: TextAlign.center,
          style: widget.style,
          decoration: widget.inputDecoration,
          textInputAction: TextInputAction.next,
          textCapitalization: index == 0 ? TextCapitalization.sentences: TextCapitalization.none,
          onChanged: (String val) {
            String answerChar;
            _controllers[index].text = '';
            if (val.length == 1) {
              answerChar = val;
              if(isValidAnswer(index, answerChar, input)) {
                _controllers[index].text = answerChar;
                _pin[index] = answerChar;
                focusNext(index);
              } else {
                _controllers[index].text = '';
                _pin[index] = '';
              }
            } else if (val.length == 2) {
              answerChar = val.substring(1, 2);
              if(isValidAnswer(index, answerChar, input)) {
                _controllers[index].text = answerChar;
                _pin[index] = answerChar;
                focusNext(index);
              } else {
                _controllers[index].text = '';
                _pin[index] = '';
              }
            } else {
              _controllers[index].text = '';
              _pin[index] = "";
              focusPrev(index);
            }

            if (_pin.indexOf("") == -1) {
              widget.submit(_pin.join());
            }
          },
        ),
      );
    } else if(input.type == AlphaInputType.CHARACTER) {
      return Container(
        margin: widget.margin,
        width: 18,
        child: TextField(
          enabled: false,
          controller: _controllers[index],
          obscureText: widget.obscureText,
          autofocus: widget.autoFocusFirstField ? index == 0 : false,
          focusNode: _focusNodes[index],
          keyboardType: TextInputType.number,
          textAlign: TextAlign.center,
          style: widget.style,
          decoration: null,
          textInputAction: TextInputAction.next,
          onChanged: (String val) {
            _controllers[index].text = input.question;
          },
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  void dispose() {
    for (TextEditingController ctrl in _controllers) {
      ctrl.dispose();
    }
    for (FocusNode fn in _focusNodes) {
      fn.dispose();
    }
    super.dispose();
  }
}
