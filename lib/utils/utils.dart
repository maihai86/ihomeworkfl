import 'dart:math';

import 'package:flutter/material.dart';
import 'package:ihomework/screens/exercise_exam_page_screen.dart';
import 'package:intl/intl.dart';

class RouteConstants {
  static const HOME_SCREEN = "/HomeScreen";
  static const LOGIN_SCREEN = "/LoginScreen";
}

/// 100% - FF
/// 95% - F2
/// 90% - E6
/// 85% - D9
/// 80% - CC
/// 75% - BF
/// 70% - B3
/// 65% - A6
/// 60% - 99
/// 55% - 8C
/// 50% - 80
/// 45% - 73
/// 40% - 66
/// 35% - 59
/// 30% - 4D
/// 25% - 40
/// 20% - 33
/// 15% - 26
/// 10% - 1A
/// 5% - 0D
/// 0% - 00
class MyColors {
  MyColors._();

  static final Color colorPrimary = hexToColor("#293377");
  static final Color colorPrimaryDark = hexToColor("#303F9F");
  static final Color colorAccent = hexToColor("#FF4081");
  static final Color blackOverlay = hexToColor("#66000000");
  static final Color mainBgColor = hexToColor("#ffffff");
  static final Color redCircle = hexToColor("#d9534f");
  static final Color yellowCircle = hexToColor("#f0ad4e");
  static final Color greenCircle = hexToColor("#5cb85c");
  static final Color divider = hexToColor("#DCDCDC");
  static final Color titleBackground = hexToColor("#DCDCDC");
  static final Color greenSound = hexToColor("#00D5A6");
  static final Color yellowText = hexToColor("#F9F871");
  static final Color selectedTile = hexToColor("#FFB95B");
  static final Color darkGray = hexToColor("#A9A9A9");

  static Color hexToColor(String code) {
      return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
}

class MyDimensions {
  MyDimensions._();

  static const double splashLogoSize = 120;
}

class PrefsKey {
  static const String KEY_ACCESS_TOKEN = 'accessToken';
  static const String KEY_REMEMBER_ME = 'rememberMe';
  static const String KEY_FIREBASE_DEVICE_TOKEN = 'deviceToken';
}

class Constants {
  static const String TATA_ENGLISH = 'Tata English';
  static const String FEEDBACK_URL = 'https://docs.google.com/forms/d/e/1FAIpQLSfiqpf84VIPy_b6RAdo-Zx8FGvWYTH8bfWKLwNfCRTacR_f3A/viewform?c=0&w=1';
  static const int ANSWER_DELAY = 1000;
  static const int INCORRECT_ANSWER_MAX = 3;
  static const List<String> PASS_EXERCISE_GIF_URLS = [
    'https://ihomework.vn/img/success-exercise/win0.gif',
    'https://ihomework.vn/img/success-exercise/win1.gif',
    'https://ihomework.vn/img/success-exercise/win2.gif',
    'https://ihomework.vn/img/success-exercise/win3.gif',
    'https://ihomework.vn/img/success-exercise/win4.gif',
    'https://ihomework.vn/img/success-exercise/win5.gif',
    'https://ihomework.vn/img/success-exercise/win6.gif',
    'https://ihomework.vn/img/success-exercise/win7.gif',
    'https://ihomework.vn/img/success-exercise/win8.gif',
  ];
  static const List<String> FAILED_EXERCISE_GIF_URLS = [
    'https://ihomework.vn/img/failed-exercise/fail1.gif',
    'https://ihomework.vn/img/failed-exercise/fail2.gif',
    'https://ihomework.vn/img/failed-exercise/fail3.gif',
    'https://ihomework.vn/img/failed-exercise/fail4.gif',
  ];
  static const List<String> FAIL_TUNE_URLS = [
    'http://media.tataenglish.com/sounds/fail_tune.mp3',
  ];
  static const List<String> WIN_TUNE_URLS = [
    'http://media.tataenglish.com/sounds/win_tune.mp3',
  ];
  static const List<String> INCORRECT_URLS = [
    'http://media.tataenglish.com/sounds/incorrect.mp3',
  ];
  static const List<String> CORRECT_URLS = [
    'http://media.tataenglish.com/sounds/correct.mp3',
  ];
  static const int DEFAULT_COUNTDOWN_MILLIS = 100;

  static const List<String> PREFIXES = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"];
}

class StageType {
  static const String ONE = 'One';
  static const String ALL = 'All';
}

class QuestionType {
  static const String ALPHA = 'Alpha';
  static const String PRONUNCIATION = 'Pronunciation';
  static const String TYPING = 'Typing';
  static const String SPEAKING = 'Speaking';
  static const String SPEAKING_LONG = 'SpeakingLong';
  static const String OPTION = 'Option';
  static const String MAPPING = 'mapping';
  static const String TYPING_SUB = 'Typing_Sub';
  static const String NINJA = 'ninja';
  static const String GAME = 'Game';
}

class Utils {
  static void snackBar(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(
      SnackBar(content: Text(message))
    );
  }

  static bool isEmptyString(String s) {
    return s == null || s.isEmpty;
  }

  static String normalizeAnswer(String src) {
    if(src == null) return '';

    String res = src.trim();
    while(res.contains('  ')) {
      res = res.replaceAll('  ', ' ');
    }
    res = res.replaceAll('.', '');
    res = res.replaceAll('!', '');
    res = res.replaceAll('?', '');
    while(res.contains('\\\\')) {
      res = res.replaceAll('\\\\', '\\');
    }
    return res;
  }

  static String normalizeAnswerTypingSub(String src) {
    if(src == null) return '';

    String res = normalizeAnswer(src);
    while(res.contains('\'')) {
      res = res.replaceAll('\'', '');
    }
    res = res.replaceAll('‘', '');
    res = res.replaceAll('’', '');
    res = res.replaceAll('"', '');
    res = res.replaceAll('“', '');
    res = res.replaceAll('”', '');
    res = res.replaceAll(',', '');
    return res;
  }

  static String normalizeTranscription(String src) {
    if(src == null) return '';

    String res = src.trim();
    while(res.contains('  ')) {
      res = res.replaceAll('  ', ' ');
    }
    res = res.replaceAll('.', '');
    return res;
  }

  static String normalizeQuestion(String src) {
    if(src == null) return '';

    String res = src.trim();
    while(res.contains('\\n')) {
      res = res.replaceAll('\\n', '\n');
    }
    return res;
  }

  static Random rnd = new Random(DateTime.now().millisecondsSinceEpoch);

  static String getRandomPassExerciseGifUrl() {
    return Constants.PASS_EXERCISE_GIF_URLS[rnd.nextInt(Constants.PASS_EXERCISE_GIF_URLS.length)];
  }

  static String getRandomFailedExerciseGifUrl() {
    return Constants.FAILED_EXERCISE_GIF_URLS[rnd.nextInt(Constants.FAILED_EXERCISE_GIF_URLS.length)];
  }

  static bool isImageUrl(String s) {
    return s != null && s.startsWith('http');
  }

  static final DateFormat dateFormat1 = DateFormat("yyyy-MM-dd HH:mm:ss");

  static String getDateOfWeekString(int weekday) {
    switch(weekday) {
      case 1: return "Thứ 2";
      case 2: return "Thứ 3";
      case 3: return "Thứ 4";
      case 4: return "Thứ 5";
      case 5: return "Thứ 6";
      case 6: return "Thứ 7";
      case 7: return "Chủ nhật";
      default: return "";
    }
  }
}