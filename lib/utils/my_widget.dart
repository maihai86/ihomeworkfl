import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/access_token.dart';

class CommonWidget {
  static final Widget tinyVerticalDivider = TransparentVerticalDivider(width: 5.0,);
  static final Widget smallVerticalDivider = TransparentVerticalDivider(width: 10.0,);
  static final Widget mediumVerticalDivider = TransparentVerticalDivider(width: 15.0,);
  static final Widget largeVerticalDivider = TransparentVerticalDivider(width: 30.0,);
  static final Widget smallHorizontalDivider = TransparentHorizontalDivider(height: 10.0,);
  static final Widget tinyHorizontalDivider = TransparentHorizontalDivider(height: 5.0,);
  static final Widget mediumHorizontalDivider = TransparentHorizontalDivider(height: 15.0,);
  static final Widget largeHorizontalDivider = TransparentHorizontalDivider(height: 20.0,);
  static final Widget hugeHorizontalDivider = TransparentHorizontalDivider(height: 30.0,);
  static final Widget appLogo = SizedBox(
    child: Image.asset('images/kien_tri.png'),
    width: 120,
    height: 120,
  );
  static Widget appBar(AccessToken accessToken) {
    return AppBar(
      title: Text(accessToken == null ? '' : accessToken.name.trim()),
    );
  }
  static final Widget emptyButtonContainer = Container(
    width: 60,
    height: 60,
  );
  static final Widget emptyButtonContainer2 = Container(
    width: 48,
    height: 48,
  );
  static Widget emptyScreenWithNotice(String notice) {
    return Center(
      child: Text(notice,
        style: TextStyle(
          fontSize: 24,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
  static Widget noExerciseContainer = const Center(
    child: const Text('Không có bài tập nào!',
      style: TextStyle(
        fontSize: 24,
      ),
      textAlign: TextAlign.center,
    ),
  );

  static Widget faWarningIcon = const Icon(FontAwesome.warning,
    color: Colors.orange,
    size: 60,
  );
}

class TransparentHorizontalDivider extends StatelessWidget {
  final height;

  TransparentHorizontalDivider({Key key, this.height}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Colors.transparent,
      height: height,
    );
  }
}

class TransparentVerticalDivider extends StatelessWidget {
  final width;

  TransparentVerticalDivider({Key key, this.width}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return VerticalDivider(
      color: Colors.transparent,
      width: width,
    );
  }
}