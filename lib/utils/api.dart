import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'dart:async';
import 'dart:convert';

import 'package:ihomework/domain/access_token.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/utils/custom_exception.dart';

class Api {
//  static const String URL_PREFIX  = 'http://103.28.37.107:81/hung/frontend/web/api';
//  static const String URL_PREFIX2 = 'http://103.28.37.107:81/hung/frontend/web/api-protected';
  static const String URL_PREFIX  = 'http://tataenglish.com/api';
  static const String URL_PREFIX2 = 'http://tataenglish.com/api-protected';

  static Future<AccessToken> postLogin(String username, String password, String deviceToken) async {
    print('Request body: ${
        {
          'username': username,
          'password': password,
          'device_token': deviceToken
        }
    }');
    final response = await http.post(URL_PREFIX + '/app-login',
        body: {
          'username': username,
          'password': password,
          'device_token': deviceToken
        }
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return AccessToken.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to login');
    }
  }

  static Future<ChangePassword> postLogout(String accessToken) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
    };

    final response = await http.post('$URL_PREFIX2/logout', headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return ChangePassword.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to logout');
    }
  }

  static Future<StudentStat> getListTest(String accessToken, int page, int menuId) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    print('Request URL: ' + '$URL_PREFIX2/app-list-test?menu_id=$menuId&page=$page');
    if(page == null) page = 1;
    final response = await http.get('$URL_PREFIX2/app-list-test?menu_id=$menuId&page=$page',
      headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body page $page : ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return StudentStat.fromJson(json.decode(response.body));
//      response.body.split('\n').forEach((word) => print(" " + word));
//      return null;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to getListTest');
    }
  }

  static Future<StudentStat> getGoldenWeekListTest(String accessToken, int page) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    if(page == null) page = 1;
    final response = await http.get('$URL_PREFIX2/app-list-test?menu_id=6&page=$page',
      headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body page $page : ${response.body}');

    if (response.statusCode == 200) {
      return StudentStat.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to getListTest');
    }
  }

  static Future<List<Part>> getTestDetail(String accessToken, int testId) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    print('Request URL: ' + '$URL_PREFIX2/app-get-test-exercise?test_id=$testId');
    final response = await http.get('$URL_PREFIX2/app-get-test-exercise?test_id=$testId',
      headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return (json.decode(response.body) as List).map((item) => Part.fromJson(item)).toList();
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to getTestDetail');
    }
  }

  static Future<Stage> getStageDetail(String accessToken, int exerciseId) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    print('Request URL: ' + '$URL_PREFIX2/app-do-exercise?exercise_id=$exerciseId');
    final response = await http.get('$URL_PREFIX2/app-do-exercise?exercise_id=$exerciseId',
//    final response = await http.get('$URL_PREFIX2/app-do-exercise?stage_id=235270___6', // haimt typing all
      headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return Stage.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to getStageDetail');
    }
  }

  static Future<Stage> getStageDetailByStageExp(String accessToken, String stageIdExp) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();
    if(Utils.isEmptyString(stageIdExp)) throw Exception('Thông tin stage bị trống!');

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    print('Request URL: ' + '$URL_PREFIX2/app-do-exercise?$stageIdExp');
    final response = await http.get('$URL_PREFIX2/app-do-exercise?$stageIdExp',
      headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return Stage.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to getStageDetail');
    }
  }

  static Future<DoExerciseResult> postSubmitExerciseExamResult(String accessToken, DoExerciseResultDto requestBody) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    print('Request body: ${jsonEncode(requestBody)}');
    final response = await http.post('$URL_PREFIX2/app-save-do-exercise-result',
      headers: headers, body: jsonEncode(requestBody));
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return DoExerciseResult.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to postSubmitExerciseExamResult');
    }
  }

  static Future<ChangePassword> postChangePassword(String accessToken, String currentPassword, String newPassword, String repeatPassword) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
    };

    final response = await http.post('$URL_PREFIX2/change-password',
        headers: headers, body: {'current_password': currentPassword, 'new_password': newPassword, 'repeat_new_password': repeatPassword}
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return ChangePassword.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to change password');
    }
  }

  static Future<String> postUploadRecordedAudio(String filePath) async {
    var request = http.MultipartRequest('POST', Uri.parse('http://speech.tataenglish.com/upload'));
    request.files.add(await http.MultipartFile.fromPath('file', filePath, contentType: MediaType.parse('multipart/form-data')));
    final streamedResponse = await request.send();
    final response = await http.Response.fromStream(streamedResponse);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return response.body;
    } else if(response.statusCode == 403 && response.body != null && response.body == 'UnknownValueError') {
      return '';
    } else {
      throw Exception('Failed to postUploadRecordedAudio');
    }
  }

  static Future<List<Schedule>> getSchedules(String accessToken) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    print('Request URL: ' + '$URL_PREFIX2/app-schedule');
    final response = await http.get('$URL_PREFIX2/app-schedule',
        headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return response.body == null ? List() : (json.decode(response.body) as List).map((item) => Schedule.fromJson(item)).toList();
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to getTestDetail');
    }
  }

  static Future<ScheduleLeftResult> getLeaveSchedule(String accessToken, int planId) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    print('Request URL: ' + '$URL_PREFIX2/app-leave-schedule?plan_id=${planId}');
    final response = await http.get('$URL_PREFIX2/app-leave-schedule?plan_id=${planId}',
        headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return ScheduleLeftResult.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to getTestDetail');
    }
  }

  static Future<Introduce> getIntroduces(String accessToken) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    String url = '$URL_PREFIX2/app-user-introduce-index';
    print('Request URL: $url');
    final response = await http.get(url, headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return Introduce.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to getTestDetail');
    }
  }

  /**
   * vi du:
   * {
      "full_name": "Mai Tien Hai",
      "phone_number": "0987654321",
      "center_target": 1,
      "job": 1,
      "school": "HVTC",
      "live_area": "Hoai Duc",
      "introduced-text": "học ở tât rất tốt",
      "acceptable": "có",
      "trouble": "phát âm",
      "percent-regis": "rất phù hợp"
      }
   */
  static Future<SubmitIntroduceResult> postSubmitIntroduce(String accessToken, SubmitIntroduceDto requestBody) async {
    if(Utils.isEmptyString(accessToken)) throw NotLoggedInException();

    Map<String, String> headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json',
    };

    print('Request body: ${jsonEncode(requestBody)}');
    final response = await http.post('$URL_PREFIX2/app-submit-introduce',
        headers: headers, body: jsonEncode(requestBody));
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return SubmitIntroduceResult.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to postSubmitIntroduce');
    }
  }
}