import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/widgets/pin_view.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_exam_state.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamAlphaScreen extends AbstractExerciseExamScreen {

//  final Stage stage;
//
//  final int questionIdx;
//
//  final Question question;
//
//  final int Function() incorrectQuestionCount;

//  final void Function(int questionIdx, bool correctAnswer) nextQuestion;
//
//  final void Function(String url) playSoundUrl;
//
//	final void Function(List<String> urls, int soundIndex) playSoundUrls;
//
//  final void Function(String url) playSmallSoundUrl;
//
//  final void Function() stopSoundUrl;
//
//  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

  ExerciseExamAlphaScreen({
    key,
    stage,
    questionIdx,
    question,
    incorrectQuestionCount,
    nextQuestion,
    playSoundUrl,
    playSoundUrls,
    playSmallSoundUrl,
    stopSoundUrl,
    updateCorrectAnswer,
  }): /*assert(stage != null),*/
//      assert(questionIdx != null),
//      assert(question != null),
//      assert(nextQuestion != null),
//      assert(playSoundUrl != null),
//      assert(playSoundUrls != null),
//      assert(playSmallSoundUrl != null),
//      assert(stopSoundUrl != null),
//      assert(updateCorrectAnswer != null),
      super(
        key: key,
        stage: stage,
        questionIdx: questionIdx,
        question: question,
        incorrectQuestionCount: incorrectQuestionCount,
        nextQuestion: nextQuestion,
        playSoundUrl: playSoundUrl,
        playSoundUrls: playSoundUrls,
        playSmallSoundUrl: playSmallSoundUrl,
        stopSoundUrl: stopSoundUrl,
        updateCorrectAnswer: updateCorrectAnswer,);

  @override
  State<StatefulWidget> createState() => ExerciseExamAlphaScreenState();

}

class ExerciseExamAlphaScreenState extends AbstractExerciseScreenState<ExerciseExamAlphaScreen> {

  static const int ANSWER_NOT_RECORDED = 0;
  static const int ANSWER_INCORRECT = 1;
  static const int ANSWER_CORRECT = 2;
  final int _fStartChar = 'a'.codeUnitAt(0);
  final int _fEndChar = 'z'.codeUnitAt(0);

  List<AlphaInputItem> inputs;

  int correctAnswer = ANSWER_NOT_RECORDED;
  int incorrectAnswerCount = 0;
  int _incorrectCharIndex = -1;

  bool _showHint = false;
  Timer _timer;

  int nextQuestionCd;
  bool showNextQuestionCountDown = false;

  var _enablePin = true;

  TextEditingController _controller;
  int typeIndex = 0;

  TextInputFormatter _textInputFormatter;
  ///
  
  @override
  void initState() {
    super.initState();

    nextQuestionCd = 0;
    _playQuestionAudio();
    _buildPinViewInput();
  }


  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();
    _controller.dispose();

    super.dispose();
  }

  @override
  bool shouldInitFCM() {
    return false;
  }

  void _buildPinViewInput() {
    String question = widget.question.fullQuestion.trim();
    inputs = List();
    for(int i = 0; i < question.length; i++)  {
      if(question[i].toLowerCase().codeUnitAt(0) >= _fStartChar && question[i].toLowerCase().codeUnitAt(0) <= _fEndChar) {
        inputs.add(AlphaInputItem(type: AlphaInputType.PROMPT, question: '_', result: question[i]));
      } else {
        inputs.add(AlphaInputItem(type: AlphaInputType.CHARACTER, question: question[i], result: question[i]));
      }
    }
    // haimt new solution
		_controller = TextEditingController(text: getRemainingQuestionPattern());
    _controller.addListener(() {
      _controller.selection = TextSelection.fromPosition(TextPosition(offset: typeIndex));
    });
    _textInputFormatter = MyTextInputFormatter(
      exerciseExamAlphaScreenState: this,
    );
  }

  String getRemainingQuestionPattern() {
    return inputs.map((item) => item.question).join();
  }

  void onTypeCharCorrectly(String answerChar) {
    this.inputs[typeIndex].question = answerChar;
    this.typeIndex++;
    // type dung
    if(_showHint) {
      setState(() {
        _showHint = false;
        this.correctAnswer = ANSWER_CORRECT;
      });
    } else {
      setState(() {
        this.correctAnswer = ANSWER_CORRECT;
      });
    }
    if(typeIndex < inputs.length) {
      if(inputs[typeIndex].type == AlphaInputType.CHARACTER) {
        this.inputs[typeIndex].question = this.inputs[typeIndex].result;
        this.typeIndex++;
      }
    }
    if(typeIndex >= inputs.length - 1) {
      nextQuestionCountDown();
      widget.updateCorrectAnswer(widget.questionIdx, true);
      widget.playSmallSoundUrl(Constants.CORRECT_URLS[0]);
    }
  }

  void onTypeCharIncorrectly(String answerChar) {
    if (typeIndex != _incorrectCharIndex) {
      incorrectAnswerCount = 1;
      _incorrectCharIndex = typeIndex;
    } else {
      incorrectAnswerCount++;
    }
    if (incorrectAnswerCount >= Constants.INCORRECT_ANSWER_MAX) {
      setState(() {
        _showHint = true;
        this.correctAnswer = ANSWER_INCORRECT;
      });
    } else {
      setState(() {
        this.correctAnswer = ANSWER_INCORRECT;
      });
    }
    widget.updateCorrectAnswer(widget.questionIdx, false);
    widget.playSmallSoundUrl(Constants.INCORRECT_URLS[0]);
  }

  Widget _buildPlayQuestionAudioButton() {
    if(widget.question == null || widget.question.questionAudio.isEmpty) {
      return CommonWidget.emptyButtonContainer;
    } else if(playSoundUrlState == PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_up,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () async {
          await widget.stopSoundUrl();
        },
      );
    } else if(playSoundUrlState != PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_off,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () {
          _playQuestionAudio();
        },
      );
    } else {
      return CommonWidget.emptyButtonContainer;
    }
  }

  Widget _buildPrecisionIcon() {
    if(correctAnswer == ANSWER_CORRECT) {
      // haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.check,
            color: Colors.green,
            size: 68,
          ),
        ],
      );
    } else if(correctAnswer == ANSWER_INCORRECT) {
      // haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.close,
            color: Colors.red,
            size: 68,
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  void _playQuestionAudio() {
    if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
      widget.playSoundUrls(widget.question.questionAudio, 0);
    }
  }

  void nextQuestionCountDown() {
    if(_timer != null) {
      _timer.cancel();
    }
    _timer = Timer(Duration(/*seconds: 1*/milliseconds: Constants.DEFAULT_COUNTDOWN_MILLIS), () {
      setState(() {
        showNextQuestionCountDown = true;
        nextQuestionCd += Constants.DEFAULT_COUNTDOWN_MILLIS;
        _enablePin = false;
        this.correctAnswer = ANSWER_CORRECT;
      });
      if(nextQuestionCd >= widget.question.time * 1000) {
        widget.stopSoundUrl().then((result) => widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT));
      } else {
        nextQuestionCountDown();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
//    print('$runtimeType.build');
    final indicatorValue = (widget.questionIdx + 1) / widget.stage.questions.length;
    return Container(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            /// header section
            Row(
              children: <Widget>[
                Container(
                  alignment: Alignment(0, 0),
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: MyColors.colorPrimary
                  ),
                  child: Text('${widget.questionIdx + 1}',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 16
                    ),
                  ),
                ),
                CommonWidget.smallVerticalDivider,
                Expanded(
                  child: LinearProgressIndicator(
                    backgroundColor: MyColors.colorPrimaryDark,
                    value: indicatorValue,
                  ),
                ),
                CommonWidget.smallVerticalDivider,
                Text('${widget.stage.questions.length - widget.incorrectQuestionCount()}/${widget.stage.questions.length}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            /// END header section
            CommonWidget.mediumHorizontalDivider,
            /// question section
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(FontAwesome.keyboard_o,
                            color: MyColors.colorPrimary,
                            size: 30,
                          ),
                          CommonWidget.smallVerticalDivider,
                          Flexible(
                            child: Text(widget.question.questionPicture,
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                              overflow: TextOverflow.visible,
                              softWrap: true,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                CommonWidget.smallVerticalDivider,
                _buildPlayQuestionAudioButton(),
              ],
            ),
            /// END question section
            /// manipulation section
            Expanded(
              child: Padding(padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
//                    PinView (
//                      inputs: _inputs,
//                      autoFocusFirstField: true,
//                      enabled: _enablePin,
//                      submit: (String answer) {
//                        print('$runtimeType, submit: $answer');
//                        // tra loi dung
//                        nextQuestionCountDown();
//                        widget.updateCorrectAnswer(widget.questionIdx);
//                      }, // gets triggered when all the fields are filled
//                      onTypeCharResult: (int index, bool result) {
//                        if (!result) {
//                          if (index != _currIndex) {
//                            incorrectAnswerCount = 1;
//                            _currIndex = index;
//                          } else {
//                            incorrectAnswerCount++;
//                          }
//                          if (incorrectAnswerCount >= Constants.INCORRECT_ANSWER_MAX) {
//                            setState(() {
//                              _showHint = true;
//                              this.correctAnswer = result ? ANSWER_CORRECT : ANSWER_INCORRECT;
//                            });
//                          } else {
//                            setState(() {
//                              this.correctAnswer = result ? ANSWER_CORRECT : ANSWER_INCORRECT;
//                            });
//                          }
//                        } else {
//                          // type dung
//                          if(_showHint) {
//                            setState(() {
//                              _showHint = false;
//                              this.correctAnswer = result ? ANSWER_CORRECT : ANSWER_INCORRECT;
//                            });
//                          } else {
//                            setState(() {
//                              this.correctAnswer = result ? ANSWER_CORRECT : ANSWER_INCORRECT;
//                            });
//                          }
//                        }
//                      },
//                      onChangeFocus: () {
//                        setState(() {
//                          this.correctAnswer = ANSWER_NOT_RECORDED;
//                        });
//                      }
//                    ),
										TextField(
											controller: _controller,
											autofocus: true,
											enabled: _enablePin,
											keyboardType: TextInputType.multiline,
											maxLines: 3,
											style: TextStyle(
                        color: Theme.of(context).textTheme.body1.color,
												fontSize: 22.0,
												fontWeight: FontWeight.bold,
                        letterSpacing: 2.0,
											),
											decoration: InputDecoration(
                        border: InputBorder.none,
                      ),
											textCapitalization: TextCapitalization.sentences,
                      inputFormatters: [
                        _textInputFormatter,
                      ],
										),
                    CommonWidget.smallHorizontalDivider,
                    _showHint ?
                    RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(
                        style: DefaultTextStyle.of(context).style,
                        children: [
                          TextSpan(
                            text: 'Gợi ý: ',
                          ),
                          TextSpan(
                            text: inputs[_incorrectCharIndex].result,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 20,
                            ),
                          ),
                        ]
                      ),
                    ) : Text('',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    CommonWidget.largeHorizontalDivider,
                    _buildPrecisionIcon(),
                  ],
                ),
              )
            ),
            /// END manipulation section
            /// bottom section
            showNextQuestionCountDown ?
            Container(
              padding: EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    width: 60.0,
                    height: 60.0,
                    child: CircularProgressIndicator(
                      backgroundColor: MyColors.colorPrimary,
                      valueColor: AlwaysStoppedAnimation<Color>(MyColors.colorAccent),
                      value: nextQuestionCd / (widget.question.time * 1000),
                      strokeWidth: 6.0,
                    ),
                  ),
                ],
              )
            ) : Container(),
            /// END bottom section
          ],
        ),
      ),
    );
  }
}

class MyTextInputFormatter extends TextInputFormatter {

  final ExerciseExamAlphaScreenState exerciseExamAlphaScreenState;

  MyTextInputFormatter({
    this.exerciseExamAlphaScreenState,
  }): assert(exerciseExamAlphaScreenState != null),
      super();

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if(newValue.text != null && newValue.text.length > 0) {
      String typedChar = newValue.text.substring(exerciseExamAlphaScreenState.typeIndex, exerciseExamAlphaScreenState.typeIndex + 1);
//      print('$runtimeType, formatEditUpdate oldValue:${oldValue.text}, newValue:${newValue.text}, typedChar:$typedChar, question:${exerciseExamAlphaScreenState.getRemainingQuestionPattern()}, typeIndex:${exerciseExamAlphaScreenState.typeIndex}');
      if (isValidAnswer(exerciseExamAlphaScreenState.typeIndex, typedChar)) {
        exerciseExamAlphaScreenState.onTypeCharCorrectly(typedChar);
//        print('$runtimeType, formatEditUpdate AFTER UPDATE question:${exerciseExamAlphaScreenState.getRemainingQuestionPattern()}, typeIndex:${exerciseExamAlphaScreenState.typeIndex}');
        return TextEditingValue(
          text: exerciseExamAlphaScreenState.getRemainingQuestionPattern(),
          selection: newValue.selection,
        );
      } else {
        exerciseExamAlphaScreenState.onTypeCharIncorrectly(typedChar);
      }
    }
    return oldValue;
  }

    bool isValidAnswer(int index, String answerChar) {
//      print('$runtimeType, isValidAnswer, answerChar:$answerChar, result:${exerciseExamAlphaScreenState.inputs[index].result.toLowerCase()}');
      bool result = answerChar.toLowerCase() == exerciseExamAlphaScreenState.inputs[index].result.toLowerCase();
      return result;
    }
}

enum AlphaInputType {
  CHARACTER, PROMPT
}

class AlphaInputItem {
  AlphaInputType type;
  String question;
  String result;

  AlphaInputItem({this.type, this.question, this.result})
    : assert(type != null),
      assert(type == AlphaInputType.CHARACTER ? question != null && question.isNotEmpty : true),
      assert(result != null);

  @override
  String toString() {
    return '{type:$type, question:$question, result:$result}';
  }
}