import 'package:flutter/material.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/widgets/circle_painter.dart';

import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseDetailScreen extends StatefulWidget {


  ExerciseDetailScreen({this.test, this.testDisplay}) :
        assert(test != null);

  final Test test;
  final testDisplay;

  @override
  _ExerciseDetailScreenState createState() => _ExerciseDetailScreenState();

}

/// @ref https://flutter.dev/docs/cookbook/lists/mixed-list
class _ExerciseDetailScreenState extends AbstractState<ExerciseDetailScreen> {
  List<Part> parts;
  int _loaded = 0;
  Map<int, int> _mapPartOrders = Map();
  Map<int, int> _mapExerciseOrders = Map();

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

//    Future.delayed(Duration(milliseconds: 300)).then((_) {
      _getTestDetail();
//    });
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  void _getTestDetail() async {
    setState(() {
      _loaded = 1;
    });
    await Api.getTestDetail(accessToken.accessToken, widget.test.id).then((_parts) {
      setState(() {
        this.parts = _parts;
        _generateOrders();
        _loaded = 2;
      });
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        _loaded = 2;
      });
    });
  }

  void _generateOrders() {
    _mapPartOrders = Map();
    _mapExerciseOrders = Map();
    int i = 1;
    parts.forEach((Part _p) => _mapPartOrders[_p.partId] = i++);
    i = 1;
    parts.forEach((Part _p) {
      _p.exercises.forEach((Exercise _e) => _mapExerciseOrders[_e.exerciseId] = i++);
    });
  }

  List<Widget> _buildExpandableContent(Part part) {
    List<Widget> columnContent = [];
    final length = part.exercises == null ? 0: part.exercises.length;
    for (int index = 0; index < length; index++) {
      final Exercise exercise = part.exercises[index];
      Exercise prevEx;
      if(index > 0) {
				prevEx = part.exercises[index - 1];
			}
      Text text;
      if(exercise.canDo) {
        text = Text('${_mapExerciseOrders[exercise.exerciseId]}. ${exercise.exerciseName}');
      } else {
        text = Text('${_mapExerciseOrders[exercise.exerciseId]}. ${exercise.exerciseName}',
          style: TextStyle(
            color: Colors.grey
          ),
        );
      }
      columnContent.add(
        InkWell(
          onTap: () {
            print('selected $index');
            if(exercise.canDo) {
							Navigator.push(context,
									MaterialPageRoute(
											builder: (context) =>
													ExerciseExamPageScreen(
															ExerciseExamPageScreenState.TYPE_BY_EXERCISE,
															exercise: exercise)));
						} else if(prevEx != null) {
							showDialog(
								context: context,
								builder: (context) => new AlertDialog(
									title: new Text('Thông tin'),
									content: new Text('Xin lỗi bạn chưa làm được bài tập này vì bạn chưa hoàn thành bài:\n${_mapExerciseOrders[prevEx.exerciseId]}. ${prevEx.exerciseName}'),
									actions: <Widget>[
										new FlatButton(
											onPressed: () => Navigator.of(context).pop(false),
											child: new Text('OK'),
										),
									],
								),
							);
						}
          },
          child: Container(
            padding: EdgeInsets.only(top: 16, bottom: 16, left: 10, right: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(child: text,),
                CustomPaint(
                  painter: CirclePainter(
                    color: exercise.exerciseDone ? Colors.green : Colors.red,
                  ),
                  size: Size(16.0, 16.0),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return columnContent;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: CommonWidget.appBar(accessToken),
      body: _loaded == 1 ?
      Center(
        child: CircularProgressIndicator(),
      ) :
      Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: 40,
              decoration: BoxDecoration(
                  color: MyColors.titleBackground
              ),
              alignment: AlignmentDirectional(0, 0),
              child: Text((widget.testDisplay).toUpperCase(),
                textAlign: TextAlign.center,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: parts == null ? 0: parts.length,
                itemBuilder: (context, index) {
                  final part = parts[index];
                  return part == null ?
                  Container() :
                  ExpansionTile(
                    title: Text('Phần ${_mapPartOrders[part.partId]}. ${part.partName}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: MyColors.colorPrimary,
                      ),
                    ),
                    children: _buildExpandableContent(part),
                    initiallyExpanded: true,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

}