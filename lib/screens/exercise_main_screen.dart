import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/access_token.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/screens/schedule_screen.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/utils.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';

import 'exercise_daily_screen.dart';
import 'exercise_detail_screen.dart';
import 'golden_week_screen.dart';
import 'introduce_screen.dart';
import 'login_screen.dart';
import 'base/abstract_state.dart';
import 'change_password_screen.dart';
import 'exercise_daily_speaking_screen.dart';

class ExerciseMainScreen extends StatefulWidget {

  @override
  _ExerciseMainScreenState createState() => _ExerciseMainScreenState();

}

class _ExerciseMainScreenState extends AbstractState<ExerciseMainScreen> {
  int _loaded = 0;
  int selectedMenuIndex = 0;

  static const int MENU_DAILY_EXERCISE = 0;
  static const int MENU_GOLDEN_WEEK = 1;
  static const int MENU_SCHEDULE = 2;
  static const int MENU_INVITE_STUDENT = 3;

  ExerciseDailyScreen exerciseDailyScreen;
//  ExerciseDailySpeakingScreen exerciseDailySpeakingScreen;
//  GoldenWeekScreen goldenWeekScreen;
//  GoldenWeekScreen toeicTrainingScreen;
  ExerciseDailyScreen goldenWeekScreen;
  ScheduleScreen fixtureScreen;
  IntroduceScreen introduceScreen;

  DateTime _currentBackPressTime;

  GlobalKey<State<StatefulWidget>> _exerciseDailyKey;
  GlobalKey<State<StatefulWidget>> _goldenWeekKey;

  Widget getDrawerItemWidget(int pos) {
    switch (pos) {
      case MENU_DAILY_EXERCISE:
        return exerciseDailyScreen;
//        return exerciseDailySpeakingScreen;
      case MENU_GOLDEN_WEEK:
        return goldenWeekScreen;
      case MENU_SCHEDULE:
        return fixtureScreen;
      case MENU_INVITE_STUDENT:
        return introduceScreen;
      default:
        return Text("");
    }
  }

  @override
  void initState() {
    super.initState();

    _exerciseDailyKey = GlobalKey();
    exerciseDailyScreen = ExerciseDailyScreen(
      key: _exerciseDailyKey,
      menuId: 1,
      navigateScheduleScreen: navigateScheduleScreen
    );
//    exerciseDailySpeakingScreen = ExerciseDailySpeakingScreen();
//    goldenWeekScreen = GoldenWeekScreen();
//    toeicTrainingScreen = GoldenWeekScreen();
    _goldenWeekKey = GlobalKey();
    goldenWeekScreen = ExerciseDailyScreen(
      key: _goldenWeekKey,
      menuId: 6
    );
    fixtureScreen = ScheduleScreen();
    introduceScreen = IntroduceScreen();
  }

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    if(accessToken != null) {
      showSnackBar('Chào mừng ${accessToken.name} đã trở lại với iHomework.');
    }
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  void _onSelectItem(int index) {
    print("onSelectItem index: $index");
    Navigator.pop(context); // close the drawer
    if(index <=3) {
      setState(() => selectedMenuIndex = index);
    } else {
    }
  }

  void navigateScheduleScreen() {
    setState(() => selectedMenuIndex = MENU_SCHEDULE);
  }

  void _confirmLogout() {
    Navigator.pop(context);

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Đăng xuất'),
          content: const Text('Bạn có chắc là muốn đăng xuất?'),
          actions: <Widget>[
            FlatButton(
              child: const Text('Không'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: const Text('Đăng xuất'),
              onPressed: () {
                Navigator.pop(context);
                _logout();
              },
            ),
          ],
        );
      }
    );
  }

  Future<void> _logout() async {
    setState(() {
      this._loaded = 1;
    });
    Api.postLogout(accessToken.accessToken).then((_logout) {
      setState(() {
        this._loaded = 2;
      });

      prefs.remove(PrefsKey.KEY_ACCESS_TOKEN).then((result) {
        accessToken = null;

        /// haimt chuyen den login screen
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => LoginScreen()));
      }, onError: (error, stacktrace) {
        showSnackBar(error.toString());
      });
    }, onError: (error, stacktrace) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        this._loaded = 2;
      });
    });
  }

  void _navChangePasswordScreen() async {
    final int changePwResult = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => ChangePasswordScreen()));
    if(changePwResult == 0) {
      showSnackBar('Đổi mật khẩu thành công');
    }
  }

  Future<bool> _onWillPop() {
    DateTime now = DateTime.now();
    if (_currentBackPressTime == null ||
        now.difference(_currentBackPressTime) > Duration(seconds: 2)) {
      _currentBackPressTime = now;
      showSnackBar('Chạm nút "Back" 2 lần để thoát iHomework');
      return Future.value(false);
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    if(_loaded == 1) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
//    final drawerHeader = UserAccountsDrawerHeader(
//      accountName: Text("Tâm Kiên Trì"),
//      accountEmail: Text('tamkientri@gmail.com'),
//    );
    
    final drawerItems = Container(
      color: Colors.white,
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
//        drawerHeader,
          Divider(
            color: Colors.transparent,
            height: 20,
          ),
          ListTile(
            leading: Icon(FontAwesome.book),
            title: Text('Bài tập về nhà'),
            selected: selectedMenuIndex == MENU_DAILY_EXERCISE,
            onTap: () => _onSelectItem(MENU_DAILY_EXERCISE),
          ),
          ListTile(
            leading: Icon(FontAwesome.trophy, color: Colors.orange,),
            title: Text('Tuần lễ vàng',
              style: TextStyle(
                  color: Colors.orange
              ),
            ),
            selected: selectedMenuIndex == MENU_GOLDEN_WEEK,
            onTap: () => _onSelectItem(MENU_GOLDEN_WEEK),
          ),
          ListTile(
            leading: Icon(FontAwesome.calendar),
            title: Text('Lịch học'),
            selected: selectedMenuIndex == MENU_SCHEDULE,
            onTap: () => _onSelectItem(MENU_SCHEDULE),
          ),
          ListTile(
            leading: Icon(FontAwesome.user_plus),
            title: Text('Giới thiệu bạn bè'),
            selected: selectedMenuIndex == MENU_INVITE_STUDENT,
            onTap: () => _onSelectItem(MENU_INVITE_STUDENT),
          ),
//        ListTile(
//          leading: Icon(Icons.folder_open),
//          title: Text('Luyện thi TOEIC'),
//          selected: selectedMenuIndex == 3,
//          onTap: () => _onSelectItem(3),
//        ),
          ListTile(
            leading: Icon(FontAwesome.envelope),
            title: Text('Hòm thư góp ý'),
            onTap: () async {
              Navigator.pop(context);

              launchURL(Constants.FEEDBACK_URL);
            },
          ),
          ListTile(
            leading: Icon(FontAwesome.lock),
            title: Text('Đổi mật khẩu'),
            onTap: () {
              Navigator.pop(context);
              _navChangePasswordScreen();
            },
          ),
          ListTile(
            leading: Icon(FontAwesome.sign_out),
            title: Text('Đăng xuất'),
            onTap: _confirmLogout,
          ),
        ],
      ),
    );
    
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(accessToken == null ? '' : accessToken.name.trim()),
        leading: Container(
          margin: EdgeInsets.all(10),
          child: Image.asset("images/kien_tri.png", width: 24, height: 24,),
        ),
//        actions: <Widget>[
//          IconButton(
//            icon: Icon(Icons.directions_bike),
//            onPressed: () {
//              print("THong bao");
//            },
//          ),
//          IconButton(
//            icon: Icon(Icons.menu),
//            onPressed: () {
//              if(!Scaffold.of(context).isEndDrawerOpen) {
//                Scaffold.of(context).openEndDrawer();
//              }
//            },
//          ),
//        ],
      ),
      endDrawer: Drawer(
        child: drawerItems,
      ),
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: Center(
          child: getDrawerItemWidget(selectedMenuIndex),
        ),
      ),
    );
  }
}