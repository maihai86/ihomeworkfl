import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamSpeakingLongScreen extends AbstractExerciseExamScreen {

//	final Stage stage;
//
//	final int questionIdx;
//
//	final Question question;
//
//	final int Function() incorrectQuestionCount;
//
//	final void Function(int questionIdx, bool correctAnswer) nextQuestion;
//
//	final void Function(String url) playSoundUrl;
//
//	final void Function(List<String> urls, int soundIndex) playSoundUrls;
//
//	final void Function() stopSoundUrl;
//
//	final void Function(int questionIdx, bool correct) updateCorrectAnswer;

	ExerciseExamSpeakingLongScreen({
		key,
		stage,
		questionIdx,
		question,
		incorrectQuestionCount,
		nextQuestion,
		playSoundUrl,
		playSoundUrls,
		playSmallSoundUrl,
		stopSoundUrl,
		updateCorrectAnswer,
	}): /*assert(stage != null),*/
//			assert(questionIdx != null),
//			assert(question != null),
//			assert(nextQuestion != null),
//			assert(playSoundUrl != null),
//			assert(playSoundUrls != null),
//			assert(stopSoundUrl != null),
//			assert(updateCorrectAnswer != null),
			super(
				key: key,
				stage: stage,
				questionIdx: questionIdx,
				question: question,
				incorrectQuestionCount: incorrectQuestionCount,
				nextQuestion: nextQuestion,
				playSoundUrl: playSoundUrl,
				playSoundUrls: playSoundUrls,
				playSmallSoundUrl: playSmallSoundUrl,
				stopSoundUrl: stopSoundUrl,
				updateCorrectAnswer: updateCorrectAnswer,);

  @override
  State<StatefulWidget> createState() => ExerciseExamSpeakingLongScreenState();

}

class ExerciseExamSpeakingLongScreenState extends AbstractExerciseScreenState<ExerciseExamSpeakingLongScreen> {

  int incorrectAnswerCount = 0;

  Timer _timer;

	int nextQuestionCd;
  ///
  
  @override
  void initState() {
    super.initState();

		nextQuestionCd = 0;
    if(widget.stage.type == null || widget.stage.type == StageType.ONE) {
			_playQuestionAudio();
		}
  }

  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();

    super.dispose();
  }

	@override
	bool shouldInitFCM() {
		return false;
	}


	@override
	void afterLoadSharedPreferences() {
		super.afterLoadSharedPreferences();

		nextQuestionCountDown();
	}

  Widget _buildPlayQuestionAudioButton() {
    if(widget.question == null || widget.question.questionAudio.isEmpty) {
      return CommonWidget.emptyButtonContainer;
    } else if(playSoundUrlState == PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_up,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () async {
					await widget.stopSoundUrl();
        },
      );
    } else if(playSoundUrlState != PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_off,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () {
          _playQuestionAudio();
        },
      );
    } else {
      return CommonWidget.emptyButtonContainer;
    }
  }

  void _playQuestionAudio() {
  	print('$runtimeType, _playQuestionAudio:${widget.question.questionAudio}');
    if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
			widget.playSoundUrls(widget.question.questionAudio, 0);
    }
  }

	void nextQuestionCountDown() {
		if(_timer != null) {
			_timer.cancel();
		}
		_timer = Timer(Duration(milliseconds: Constants.DEFAULT_COUNTDOWN_MILLIS), () {
			setState(() {
				nextQuestionCd += Constants.DEFAULT_COUNTDOWN_MILLIS;
			});
			if(nextQuestionCd >= widget.question.time * 1000) {
//				widget.stopSoundUrl();
//				widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
			} else {
				nextQuestionCountDown();
			}
		});
	}

	List<TextSpan> _buildTextSpan() {
		List<TextSpan> result = List();
		if(widget.question != null && widget.question.questionFlow != null) {
			for (int i = 0; i < widget.question.questionFlow.length; i++) {
				QuestionFlowItem flowItem = widget.question.questionFlow[i];
				result.add(TextSpan(
					text: (i % 2 == 1 ? '    ' : '') + flowItem.value + '\n',
					style: TextStyle(
						color: flowItem.color == 1 ? Colors.black87 : Colors.indigo,
					),
				));
			}
		}
		return result;
	}

  ///
  /**
   * Question example:
   * {
			"id": 1,
			"type": "SpeakingLong",
			"question_picture": "1-A.Pedestrians are crossing the street.Những người đi bộ đang băng qua phố.\nB.Some vehicles are stopped in traffic.Một số phương tiện bị dừng lại trong khi tham gia giao thông.\nC.Goods are being unloaded from a truck.Hàng hoá đang được dỡ ra từ một chiếc xe tải.\nD.Some people are getting off their motorbikes.Một số người đang xuống xe máy của họ.\n2-A.The woman is unplugging an appliance.Người phụ nữ đang rút phích cắm một thiết bị.\nB.Some dishes are being placed in a cabinet.Một số món ăn đang được đặt trong một cái tủ.\nC.The woman is emptying out the contents of a cup.Người phụ nữ đang đổ đồ ra ngoài cốc.\nD.The kitchen plumbing is being repaired.Hệ thống ống nước nhà bếp đang được sửa chữa.\n3-A.The furniture is being rearranged.Đồ đạc đang được sắp xếp lại.\nB.The carpet has been rolled up in the corner.Thảm đã được cuộn lại ở góc.\nC.Some pillows are stacked beside a bed.Một vài cái gối được xếp chồng lên nhau bên cạnh một cái giường.\nD.Bedding has been folded and placed on a mattress.Bộ đồ giường đã được gấp lại và đặt trên một cái đệm.\n4-A.Some chairs are lined up by the side of swimming pool.Một vài cái ghế được đặt thành hàng bên cạnh bể bơi.\nB.A tugboat is pulling a ship out to sea.Tàu kéo đang kéo một con tàu ra biển.\nC.Umbrellas are sheltering people from the rain.Những chiếc ô đang che cho mọi người dưới mưa.\nD.Leaves are being removed from the pool.Những chiếc lá cây đang được dọn ra khỏi bể bơi.\n5-A.Workers are taking a tank out of a building.Những người công nhân đang lấy một cái bình chứa ra khỏi một tòa nhà.\nB.Plastic sheets cover a building frame.Các tấm nhựa phủ kín một cái khung của tòa nhà.\nC.Stepladders are being set up in a tent.Thang đứng đang được dựng lên trong một cái lều.\nD.Some workers are trimming pieces of wood.Một số công nhân đang bào gỗ.",
			"question": "",
			"answer_text": "",
			"hint_text": "",
			"question_audio": [
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/number1.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/TOEIC_ETS_2016_0816.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/number2.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/TOEIC_ETS_2016_0817.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/number3.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/TOEIC_ETS_2016_0818.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/number4.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/TOEIC_ETS_2016_0819.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/number5.mp3",
			"http://media.tataenglish.com/Listening/TOEIC ETS 2016/Audio/TOEIC_ETS_2016_08110.mp3"
			],
			"time": 2
			}
   */
  ///
  @override
  Widget build(BuildContext context) {
		return Container(
			child: Padding(
				padding: EdgeInsets.all(10),
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						/// header section
						Row(
							children: <Widget>[
								Container(
									alignment: Alignment(0, 0),
									width: 32,
									height: 32,
									decoration: BoxDecoration(
											shape: BoxShape.circle,
											color: MyColors.colorPrimary
									),
									child: Text('${widget.questionIdx + 1}',
										style: TextStyle(
												color: Colors.white,
												fontWeight: FontWeight.bold,
												fontSize: 16
										),
									),
								),
								CommonWidget.smallVerticalDivider,
								Expanded(
									child: LinearProgressIndicator(
										backgroundColor: MyColors.colorPrimaryDark,
										value: (widget.questionIdx + 1) / widget.stage.questions.length,
									),
								),
								CommonWidget.smallVerticalDivider,
								Text('${widget.stage.questions.length - widget.incorrectQuestionCount()}/${widget.stage.questions.length}',
									style: TextStyle(
										fontWeight: FontWeight.bold,
									),
								),
							],
						),
						/// END header section
						CommonWidget.mediumHorizontalDivider,
						/// question section
						Expanded(
							child: Container(
								padding: EdgeInsets.all(10),
								decoration: BoxDecoration(
										border: Border.all(
												color: MyColors.colorPrimary,
												width: 1.0
										)
								),
								alignment: AlignmentDirectional(0, 0),
								child: SingleChildScrollView(
									child: RichText(
										textAlign: TextAlign.start,
										text: TextSpan(
											style: const TextStyle(
												fontSize: 17,
												fontWeight: FontWeight.w600,
											),
											children: _buildTextSpan(),
										),
									),
								),
							)
						),
						/// END question section
						/// manipulation section
						/// END manipulation section
						/// bottom section
						CommonWidget.smallHorizontalDivider,
						Row(
							mainAxisAlignment: MainAxisAlignment.spaceBetween,
							crossAxisAlignment: CrossAxisAlignment.center,
							children: <Widget>[
								_buildPlayQuestionAudioButton(),
								GestureDetector(
									onTap: nextQuestionCd < widget.question.time * 1000 ? null : () async {
										await widget.stopSoundUrl();
										widget.updateCorrectAnswer(widget.questionIdx, true);
										widget.nextQuestion(widget.questionIdx, true);
									},
									child: Stack(
										alignment: AlignmentDirectional(0, 0),
										children: <Widget>[
											SizedBox(
												width: 60.0,
												height: 60.0,
												child: CircularProgressIndicator(
													backgroundColor: MyColors.colorPrimary,
													valueColor: AlwaysStoppedAnimation<Color>(MyColors.colorAccent),
													value: nextQuestionCd / (widget.question.time * 1000),
													strokeWidth: 6.0,
												),
											),
											Text(nextQuestionCd < widget.question.time * 1000 ? '' : 'Next'),
										],
									),
								),
								CommonWidget.emptyButtonContainer,
							],
						),
						/// END bottom section
					],
				),
			),
		);
  }
}