import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:intl/intl.dart';
import 'package:audioplayers/audioplayers.dart';
//import 'package:speech_recognition/speech_recognition.dart';
import 'package:permission_handler/permission_handler.dart';
//import 'package:flutter_sound/flutter_sound.dart';

import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/utils.dart';

import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_alpha_screen.dart';
import 'exercise_exam_game_screen.dart';
import 'exercise_exam_ninja_screen.dart';
import 'exercise_exam_option_all_screen.dart';
import 'exercise_exam_speaking_long_screen.dart';
import 'exercise_exam_typing_all_screen.dart';
import 'exercise_exam_option_screen.dart';
import 'exercise_exam_pronunciation_screen.dart';
import 'exercise_exam_speaking_screen.dart';
import 'exercise_exam_start_screen.dart';
import 'exercise_exam_end_screen.dart';
import 'exercise_exam_mapping_screen.dart';
import 'exercise_exam_typing_screen.dart';
import 'exercise_exam_typing_sub_screen.dart';

class ExerciseExamPageScreen extends StatefulWidget {

  final GlobalKey<ExerciseExamPageScreenState> exerciseExamPageScreenKey = GlobalKey<ExerciseExamPageScreenState>();

  final int type;

  final Exercise exercise;

  final Stage stage;

  ExerciseExamPageScreen(
    this.type,
    {this.exercise,
    this.stage,
  }) : assert((type == ExerciseExamPageScreenState.TYPE_BY_EXERCISE && exercise != null)
      || (type == ExerciseExamPageScreenState.TYPE_BY_STAGE && stage != null));

  @override
  ExerciseExamPageScreenState createState() => ExerciseExamPageScreenState();

}

enum PlayerState { stopped, playing, paused }

class ExerciseExamPageScreenState extends AbstractState<ExerciseExamPageScreen> {
  static const int TYPE_BY_EXERCISE = 1;
  static const int TYPE_BY_STAGE = 2;

  PageController _pageController;
//  int _currPageIndex = 0;
  //

  Stage stage;
//  int numIncorrect = 0;
  int _loaded = 0;
  List<Widget> _pages;
  List<int> correctQuestionIdxs = List();

  Timer _timer;
  int secondExam = 0;

  int examStatus = 0; // not start yet
  final DateFormat df = DateFormat('mm:ss');

//  /// @author haimt
//  /// for speech recognition
//  static const int STATUS_NOT_SPEECH = 0;
//  static const int STATUS_START_RECOGNIZE = 1;
//  static const int STATUS_SPEAKING = 2;
//  static const int STATUS_FINISH_RECOGNIZE = 3;
//  static const int STATUS_UPLOADED_API = 4;
//  static const int STATUS_GET_RESULT = 5;
//
//  SpeechRecognition speechRecognition;
//  bool speechRecognitionAvailable = false;
//
//  bool checkSpeechRecognitionAvailable() => speechRecognitionAvailable;
//
//  static const String LOCALE_EN_US = 'en_US';

  ///
  int numIncorrect = 0;

  // for Game exercise only
  int _gameLive = 0;
  int getGameLive() => _gameLive;
  int _gameScore = 0;
  int getGameScore() => _gameScore;
  bool _gamePass = false;
  bool isGamePass() => _gamePass;
  ///

  /// sound record
//  FlutterSound flutterSound;
//  bool isRecording = false;
//  bool isPlayingRecorded = false;
//  StreamSubscription _recorderSubscription;
//  StreamSubscription _dbPeakSubscription;
//  StreamSubscription _playerSubscription;
//
//  double _dbLevel;
//
//  double sliderCurrentPosition = 0.0;
//  double maxDuration = 1.0;
//
////  String recordedFilePath = ''; // not use
//  String recordedSpeakingFilePath = '';
  ///
  
  /// haimt: audioPlayer
  /// in order to play remote url
  bool isLocal = false;
  PlayerMode mode = PlayerMode.MEDIA_PLAYER;

  AudioPlayer _audioPlayer;
  AudioPlayer _audioOtherPlayer;

  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;
  ///
  List<GlobalKey> _globalKeys;

  String soundUrl = null;
	List<String> soundUrls = null;
	int currSoundIndex = 0;
//	String smallSoundUrl;
  ///

  PermissionStatus _permMicrophoneStt;

  int currQuestionIndex = 0;
  int currPageIndex = 0;

  Widget _buildPage(int index) {
    Widget page = _pages[index];
    if(page == null) {
      if(stage.type == null || stage.type == StageType.ONE) {
        /// stage One
        GlobalKey key;
        if (index == _buildPageCount() - 1) {
          key = GlobalKey();
          page = ExerciseExamEndScreen(
            key: key,
            exercise: widget.exercise,
            stage: stage,
            numIncorrect: getNumIncorrect,
            secondExam: secondExam,
            playSoundUrl: playSoundUrl,
            playSmallSoundUrl: playSmallSoundUrl,
            stopSoundUrl: stopSoundUrl,
            finish: finish,
            getGameScore: getGameScore,
            isGamePass: isGamePass,
          );
        } else if (index == 0) {
					key = GlobalKey();
					page = ExerciseExamStartScreen(
						key: key,
						stage: stage,
						startQuestion: startQuestion,
						checkPermission: checkPermission,
					);
				} else if(getQuestion(0).type == QuestionType.NINJA) {
					key = GlobalKey();
					page = ExerciseExamNinjaScreen(
						key: key,
						stage: stage,
						incorrectQuestionCount: getIncorrectQuestionCount,
						nextQuestion: nextQuestion,
						finishQuestion: finishQuestion,
//                playSoundUrl: playSoundUrl,
//                stopSoundUrl: stopSoundUrl,
						updateCorrectAnswer: updateCorrectAnswer,
						exerciseExamPageScreenState: this,
						numIncorrect: getNumIncorrect,
						secondExam: secondExam,
					);
        } else {
          final Question question = getQuestion(index - 1);
          if (question == null) {
            return CommonWidget.noExerciseContainer;
          } else {
            switch (question.type) {
              case QuestionType.PRONUNCIATION:
                key = GlobalKey();
                page = ExerciseExamPronunciationScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
//                  checkSpeechRecognitionAvailable: checkSpeechRecognitionAvailable,
//                  startRecognition: startRecognition,
//                  stopRecognition: stopRecognition,
//                  clearTranscription: clearTranscription,
                  playSoundUrl: playSoundUrl,
									playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                  /*startPlaySpeaking: startPlaySpeaking*/);
                break;
              case QuestionType.ALPHA:
                key = GlobalKey();
                page = ExerciseExamAlphaScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
                  playSoundUrl: playSoundUrl,
									playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                );
                break;
              case QuestionType.TYPING:
                key = GlobalKey();
                page = ExerciseExamTypingScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
                  playSoundUrl: playSoundUrl,
									playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                );
                break;
              case QuestionType.SPEAKING:
                key = GlobalKey();
                page = ExerciseExamSpeakingScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
                  playSoundUrl: playSoundUrl,
									playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                );
                break;
              case QuestionType.SPEAKING_LONG:
                key = GlobalKey();
                page = ExerciseExamSpeakingLongScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
                  playSoundUrl: playSoundUrl,
                  playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                );
                break;
              case QuestionType.OPTION:
                key = GlobalKey();
                page = ExerciseExamOptionScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
                  playSoundUrl: playSoundUrl,
									playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                );
                break;
              case QuestionType.MAPPING:
                key = GlobalKey();
                page = ExerciseExamMappingScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
                  playSoundUrl: playSoundUrl,
									playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                );
                break;
              case QuestionType.TYPING_SUB:
                key = GlobalKey();
                page = ExerciseExamTypingSubScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
                  playSoundUrl: playSoundUrl,
									playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                );
                break;
              case QuestionType.GAME:
                key = GlobalKey();
                page = ExerciseExamGameScreen(
                  key: key,
                  stage: stage,
                  questionIdx: index - 1,
                  question: getQuestion(index - 1),
                  incorrectQuestionCount: getIncorrectQuestionCount,
                  nextQuestion: nextQuestion,
                  playSoundUrl: playSoundUrl,
                  playSoundUrls: playSoundUrls,
                  playSmallSoundUrl: playSmallSoundUrl,
                  stopSoundUrl: stopSoundUrl,
                  updateCorrectAnswer: updateCorrectAnswer,
                  getGameLive: getGameLive,
                  getGameScore: getGameScore,
                  isGamePass: isGamePass,
                  adjustGameLive: adjustGameLive,
                  adjustGameScore: adjustGameScore,
                  setGamePass: setGamePass,
                  finishQuestion: finishQuestion,
                );
                break;
              default:
                key = GlobalKey();
                page = CommonWidget.emptyScreenWithNotice(
                    'Dạng bài tập chưa được hỗ trợ: ${question.type}!!');
                break;
            }
          }
        }
        _pages[index] = page;
        _globalKeys[index] = key;
      } else {
        /// dang stage all
        GlobalKey key;
        if (index == 0) {
          key = GlobalKey();
          page = ExerciseExamStartScreen(
            key: key,
            stage: stage,
            startQuestion: startQuestion,
            checkPermission: checkPermission,
          );
        } else {
          final Question question = getQuestion(0);
          if (question == null) {
            return CommonWidget.noExerciseContainer;
          } else {
            switch (question.type) {
            case QuestionType.TYPING:
              key = GlobalKey();
              page = ExerciseExamTypingAllScreen(
                key: key,
                stage: stage,
                incorrectQuestionCount: getIncorrectQuestionCount,
                nextQuestion: nextQuestion,
                finishQuestion: finishQuestion,
//                playSoundUrl: playSoundUrl,
//                stopSoundUrl: stopSoundUrl,
                updateCorrectAnswer: updateCorrectAnswer,
                exerciseExamPageScreenState: this,
                numIncorrect: getNumIncorrect,
                secondExam: secondExam,
              );
              break;
            case QuestionType.OPTION:
              key = GlobalKey();
              page = ExerciseExamOptionAllScreen(
                key: key,
                stage: stage,
                incorrectQuestionCount: getIncorrectQuestionCount,
                nextQuestion: nextQuestion,
                finishQuestion: finishQuestion,
//                playSoundUrl: playSoundUrl,
//                stopSoundUrl: stopSoundUrl,
                updateCorrectAnswer: updateCorrectAnswer,
                exerciseExamPageScreenState: this,
                numIncorrect: getNumIncorrect,
                secondExam: secondExam,
              );
              break;
            default:
              key = GlobalKey();
              page = CommonWidget.emptyScreenWithNotice(
                  'Dạng bài tập chưa được hỗ trợ: ${question.type}!!');
              break;
            }
          }
          _pages[index] = page;
          _globalKeys[index] = key;
        }
      }
		}
//    print('$runtimeType._buildPage, index:$index, page:$page,${page.key}');
    return page;
  }

  int _buildPageCount() {
    if(getQuestion(0).type == QuestionType.NINJA) {
      return 3;
    } else if(stage == null || stage.type == null || stage.type == StageType.ONE) {
			return stage == null || stage.questions == null ? 1 : stage.questions.length + 2;
		} else {
			return 2;
		}
  }

  Question getQuestion(int index) {
    return stage == null || stage.questions == null || stage.questions.length < index ? null
        : stage.questions[index];
  }

  void startQuestion() {
    secondExam = 0;
    numIncorrect = 0;
    _timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      setState(() {
        secondExam = secondExam += 1;
        examStatus = 1; // start exam
      });
    });
    currPageIndex = 1;
    _pageController.jumpToPage(currPageIndex);
  }

  void finishQuestion() {
    _timer.cancel();
    setState(() {
      examStatus = 2; // finish exam
    });
    _pageController.jumpToPage(_buildPageCount());
  }

  void nextQuestion(int currQuestionIdx, bool currQuestionCorrectAnswer) {
//    print('$runtimeType.nextQuestion, currQuestionIdx=$currQuestionIdx');
    this.currQuestionIndex = currQuestionIdx + 1;
    this.currPageIndex = currQuestionIdx + 2;
    if(currQuestionIdx == stage.questions.length - 1) {
      finishQuestion();
    } else {
      _pageController.jumpToPage(currPageIndex);
    }
  }

  Widget _buildTimeExamCounter() {
    switch(examStatus) {
      case 1:
      case 2:
        return Container(
          padding: EdgeInsets.all(10),
          alignment: AlignmentDirectional(0, 0),
          child: Text(df.format(DateTime.fromMillisecondsSinceEpoch(secondExam * 1000)),
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        );
        break;
      default:
        return Container();
        break;
    }
  }

  @override
  void initState() {
    super.initState();

    _pageController = PageController(
      keepPage: false,
    );

    _initAudioPlayer();
  }

  @override
  bool shouldInitFCM() {
    return false;
  }

//  // speech recognition
//  // Platform messages are asynchronous, so we initialize in an async method.
//  void _activateSpeechRecognizer() {
//    print('$runtimeType.activateSpeechRecognizer... ');
//    speechRecognition = new SpeechRecognition();
//    speechRecognition.setAvailabilityHandler(onSpeechAvailability);
//    speechRecognition.setCurrentLocaleHandler(onCurrentLocale);
//    speechRecognition.setRecognitionStartedHandler(onRecognitionStarted);
//    speechRecognition.setRecognitionResultHandler(onRecognitionResult);
//    speechRecognition.setRecognitionCompleteHandler(onRecognitionComplete);
//    speechRecognition.setErrorHandler(errorHandler);
//    speechRecognition
//        .activate()
//        .then((res) {
//          speechRecognitionAvailable = res;
////          if(_globalKeyExerciseExamSpeaking.currentState != null) {
////            _globalKeyExerciseExamSpeaking.currentState.updateSpeechRecognitionAvailable(res);
////          }
//          if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState is ExerciseExamPronunciationScreenState) {
//            (_globalKeys[currPageIndex].currentState as ExerciseExamPronunciationScreenState).updateSpeechRecognitionAvailable(res);
//          }
//        });
//  }
//
//  void startRecognition() {
//    clearTranscription();
//    stopSoundUrl();
//
//    speechRecognition
//        .listen(locale: LOCALE_EN_US) // only en_US locale supported
//        .then((result) {
//      print('$runtimeType.start => result $result');
//      if(result) {
////        _globalKeyExerciseExamSpeaking.currentState.updateSpeechStatus(STATUS_START_RECOGNIZE);
//      }
//    });
//  }
//
//  void cancelRecognition() =>
//      speechRecognition.cancel().then((result) {
////        _globalKeyExerciseExamSpeaking.currentState.updateSpeechRecognitionListeningAndStatus(result, STATUS_FINISH);
//        if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState is ExerciseExamPronunciationScreenState) {
//          (_globalKeys[currPageIndex].currentState as ExerciseExamPronunciationScreenState).updateSpeechRecognitionListeningAndStatus(result, STATUS_FINISH);
//        }
//      });
//
//  void stopRecognition() async {
//    await speechRecognition.stop().then((result) {
//      print('$runtimeType.stopRecognition, result=$result');
////      _globalKeyExerciseExamSpeaking.currentState.updateSpeechRecognitionListeningAndStatus(result, STATUS_FINISH);
//      if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState is ExerciseExamPronunciationScreenState) {
//        (_globalKeys[currPageIndex].currentState as ExerciseExamPronunciationScreenState).updateSpeechRecognitionListeningAndStatus(result, STATUS_FINISH);
//      }
//    });
//  }
//
//  void clearTranscription() {
////    _globalKeyExerciseExamSpeaking.currentState.clearTranscription();
//    if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState is ExerciseExamPronunciationScreenState) {
//      (_globalKeys[currPageIndex].currentState as ExerciseExamPronunciationScreenState).clearTranscription();
//    }
//  }
//
//  void onSpeechAvailability(bool result) {
//    speechRecognitionAvailable = result;
////    _globalKeyExerciseExamSpeaking.currentState.updateSpeechRecognitionAvailable(result);
//    if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState is ExerciseExamPronunciationScreenState) {
//      (_globalKeys[currPageIndex].currentState as ExerciseExamPronunciationScreenState).updateSpeechRecognitionAvailable(result);
//    }
//  }
//
//  void onCurrentLocale(String locale) {
//    print('$runtimeType.onCurrentLocale... $locale');
//  }
//
//  void onRecognitionStarted() {
////    _globalKeyExerciseExamSpeaking.currentState.onRecognitionStarted();
//    if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState is ExerciseExamPronunciationScreenState) {
//      (_globalKeys[currPageIndex].currentState as ExerciseExamPronunciationScreenState).onRecognitionStarted();
//    }
//  }
//
//  void onRecognitionResult(String text) {
////    _globalKeyExerciseExamSpeaking.currentState.onRecognitionResult(text);
//    if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState is ExerciseExamPronunciationScreenState) {
//      (_globalKeys[currPageIndex].currentState as ExerciseExamPronunciationScreenState).onRecognitionResult(text);
//    }
//  }
//
//  void onRecognitionComplete(String text) {
////    _globalKeyExerciseExamSpeaking.currentState.onRecognitionComplete(text);
//    if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState is ExerciseExamPronunciationScreenState) {
//      (_globalKeys[currPageIndex].currentState as ExerciseExamPronunciationScreenState).onRecognitionComplete(text);
//    }
//  }
//
//  void errorHandler() {
//    showSnackBar('Có lỗi xảy ra!!!');
//    _activateSpeechRecognizer();
//  }
//  // END speech recognition

//  // record sound
//
//
//  void _initSoundRecord() {
//    flutterSound = new FlutterSound();
//    flutterSound.setSubscriptionDuration(0.01);
//    flutterSound.setDbPeakLevelUpdate(0.8);
//    flutterSound.setDbLevelEnabled(true);
//  }
//
//  void startRecordSpeaking() async {
//    try {
//      recordedSpeakingFilePath = await flutterSound.startRecorder(null);
//      print('$runtimeType.startRecorder: $recordedSpeakingFilePath');
//
//      _recorderSubscription = flutterSound.onRecorderStateChanged.listen((e) {
////        print('$runtimeType.startRecorder, recorder start listening, event: $e');
//      });
//      _dbPeakSubscription =
//          flutterSound.onRecorderDbPeakChanged.listen((value) {
////            print("$runtimeType.got update -> $value");
////            setState(() {
//            this._dbLevel = value;
////            });
//          });
//
////      this.setState(() {
////        this.isRecording = true;
////        this.recordedFilePath = '';
////      });
//      _globalKeyExerciseExamSpeaking.currentState.updateRecordedFilePath('');
//    } catch (err) {
//      print('$runtimeType.startRecorder error: $err');
//      showSnackBar('Có lỗi xảy ra: $err');
//    }
//  }
//
//  void stopRecordSpeaking() async {
//    try {
//      String result = await flutterSound.stopRecorder();
//      print('$runtimeType.stopRecorder: $result');
//
//      if (_recorderSubscription != null) {
//        _recorderSubscription.cancel();
//        _recorderSubscription = null;
//      }
//      if (_dbPeakSubscription != null) {
//        _dbPeakSubscription.cancel();
//        _dbPeakSubscription = null;
//      }
//
////      this.setState(() {
////        this.isRecording = false;
////        this.recordedFilePath = result;
////      });
//      _globalKeyExerciseExamSpeaking.currentState.updateRecordedFilePath(result);
//    } catch (err) {
//      print('stopRecorder error: $err');
//      showSnackBar('Có lỗi xảy ra: $err');
//    }
//  }
//
//  void startPlaySpeaking() async {
//    String path = await flutterSound.startPlayer(null);
//    await flutterSound.setVolume(1.0);
//    print('$runtimeType.startPlayer: $path');
//
//    try {
//      _playerSubscription = flutterSound.onPlayerStateChanged.listen((e) {
////        print('$runtimeType.startPlayer, listen event: $e');
//        if (e != null) {
//          sliderCurrentPosition = e.currentPosition;
//          maxDuration = e.duration;
////          this.setState(() {
//          this.isPlayingRecorded = true;
////          });
//          if(e.duration == e.currentPosition) {
//            _globalKeyExerciseExamSpeaking.currentState.setState(() {
//              _globalKeyExerciseExamSpeaking.currentState.playSpeakingState = 2;
//            });
//          }
//        }
//      });
//      _globalKeyExerciseExamSpeaking.currentState.setState(() {
//        _globalKeyExerciseExamSpeaking.currentState.playSpeakingState = 1;
//      });
//    } catch (err) {
//      print('$runtimeType.startPlayer error: $err');
//      showSnackBar('Có lỗi xảy ra: $err');
//    }
//  }
//
//  void stopPlaySpeaking() async{
//    try {
//      String result = await flutterSound.stopPlayer();
//      print('$runtimeType.stopPlay: $result');
//      if (_playerSubscription != null) {
//        _playerSubscription.cancel();
//        _playerSubscription = null;
//      }
//
////      this.setState(() {
//      this.isPlayingRecorded = false;
////      });
//    } catch (err) {
//      print('$runtimeType.stopPlayer error: $err');
//      showSnackBar('Có lỗi xảy ra: $err');
//    }
//  }
//  // END record sound

  @override
  void dispose() {
    _pageController.dispose();
    if(_timer != null && _timer.isActive) _timer.cancel();
    stopSoundUrl();
    _disposeAudioPlayer();
    super.dispose();
  }

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    if(widget.type == TYPE_BY_EXERCISE) {
      _getStageDetail();
    } else {
      setState(() {
        this.stage = this.widget.stage;
        _loaded = 2;
        _initExercise();
      });
    }
  }

  void _initExercise() {
    _pages = List(this.stage == null || this.stage.questions == null ? 2 : this.stage.questions.length + 2);
    _globalKeys = List(_pages.length);
    if(getQuestion(0).type == QuestionType.GAME) {
      _gameLive = this.stage.gameLive;
    }
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Thoát'),
        content: new Text('Bạn đang làm bài tập, bạn có chắc là bạn muốn thoát?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('Không'),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Có'),
          ),
        ],
      ),
    ) ?? false;
  }

  void finish() {
    Navigator.of(context).pop(true);
  }
  
  void _getStageDetail() async {
    setState(() {
      _loaded = 1;
    });
    await Api.getStageDetail(accessToken.accessToken, widget.exercise.exerciseId).then((_stage) {
      setState(() {
        this.stage = _stage;
        _loaded = 2;
        _initExercise();
      });
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        _loaded = 2;
      });
    });
  }

  void _initAudioPlayer() {
    AudioPlayer.logEnabled = false;
    _audioPlayer = AudioPlayer(/*mode: mode*/);
    _audioOtherPlayer = AudioPlayer(/*mode: mode*/);

    _durationSubscription =
        _audioPlayer.onDurationChanged.listen((duration) {
        });

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) {
        });

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
          _onPlaySoundUrlComplete();
        });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      showSnackBar(msg);
//      if(_globalKeyExerciseExamSpeaking != null) _globalKeyExerciseExamSpeaking.currentState.updatePlaySoundUrlState(PlayerState.stopped);
//      if(_globalKeyExerciseExamAlpha != null) _globalKeyExerciseExamAlpha.currentState.updatePlaySoundUrlState(PlayerState.stopped);
//      if(_globalKeyExerciseExamTyping != null) _globalKeyExerciseExamTyping.currentState.updatePlaySoundUrlState(PlayerState.stopped);
      if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null) {
        if(_globalKeys[currPageIndex].currentState is AbstractExerciseScreenState) {
          (_globalKeys[currPageIndex].currentState as AbstractExerciseScreenState).updatePlaySoundUrlState(PlayerState.stopped);
        }
      }
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
    });
  }

  void _disposeAudioPlayer() {
//    this.soundUrl = null;
//    this.soundUrls = null;
//    _audioPlayer.stop();
//    _audioOtherPlayer.stop();
    _audioPlayer.release();
    _audioOtherPlayer.release();
    _durationSubscription?.cancel();
    _positionSubscription?.cancel();
    _playerCompleteSubscription?.cancel();
    _playerErrorSubscription?.cancel();
    _playerStateSubscription?.cancel();
  }

  Future playSoundUrl(String url) async {
    if(Utils.isEmptyString(url)) return 0;
    this.soundUrl = url;
//    print('$runtimeType.playSoundUrl url:$url');
    final result = await _audioPlayer.play(Uri.encodeFull(this.soundUrl), volume: 2.0);
//    if(_globalKeyExerciseExamSpeaking != null) _globalKeyExerciseExamSpeaking.currentState.updatePlaySoundUrlState(PlayerState.playing);
//    if(_globalKeyExerciseExamAlpha != null) _globalKeyExerciseExamAlpha.currentState.updatePlaySoundUrlState(PlayerState.playing);
//    if(_globalKeyExerciseExamTyping != null) _globalKeyExerciseExamTyping.currentState.updatePlaySoundUrlState(PlayerState.playing);
    if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null) {
      if(_globalKeys[currPageIndex].currentState is AbstractExerciseScreenState) {
        (_globalKeys[currPageIndex].currentState as AbstractExerciseScreenState).updatePlaySoundUrlState(PlayerState.playing);
      }
    }
    return result;
  }

	Future playSmallSoundUrl(String url) async {
//		this.smallSoundUrl = url;
		final result = await _audioOtherPlayer.play(url, volume: 2.0);
		return result;
	}

	Future playSoundUrls(List<String> urls, int soundIndex) async {
		this.soundUrl = null;
		this.soundUrls = urls;
		this.currSoundIndex = soundIndex;
		final result = await _audioPlayer.play(Uri.encodeFull(this.soundUrls[currSoundIndex]), volume: 2.0);
		if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null) {
			if(_globalKeys[currPageIndex].currentState is AbstractExerciseScreenState) {
				(_globalKeys[currPageIndex].currentState as AbstractExerciseScreenState).updatePlaySoundUrlState(PlayerState.playing);
			}
		}
		return result;
	}

  Future pauseSoundUrl() async {
    final result = await _audioPlayer.pause();
//    _globalKeyExerciseExamSpeaking.currentState.updatePlaySoundUrlState(PlayerState.paused);
    if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null) {
      if(_globalKeys[currPageIndex].currentState is AbstractExerciseScreenState) {
        (_globalKeys[currPageIndex].currentState as AbstractExerciseScreenState).updatePlaySoundUrlState(PlayerState.paused);
      }
    }
    return result;
  }

  Future<int> stopSoundUrl() async {
    this.soundUrl = null;
		this.soundUrls = null;
    final result = await _audioPlayer.stop();
    if (result == 1) {
//      if(_globalKeyExerciseExamSpeaking != null) _globalKeyExerciseExamSpeaking.currentState.updatePlaySoundUrlState(PlayerState.stopped);
//      if(_globalKeyExerciseExamAlpha != null) _globalKeyExerciseExamAlpha.currentState.updatePlaySoundUrlState(PlayerState.stopped);
//      if(_globalKeyExerciseExamTyping != null) _globalKeyExerciseExamTyping.currentState.updatePlaySoundUrlState(PlayerState.stopped);
      if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState.mounted) {
        if(_globalKeys[currPageIndex].currentState is AbstractExerciseScreenState) {
          (_globalKeys[currPageIndex].currentState as AbstractExerciseScreenState).updatePlaySoundUrlState(PlayerState.stopped);
        }
      }
    }
    return result;
  }

  Future stopSmallSoundUrl() async {
    final result = await _audioOtherPlayer.stop();
    if (result == 1) {
    }
    return result;
  }

  void _onPlaySoundUrlComplete() {
    if(soundUrl != null) playSoundUrl(soundUrl);
		if(soundUrls != null && soundUrls.isNotEmpty) playSoundUrls(soundUrls, currSoundIndex == soundUrls.length -1 ? 0 : currSoundIndex + 1);
  }

  /// Runtime permission
  Future checkPermission() async {
    _permMicrophoneStt = await PermissionHandler().checkPermissionStatus(PermissionGroup.microphone);

    if(_permMicrophoneStt != PermissionStatus.granted) {
      // not granted
      _requestRecordAudioPermission();
      return false;
    } else {
//      if(speechRecognition == null) {
//        _activateSpeechRecognizer();
//      }
      return true;
    }
  }

  Future _requestRecordAudioPermission() async {
    Map<PermissionGroup, PermissionStatus> permissions = await PermissionHandler().requestPermissions([PermissionGroup.microphone]);

    if(permissions[PermissionGroup.microphone] != null && permissions[PermissionGroup.microphone] == PermissionStatus.granted) {
//      _activateSpeechRecognizer();
    } else {
      // request failed
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Microphone'),
            content: Text('Bạn cần đồng ý cho iHomework sử dụng microphone để làm bài tập.'),
            actions: <Widget>[
              FlatButton(
                child: Text('Tôi đã hiểu'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      );
    }
  }
  /// END Runtime permission

  void updateCorrectAnswer(int questionIdx, bool correct) {
    if(correct) {
      if (!correctQuestionIdxs.contains(questionIdx)) correctQuestionIdxs.add(questionIdx);
    } else {
      numIncorrect++;
    }
  }

  int getIncorrectQuestionCount() {
    return stage.questions.length - correctQuestionIdxs.length;
  }

  int getNumIncorrect() {
    return numIncorrect;
  }

  void adjustGameLive(int quantity) {
    this._gameLive += quantity;
  }

  void adjustGameScore(int quantity) {
    this._gameScore += quantity;
    if(_globalKeys[currPageIndex] != null && _globalKeys[currPageIndex].currentState != null && _globalKeys[currPageIndex].currentState.mounted &&
        _globalKeys[currPageIndex].currentState is ExerciseExamGameScreenState) {
      (_globalKeys[currPageIndex].currentState as ExerciseExamGameScreenState).playScoreAnimation();
    }
  }

  void setGamePass(bool pass) {
    this._gamePass = pass;
  }

  @override
  Widget build(BuildContext context) {
//    print('$runtimeType.build');
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(accessToken == null ? '' : accessToken.name.trim()),
        actions: <Widget>[
          _buildTimeExamCounter(),
        ],
      ),
      body: _loaded == 1 ?
      Center(
        child: CircularProgressIndicator(),
      ) :
      (stage == null || stage.questions == null ?
        CommonWidget.noExerciseContainer :
        WillPopScope(
          child: Container(
            child: PageView.builder(
              itemCount: _buildPageCount(),
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return _buildPage(index);
              },
            ),
          ),
          onWillPop: _onWillPop,
        )
      ),
    );
  }
}