import 'package:flutter/material.dart';
import 'package:ihomework/domain/exercise.dart';

abstract class AbstractExerciseExamScreen extends StatefulWidget {

  final Stage stage;

  final int questionIdx;

  final Question question;

  final int Function() incorrectQuestionCount;

  final void Function(int questionIdx, bool correctAnswer) nextQuestion;

  final void Function(String url) playSoundUrl;

  final void Function(List<String> urls, int soundIndex) playSoundUrls;

  final void Function(String url) playSmallSoundUrl;

  final Future<int> Function() stopSoundUrl;

  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

  AbstractExerciseExamScreen({
    key,
    this.stage,
    this.questionIdx,
    this.question,
    this.incorrectQuestionCount,
    this.nextQuestion,
    this.playSoundUrl,
    this.playSoundUrls,
    this.playSmallSoundUrl,
    this.stopSoundUrl,
    this.updateCorrectAnswer,
  }): assert(stage != null),
      assert(questionIdx != null),
      assert(question != null),
      assert(nextQuestion != null),
      assert(playSoundUrl != null),
      assert(playSoundUrls != null),
      assert(playSmallSoundUrl != null),
      assert(stopSoundUrl != null),
      assert(updateCorrectAnswer != null),
      super(key: key);
}