import 'package:async/async.dart';
import 'package:flutter/widgets.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/utils.dart';

import '../exercise_exam_page_screen.dart';
import 'abstract_state.dart';

abstract class AbstractExerciseExamState<T extends StatefulWidget> extends AbstractState<T> {

  final Stage stage;

  final int questionIdx;

  final Question question;

  final int Function() numIncorrect;

  final void Function(int questionIdx, bool correctAnswer) nextQuestion;

  final void Function(String url) playSoundUrl;

  final void Function(List<String> urls, int soundIndex) playSoundUrls;

  final void Function() stopSoundUrl;

  final void Function(int questionIdx) updateCorrectAnswer;

  AbstractExerciseExamState({
    this.stage,
    this.questionIdx,
    this.question,
    this.numIncorrect,
    this.nextQuestion,
    this.playSoundUrl,
    this.playSoundUrls,
    this.stopSoundUrl,
    this.updateCorrectAnswer,
  }): assert(stage != null),
      assert(questionIdx != null),
      assert(question != null),
      assert(nextQuestion != null),
      assert(playSoundUrl != null),
      assert(playSoundUrls != null),
      assert(stopSoundUrl != null),
      assert(updateCorrectAnswer != null)
  ;

  static const int ANSWER_NOT_RECORDED = 0;
  static const int ANSWER_INCORRECT = 1;
  static const int ANSWER_CORRECT = 2;

  /// play question audio
  int correctAnswer = ANSWER_NOT_RECORDED;
  var incorrectAnswerCount = 0;
  /// END play question audio

  /// play question audio
  PlayerState playSoundUrlState = PlayerState.stopped;
  ///

  void playQuestionAudio() {
    if(question != null && question.questionAudio.isNotEmpty) {
      playSoundUrls(question.questionAudio, 0);
    }
  }

  void updatePlaySoundUrlState(PlayerState playSoundUrlState) {
    setState(() {
      this.playSoundUrlState = playSoundUrlState;
    });
  }
}