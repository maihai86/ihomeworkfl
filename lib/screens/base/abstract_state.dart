import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ihomework/domain/firebase_message.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:ihomework/domain/access_token.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/screens/exercise_exam_page_screen.dart';
import 'package:url_launcher/url_launcher.dart';

abstract class AbstractState<T extends StatefulWidget> extends State<T> {
  static const int LOAD_IDLE = 0;
  static const int LOAD_LOADING = 1;
  static const int LOAD_FINISH = 2;

  SharedPreferences prefs;
  AccessToken accessToken;
  bool loadAccessToken = false;
  int loaded = LOAD_IDLE;

  /// haimt firebase messaging
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  /// END haimt firebase messaging

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    _loadSharedPreferences();
    if(shouldInitFCM()) {
      _initFirebaseMessaging();
    }
  }

  void _loadSharedPreferences() async {
    prefs = await SharedPreferences.getInstance();

    AccessToken _accessToken;
    if(prefs.getString(PrefsKey.KEY_ACCESS_TOKEN) != null) {
      _accessToken = AccessToken.fromJson(jsonDecode(prefs.getString(PrefsKey.KEY_ACCESS_TOKEN)));
    }
    print('access token: ${prefs.getString(PrefsKey.KEY_ACCESS_TOKEN)}, '
//        '${jsonDecode(prefs.getString(PrefsKey.KEY_ACCESS_TOKEN))}, '
        '$_accessToken');
    setState(() {
      try {

        accessToken = _accessToken;
      } catch(e) {
        showSnackBar('Có lỗi xảy ra: ${e.toString()}');
      }
      loadAccessToken = true;
    });

    // get firebase device token
    if(shouldInitFCM() && shouldGetFCMToken()) {
      _getFCMToken();
    }
    // END get firebase device token

    afterLoadSharedPreferences();
  }

  bool shouldInitFCM();

  void _initFirebaseMessaging() {
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("$runtimeType.onMessage: $message");
        _showFirebaseMessagingContent(message);
      },
      // onLaunch = onResume
      onLaunch: (Map<String, dynamic> message) async {
        print("$runtimeType.onLaunch: $message");
        _showFirebaseMessagingContent(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("$runtimeType.onResume: $message");
        _showFirebaseMessagingContent(message);
      },
    );
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    firebaseMessaging.getToken().then((String token) {
      assert(token != null);
//      setState(() {
//        _homeScreenText = "Push Messaging token: $token";
//      });
    });
  }

  bool shouldGetFCMToken() => false;

  void _getFCMToken() {
    firebaseMessaging.getToken().then((token) {
      print('FIREBASE_TOKEN:$token');
      prefs.setString(PrefsKey.KEY_FIREBASE_DEVICE_TOKEN, token);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// haimt override when needed
  void afterLoadSharedPreferences() {}

  void showSnackBar(String message) {
    try {
      if(scaffoldKey != null && scaffoldKey.currentState != null) {
        scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text(message,
                textAlign: TextAlign.center,
              ),
            )
        );
      } else {
        Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(message,
                textAlign: TextAlign.center,
              ),
            )
        );
      }

    } catch(e) {
      print(e);
    }
  }

  /// sample firebase message json
  /*
  {
    "from": 440759390672,
    "collapse_key": "vn.tata.ihomework",
    "notification": {
      "body": "notification content",
      "title": "notification title 18/6/2019",
      "e": 1,
      "tag": "campaign_collapse_key_4996920559440196071"
    }
  }
  */
  /*
  {
  "google.c.a.e": 1,
  "aps": {
    "alert": {
      "title": "Thông báo",
      "body": "Các bẹn ngày mai nghị học"
    }
  },
  "gcm.n.e": 1,
  "google.c.a.c_id": 7661749300985940000,
  "google.c.a.udt": 0,
  "gcm.message_id": 1562686485876919,
  "google.c.a.ts": 1562686485
}
   */
  void _showFirebaseMessagingContent(Map<String, dynamic> message) {
    if(message == null) return;
    final String title = message['aps']['alert']['title'];
    final String body = message['aps']['alert']['body'];
    showDialog<bool>(
      context: context,
      builder: (_) => AlertDialog(
        title: Text(title),
        content: Text(body),
        actions: <Widget>[
          FlatButton(
            child: const Text('OK'),
            onPressed: () {
              Navigator.pop(context, false);
            },
          ),
        ],
      ),
    );
  }

  void hideKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  Future<bool> launchURL(String feedbackUrl) async {
    if (await canLaunch(feedbackUrl)) {
      return await launch(feedbackUrl);
    } else {
      showSnackBar('Không thể đi tới trang phản hồi. Bạn vui lòng thử lại sau!');
      return false;
    }
  }
}