import 'package:flutter/material.dart';

import '../exercise_exam_page_screen.dart';
import 'abstract_exercise_exam_screen.dart';
import 'abstract_state.dart';

abstract class AbstractExerciseScreenState<T extends StatefulWidget> extends AbstractState<T> {

	/// play question audio
	PlayerState playSoundUrlState = PlayerState.stopped;

	void updatePlaySoundUrlState(PlayerState playSoundUrlState) {
		if(this.mounted) {
			setState(() {
				this.playSoundUrlState = playSoundUrlState;
			});
		}
	}

//	@override
//	void dispose() {
//		if(widget is AbstractExerciseExamScreen) {
//			(widget as AbstractExerciseExamScreen).stopSoundUrl();
//		}
//		super.dispose();
//	}
}