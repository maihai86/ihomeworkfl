import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamTypingSubScreen extends AbstractExerciseExamScreen {

//  final Stage stage;
//
//  final int questionIdx;
//
//  final Question question;
//
//  final int Function() incorrectQuestionCount;
//
//  final void Function(int questionIdx, bool correctAnswer) nextQuestion;
//
//  final void Function(String url) playSoundUrl;
//
//	final void Function(List<String> urls, int soundIndex) playSoundUrls;

  final void Function(int questionIndex, String url) playSoundUrlNew;

//  final void Function(String url) playSmallSoundUrl;
//
//  final void Function() stopSoundUrl;

  final void Function(int questionIndex) stopSoundUrlNew;

//  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

  final void Function(int index) scrollToIndex;

	final int Function() currentQuestionIndex;

	final FocusNode focusNode;

	final TextEditingController textEditingController;

  final Function() nextQuestionOrValidateAllAnswer;

	ExerciseExamTypingSubScreen({
    key,
    stage,
    questionIdx,
    question,
    incorrectQuestionCount,
    nextQuestion,
    playSoundUrl,
    playSoundUrls,
    playSmallSoundUrl,
    stopSoundUrl,
    updateCorrectAnswer,
    this.scrollToIndex,
		this.currentQuestionIndex,
		this.playSoundUrlNew,
		this.stopSoundUrlNew,
		this.focusNode,
		this.textEditingController,
		this.nextQuestionOrValidateAllAnswer,
  }): /*assert(stage != null),*/
//      assert(questionIdx != null),
//      assert(question != null),
//      assert(nextQuestion != null),
//      assert(playSoundUrl != null),
//      assert(playSoundUrls != null),
//      assert(playSmallSoundUrl != null),
//      assert(stopSoundUrl != null),
//      assert(updateCorrectAnswer != null),
      super(
				key: key,
				stage: stage,
				questionIdx: questionIdx,
				question: question,
				incorrectQuestionCount: incorrectQuestionCount,
				nextQuestion: nextQuestion,
				playSoundUrl: playSoundUrl,
				playSoundUrls: playSoundUrls,
				playSmallSoundUrl: playSmallSoundUrl,
				stopSoundUrl: stopSoundUrl,
				updateCorrectAnswer: updateCorrectAnswer,);

  @override
  State<StatefulWidget> createState() => ExerciseExamTypingSubScreenState();

}

class ExerciseExamTypingSubScreenState extends AbstractExerciseScreenState<ExerciseExamTypingSubScreen> {

  static const int ANSWER_NOT_RECORDED = 0;
  static const int ANSWER_INCORRECT = 1;
  static const int ANSWER_CORRECT = 2;

  int correctAnswer = ANSWER_NOT_RECORDED;
//  int incorrectAnswerCount = 0;

	bool _showHint = false;
  Timer _timer;
  ///

  TextEditingController _controller;
	FocusNode _focusNode;

	int typeIndex = 1;
	String currAnswer;
	Map<String, String> mapUserAnswers = Map();
	Map<String, List<String>> mapAnswerGroups = Map();
	Map<String, int> mapIncorrectAnswerCount;

  @override
  void initState() {
    super.initState();

    _controller = TextEditingController();
		_focusNode = FocusNode();
		_playQuestionAudio();
  }

  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();
    _controller.dispose();
		_focusNode.dispose();

    super.dispose();
  }

	@override
	bool shouldInitFCM() {
		return false;
	}


	@override
	void afterLoadSharedPreferences() {
		super.afterLoadSharedPreferences();

		_initExercise();
		FocusScope.of(context).requestFocus(_focusNode);
	}

	void _initExercise() {
		mapIncorrectAnswerCount = Map();
		for(Map<String, String> _map in widget.question.questionGroup) {
			mapIncorrectAnswerCount[_map.keys.toList()[0]] = 0;
		}
	}

	Widget _buildPlayQuestionAudioButton() {
    if(widget.question == null || widget.question.questionAudio.isEmpty) {
      return CommonWidget.emptyButtonContainer;
    } else if(playSoundUrlState == PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_up,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () async {
					await widget.stopSoundUrl();
        },
      );
    } else if(playSoundUrlState != PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_off,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () {
          _playQuestionAudio();
        },
      );
    } else {
      return CommonWidget.emptyButtonContainer;
    }
  }

  Widget _buildPrecisionIcon() {
    if(correctAnswer == ANSWER_CORRECT) {
      // haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.check,
            color: Colors.green,
            size: 68,
          ),
        ],
      );
    } else if(correctAnswer == ANSWER_INCORRECT) {
      // haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.close,
            color: Colors.red,
            size: 68,
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  void _playQuestionAudio() {
  	print('$runtimeType, _playQuestionAudio:${widget.question.questionAudio}');
    if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
			widget.playSoundUrls(widget.question.questionAudio, 0);
    }
  }

  void _validateAnswer() {
		if (widget.question.mapAnswerGroup[typeIndex.toString()].contains(Utils.normalizeAnswerTypingSub(_controller.text.trim().toUpperCase()))) {
			widget.playSmallSoundUrl(Constants.CORRECT_URLS[0]);
			mapUserAnswers[typeIndex.toString()] = _controller.text.trim();
			if(_showHint) {
				setState(() {
					this.typeIndex += 1;
					this._showHint = false;
					this.correctAnswer = ANSWER_CORRECT;
				});
			} else {
				setState(() {
					this.typeIndex += 1;
					this.correctAnswer = ANSWER_CORRECT;
				});
			}
			_controller.clear();

			if(widget.question != null && widget.question.questionGroup != null && widget.question.questionGroup.length == mapUserAnswers.length) {
				hideKeyboard();
				widget.updateCorrectAnswer(widget.questionIdx, true);

				Timer(Duration(milliseconds: 500), () async {
					await widget.stopSoundUrl();
					widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
				});
			}
		} else {
//			incorrectAnswerCount++;
			if (mapIncorrectAnswerCount[typeIndex.toString()] >= Constants.INCORRECT_ANSWER_MAX) {
				setState(() {
					_showHint = true;
					this.correctAnswer = ANSWER_INCORRECT;
					mapIncorrectAnswerCount[typeIndex.toString()] = mapIncorrectAnswerCount[typeIndex.toString()] + 1;
					this.currAnswer = _controller.text.trim();
				});
			} else {
				setState(() {
					this.correctAnswer = ANSWER_INCORRECT;
					mapIncorrectAnswerCount[typeIndex.toString()] = mapIncorrectAnswerCount[typeIndex.toString()] + 1;
					this.currAnswer = _controller.text.trim();
				});
			}
			widget.updateCorrectAnswer(widget.questionIdx, false);
			widget.playSmallSoundUrl(Constants.INCORRECT_URLS[0]);
		}
		FocusScope.of(context).requestFocus(_focusNode);
	}

	List<TextSpan> _buildTextQuestion() {
		if(widget.question == null || widget.question.questionGroup == null || widget.question.questionGroup.isEmpty) {
			return List();
		}

		List<TextSpan> result = List();
		for(int i = 0; i < widget.question.questionGroup.length; i++) {
			Map<String, String> item = widget.question.questionGroup[i];
			if(typeIndex.toString() == item.keys.toList()[0]) {
				result.add(TextSpan(
					text: item.values.toList()[0] + ' ',
					style: TextStyle(
						color: Colors.red,
					)
				));
			} else {
				result.add(TextSpan(
					text: item.values.toList()[0] + ' ',
					style: TextStyle(
						color: MyColors.colorPrimary,
					)
				));
			}
		}
		return result;
	}

	List<TextSpan> _buildTextAnswer() {
		if(mapUserAnswers.isEmpty) {
			return List();
		}

		List<TextSpan> result = List();
		for(int i = 0; i < mapUserAnswers.keys.length; i++) {
			String item = mapUserAnswers[mapUserAnswers.keys.toList()[i]];
			result.add(TextSpan(
				text: item + ' ',
				style: TextStyle(
					color: MyColors.colorPrimary,
				)
			));
		}
		return result;
	}

	String _getTextHint() {
		if(widget.question != null && widget.question.answerGroup != null) {
			for(var map in widget.question.answerGroup) {
				if(map.containsKey(typeIndex.toString())) {
					return map[typeIndex.toString()];
				}
			}
		}
		return '';
	}

  ///
  /**
   * Question example:
   * {
			"id": 1,
			"type": "Typing_Sub",
			"question_picture": null,
			"question_group": [
			{
			"1": "VẤN ĐỀ"
			},
			{
			"2": "VỚI"
			},
			{
			"3": "TIỀN BẠC"
			}
			],
			"answer_group": [
			{
			"1": "PROBLEM/ONE TROUBLE/ONE PROBLEM/A TROUBLE/A PROBLEM/1 TROUBLE/1 PROBLEM/THE TROUBLE/THE PROBLEM/ TROUBLE/ PROBLEM/THE TROUBLES/THE PROBLEMS/ TROUBLES/ PROBLEMS"
			},
			{
			"2": "WITH"
			},
			{
			"3": "MONEY"
			}
			],
			"show_hint": true,
			"question_audio": "http://media.tataenglish.com/Listening/WRITING/Audio/WRITING_7.3_c.mp3"
			}
   */
  ///
  @override
  Widget build(BuildContext context) {
		final indicatorValue = (widget.questionIdx + 1) / widget.stage.questions.length;
		return Container(
			padding: EdgeInsets.all(10),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				mainAxisAlignment: MainAxisAlignment.center,
				children: <Widget>[
					/// header section
					Row(
						children: <Widget>[
							Container(
								alignment: Alignment(0, 0),
								width: 32,
								height: 32,
								decoration: BoxDecoration(
										shape: BoxShape.circle,
										color: MyColors.colorPrimary
								),
								child: Text('${widget.questionIdx + 1}',
									style: TextStyle(
											color: Colors.white,
											fontWeight: FontWeight.bold,
											fontSize: 16
									),
								),
							),
							CommonWidget.smallVerticalDivider,
							Expanded(
								child: LinearProgressIndicator(
									backgroundColor: MyColors.colorPrimaryDark,
									value: indicatorValue,
								),
							),
							CommonWidget.smallVerticalDivider,
							Text('${widget.stage.questions.length - widget.incorrectQuestionCount()}/${widget.stage.questions.length}',
								style: TextStyle(
									fontWeight: FontWeight.bold,
								),
							),
						],
					),
					/// END header section
					CommonWidget.mediumHorizontalDivider,
					/// question section
					CommonWidget.smallHorizontalDivider,
					Row(
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							Icon(FontAwesome.question,
								color: MyColors.colorPrimary,
								size: 30,
							),
							CommonWidget.smallVerticalDivider,
							Flexible(
								child: RichText(
									textAlign: TextAlign.start,
									text: TextSpan(
										style: TextStyle(
											fontSize: 16,
											fontWeight: FontWeight.w600,
											color: MyColors.colorPrimary,
										),
										children: _buildTextQuestion(),
									),
								),
							),
						],
					),
					CommonWidget.smallHorizontalDivider,
					Row(
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							Icon(FontAwesome.pencil,
								color: MyColors.colorPrimary,
								size: 30,
							),
							CommonWidget.smallVerticalDivider,
							Flexible(
								child: RichText(
									textAlign: TextAlign.start,
									text: TextSpan(
										style: const TextStyle(
												fontSize: 16,
												fontWeight: FontWeight.w600
										),
										children: _buildTextAnswer(),
									),
								),
							),
						],
					),
					CommonWidget.smallHorizontalDivider,
					Row(
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							Icon(FontAwesome.keyboard_o,
								color: MyColors.colorPrimary,
								size: 30,
							),
							CommonWidget.smallVerticalDivider,
							Expanded(
								child: TextFormField(
									controller: _controller,
									maxLines: 1,
									keyboardType: TextInputType.text,
									autofocus: true,
									textInputAction: TextInputAction.done,
									onFieldSubmitted: (value) => _validateAnswer(),
									focusNode: _focusNode,
								),
							),
						],
					),
					CommonWidget.smallHorizontalDivider,
					correctAnswer == ANSWER_INCORRECT ?
					Container(
						child: Center(
							child: Text('#${mapIncorrectAnswerCount[typeIndex.toString()]} ${currAnswer}',
								style: TextStyle(
									color: Colors.red,
									fontWeight: FontWeight.w400,
								),
							),
						),
					) : Container(),
					/// END question section
					/// manipulation section
					CommonWidget.smallHorizontalDivider,
					_showHint && !Utils.isEmptyString(_getTextHint()) ?
					RichText(
						textAlign: TextAlign.center,
						text: TextSpan(
							style: DefaultTextStyle.of(context).style,
							children: [
								TextSpan(
									text: 'Đáp án: ',
								),
								TextSpan(
									text: _getTextHint(),
									style: TextStyle(
										fontStyle: FontStyle.italic,
										fontSize: 16,
									),
								),
							],
						),
					) : Text('',
						style: TextStyle(
							fontSize: 16,
						),
					),
					CommonWidget.smallHorizontalDivider,
					Expanded(
						child: Column(
							children: <Widget>[
//							_buildPrecisionIcon(),
								Container(),
							],
						),
					),
					/// END manipulation section
					/// bottom section
					Row(
						mainAxisAlignment: MainAxisAlignment.spaceBetween,
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							_buildPlayQuestionAudioButton(),
							_buildPrecisionIcon(),
							Container(
								width: 50,
								child: FlatButton(
									textColor: MyColors.colorPrimary,
									padding: EdgeInsets.all(10.0),
									child: Text("OK",
										style: TextStyle(
												fontWeight: FontWeight.bold,
												fontSize: 18
										),
									),
									onPressed: () {
										_validateAnswer();
									},
								),
							),
						],
					),
					/// END bottom section
				],
			),
		);
  }
}

class _TypingSubAnswer {
	final String key;
	final String answer;

  _TypingSubAnswer({this.key, this.answer});
}