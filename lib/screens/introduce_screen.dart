import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:http/http.dart' as http;
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/utils/api.dart';

import 'base/abstract_state.dart';
import 'invite_friend_screen.dart';

class IntroduceScreen extends StatefulWidget {

  @override
  _IntroduceScreenState createState() => _IntroduceScreenState();

}

class _IntroduceScreenState extends AbstractState<IntroduceScreen> {

  Introduce _introduce;

  @override
  bool shouldInitFCM() {
    return true;
  }

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    _getIntroduces();
  }

  void _getIntroduces() async {
    setState(() {
      loaded = AbstractState.LOAD_LOADING;
    });
    await Api.getIntroduces(accessToken.accessToken).then((_introduce) {
      setState(() {
        this._introduce = _introduce;
        loaded = AbstractState.LOAD_FINISH;
      });
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        loaded = AbstractState.LOAD_FINISH;
      });
    });
  }

  List<Widget> _buildContent() {
    List<Widget> list = List.of([
      Padding(
        padding: const EdgeInsets.all(10),
        child: const Text("""CHÍNH SÁCH ƯU TIÊN NỘI BỘ
NHÂN DỊP SINH NHẬT 9 CƠ SỞ CỦA TATA
(Chính sách ưu tiên khủng nhất chỉ có 1 lần duy nhất từ khi Tata thành lập đến nay!)

\tNếu bạn bè của em nằm trong danh sách được em giới thiệu bên dưới mà đăng ký học thì sẽ được giảm 9% học phí từ 4,800,000đ xuống còn 4,380,000đ. Ngoài ra còn được tặng khóa học kỹ năng thiết yếu trong môi trường học tập và làm việc trị giá 1.950.000đ.""",
          textAlign: TextAlign.left,
        ),
      ),
      Divider(
        color: MyColors.darkGray,
        height: 1.5,
      ),
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 70,
            alignment: const AlignmentDirectional(0, 0),
            child: const Text('STT',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
              child: Container(
                alignment: Alignment.centerLeft,
                child: const Text("""Họ và tên
Số điện thoại""",
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
          ),
          Container(
            width: 100,
            alignment: Alignment.centerLeft,
            child: Text("""Đã đăng ký
Đã nhận quà""",
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            width: 40,
            alignment: const AlignmentDirectional(0, 0),
            child: IconButton(
              icon: Icon(FontAwesome.question_circle),
              onPressed: _showInfo,
            ),
          ),
        ],
      ),
      Divider(
        color: MyColors.darkGray,
        height: 1,
      )
    ], growable: true);
    _introduce?.introduces?.asMap()?.forEach((index, _introduce) {
      list.add(_IntroduceItem(order: index + 1, userIntroduce: _introduce));
      list.add(Divider(
        color: MyColors.darkGray,
        height: 1,
      )
      );
    });
    return list;
  }

  void _showInfo() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Thông báo'),
            content: const Text('Đã đăng ký? - Tức là bạn mình giới thiệu đã đăng ký (nộp đủ học phí) hay chưa.\nĐã nhận quà? - Tức là sau khi bạn của mình đã đăng ký, thì mình đã nhận quà là 420.000đ hay chưa.',
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
    );
  }

  Future _navigateInviteFriendScreen() async {
    final int result = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => InviteFriendScreen(centers: _introduce.centers)));
    if(result == 1) {
      showSnackBar('Giới thiệu bạn bè thành công! Xin cảm ơn bạn!');
      _getIntroduces();
    }
  }

  @override
  Widget build(BuildContext context) {
    if(loaded == AbstractState.LOAD_LOADING) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: _buildContent(),
              ),
            )
          ),
          Container(
              margin: EdgeInsets.only(top: 10, left: 40, right: 40, bottom: 10),
              child: AbsorbPointer(
                absorbing: loaded == 1,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Colors.red,
                  child: Text("Giới thiệu bạn bè"),
                  padding: EdgeInsets.all(15),
                  onPressed: _introduce == null || _introduce.centers == null ? null : _navigateInviteFriendScreen,
                ),
              )
          ),
        ],
      );
    }
  }
}

class _IntroduceItem extends StatelessWidget {

  final int order;
  final UserIntroduce userIntroduce;

  const _IntroduceItem({Key key, this.order, this.userIntroduce})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 70,
            alignment: AlignmentDirectional(0, 0),
            child: Text(order.toString(),
            ),
          ),
          Expanded(
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text('${userIntroduce?.name}\n${userIntroduce?.phoneNumber}',
                ),
              )
          ),
          Container(
            width: 140,
            alignment: Alignment.centerLeft,
            child: Text(userIntroduce?.status,
            ),
          ),
        ],
      ),
    );
  }

}