import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';

import 'base/abstract_state.dart';
import 'exercise_detail_screen.dart';

@deprecated
class GoldenWeekScreen extends StatefulWidget {
  @override
  _GoldenWeekScreenState createState() => _GoldenWeekScreenState();

}

@deprecated
class _GoldenWeekScreenState extends AbstractState<GoldenWeekScreen> {

  int _loaded = 0;

  StudentStat _studentStat;

  ScrollController _controller;

  int page = 1;

  double _scrollOffset = 0;

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    page = 1;
    _loadData();
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    page = 1;

    super.initState();
  }

  @override
  void dispose() {
    _controller.removeListener(_scrollListener);
    _controller.dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      print('reach the bottom');

      if(_loaded != 1) {
        page++;
        _loadData();
      }
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      print('reach the top');
    }
  }

  void _loadData() {
    if(accessToken != null) {
      setState(() {
        _loaded = 1;
      });
      Api.getGoldenWeekListTest(accessToken.accessToken, page).then((_studentStat) {
        setState(() {
          if(this._studentStat == null) {
            this._studentStat = _studentStat;
          } else {
            if(_studentStat.diligenceScore != null) {
              this._studentStat.diligenceScore = _studentStat.diligenceScore;
            }
            if(_studentStat.usersRanking != null) {
              this._studentStat.usersRanking = _studentStat.usersRanking;
            }
            if(_studentStat.chart != null) {
              this._studentStat.chart = _studentStat.chart;
            }
            if(_studentStat.listTest != null) {
              if(_studentStat.listTest.lastTestId != null) {
                this._studentStat.listTest.lastTestId = _studentStat.listTest.lastTestId;
              }
              if(_studentStat.listTest.lastIndex != null) {
                this._studentStat.listTest.lastIndex = _studentStat.listTest.lastIndex;
              }
              if(_studentStat.listTest.tests != null) {
                this._studentStat.listTest.tests.addAll(_studentStat.listTest.tests);
              }
            }
          }
          _loaded = 2;
        });
      }, onError: (error) {
        print('error: $error');
        showSnackBar(error.toString());
        setState(() {
          _loaded = 2;
        });
      });
    }
  }

  void _navDetailScreen(Test test, String testDisplay) {
    if(test == null) {
      showSnackBar('Vui lòng chọn bài tập');
      return;
    }
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ExerciseDetailScreen(test: test, testDisplay: testDisplay)));
  }

  @override
  Widget build(BuildContext context) {
    if(_loaded == 1) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else if(_studentStat == null) {
      return new Container(
        alignment: AlignmentDirectional(0, 0),
        padding: EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CommonWidget.faWarningIcon,
            CommonWidget.mediumVerticalDivider,
            const Text('Bạn chưa được phép làm bài tập phần này!',
              style: const TextStyle(
                fontSize: 16,
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
        child: ListView.builder(
          key: PageStorageKey('testList'),
          // haimt persist scroll position, perfect
          controller: _controller,
          itemCount: _studentStat == null || _studentStat.listTest == null ||
              _studentStat.listTest.tests == null ? 1 : _studentStat.listTest.tests.length + 1,
          itemBuilder: (context, index) {
            var testIdx = index - 1;
            switch (index) {
              case 0:
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: MyColors.titleBackground
                      ),
                      alignment: AlignmentDirectional(0, 0),
                      child: Text("Lộ trình bài tập Speaking".toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                );
              default:
                var test = _studentStat.listTest.tests[testIdx];
                final String testDisplay = '${test.name}' + (test.workTime == 0 ? '' : ' (${test.workTime} phút)');
                return InkWell(
                    onTap: () {
                      print('selected $index');
                      _navDetailScreen(test, testDisplay);
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 16, bottom: 16, left: 10, right: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text(testDisplay),
                          ),
                          SizedBox(
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Colors.green),
                              backgroundColor: Colors.red,
                              strokeWidth: 10,
                              value: test == null || test.testMoreInfo == null || test.testMoreInfo.successPercent == null ?
                                  0 : (test.testMoreInfo.successPercent / 100).toDouble(),
                            ),
                            height: 10.0,
                            width: 10.0,
                          )
                        ],
                      ),
                    )
                );
            }
          },
        ),
      );
    }
  }

}