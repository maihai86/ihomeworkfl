import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ihomework/domain/access_token.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/utils.dart';
import 'login_screen.dart';
import 'exercise_main_screen.dart';
import 'package:ihomework/screens/base/abstract_state.dart';

class SplashScreen extends StatefulWidget {

  @override
  _SplashScreen createState() => _SplashScreen();

}

class _SplashScreen extends AbstractState<SplashScreen> {
  int start = 0;
//  SharedPreferences prefs;
  int loggedIn = 0;

  _startTime() async {
    var duration = Duration(seconds: 2);
    return Timer(duration, () {
      if(accessToken == null || accessToken.accessToken == null || accessToken.accessToken.isEmpty) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => LoginScreen()));
      } else {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => ExerciseMainScreen()));
      }
    });
  }

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    print('accessToken: ${prefs.getString(PrefsKey.KEY_ACCESS_TOKEN)}');
    print('rememberMe: ${prefs.getBool(PrefsKey.KEY_REMEMBER_ME)}');
    bool rememberMe = prefs.getBool(PrefsKey.KEY_REMEMBER_ME);
    if(rememberMe == null || !rememberMe) {
      prefs.remove(PrefsKey.KEY_ACCESS_TOKEN);
      setState(() {
        accessToken = null;
      });
    }

    _startTime();
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  @override
  bool shouldGetFCMToken() => true;

//  @override
//  void initState() {
//    super.initState();
//
//    _loadSharedPreferences();
//  }
//
//  void _loadSharedPreferences() async {
//    prefs = await SharedPreferences.getInstance();
//
//    print('accessToken: ${prefs.getString(Constants.KEY_ACCESS_TOKEN)}');
//    print('rememberMe: ${prefs.getBool(Constants.KEY_REMEMBER_ME)}');
//    bool rememberMe = prefs.getBool(Constants.KEY_REMEMBER_ME);
//    if(rememberMe == null || !rememberMe) {
//      prefs.remove(Constants.KEY_ACCESS_TOKEN);
//    }
//
//    _startTime();
//  }
//
//  @override
//  void dispose() {
//    super.dispose();
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Container(
        child: Hero(
          tag: 'logoImage',
          child: CommonWidget.appLogo,
        ),
        alignment: AlignmentDirectional(0, -0.5),
        color: MyColors.colorPrimary,
      ),
    );
  }

}