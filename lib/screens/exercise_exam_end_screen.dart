import 'dart:async';
import 'package:flutter/material.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/utils/api.dart';

import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamEndScreen extends StatefulWidget {

  final Exercise exercise;

  final Stage stage;

  final Function() numIncorrect;

  final int secondExam;

  final void Function(String url) playSoundUrl;

  final void Function(String url) playSmallSoundUrl;

  final Future<int> Function() stopSoundUrl;

  final void Function() finish;

  final int Function() getGameScore;

  final bool Function() isGamePass;

  ExerciseExamEndScreen({
    key,
    this.exercise,
    this.stage,
    this.numIncorrect,
    this.secondExam,
    this.playSoundUrl,
    this.playSmallSoundUrl,
    this.stopSoundUrl,
    this.finish,
    this.getGameScore,
    this.isGamePass,
  }): assert(playSoundUrl != null),
      assert(playSmallSoundUrl != null),
      assert(stopSoundUrl != null),
      super(key: key);

  @override
  _ExerciseExamEndScreenState createState() => _ExerciseExamEndScreenState();
}

class _ExerciseExamEndScreenState extends AbstractState<ExerciseExamEndScreen> {

  int _loaded = 0;
  DoExerciseResult result;

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();
    _postSubmitExerciseExamResult();
  }

  @override
  bool shouldInitFCM() {
    return false;
  }

//  Future _postSubmitExerciseExamResult() async {
//    setState(() {
//      _loaded = 1;
//    });
//    await Api.postSubmitExerciseExamResult(accessToken.accessToken, DoExerciseResultDto(stageId: widget.stage.stageId, numErrors: widget.numIncorrect, timeDo: widget.secondExam)).then((_result) {
//      setState(() {
//        _loaded = 2;
//      });
////      finishQuestion();
//    }, onError: (error) {
//      print('error: $error');
//      showSnackBar(error.toString());
//      setState(() {
//        _loaded = 2;
//      });
//    });
//  }

  Future _postSubmitExerciseExamResult() async {
    setState(() {
      _loaded = 1;
    });
    String questionType = widget.stage.questions[0].type;
    Api.postSubmitExerciseExamResult(accessToken.accessToken,
        questionType == QuestionType.GAME ?
        DoExerciseResultDto(stageId: widget.stage.stageId,
            numErrors: widget.numIncorrect(),
            timeDo: widget.secondExam,
            questionType: questionType,
            gameScore: widget.getGameScore(),
            gamePass: widget.isGamePass()) :
        DoExerciseResultDto(stageId: widget.stage.stageId,
            numErrors: widget.numIncorrect(),
            timeDo: widget.secondExam,
            questionType: questionType)).then((_result) {
      setState(() {
        this.result = _result;
        _loaded = 2;
      });

      if(result.pass) {
        widget.playSmallSoundUrl(Constants.WIN_TUNE_URLS[0]);
      } else {
        widget.playSmallSoundUrl(Constants.FAIL_TUNE_URLS[0]);
      }
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        _loaded = 2;
      });
    });
  }

  void _getStageDetail(String stageIdExp) async {
    setState(() {
      _loaded = 1;
    });
    await Api.getStageDetailByStageExp(accessToken.accessToken, stageIdExp).then((_stage) {
      setState(() {
        _loaded = 2;
      });
      Navigator.pop(context);
      Navigator.push(context,
        MaterialPageRoute(
          builder: (context) => ExerciseExamPageScreen(ExerciseExamPageScreenState.TYPE_BY_STAGE, stage: _stage)));
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        _loaded = 2;
      });
    });
  }

  Future _onTryAgain() async {
    print("try again");
    await widget.stopSoundUrl();
    Navigator.pop(context);
    Navigator.push(context,
        MaterialPageRoute(
            builder: (context) => ExerciseExamPageScreen(ExerciseExamPageScreenState.TYPE_BY_STAGE, stage: widget.stage)));
  }

  Future _onNextExercise() async {
    print("next");
    await widget.stopSoundUrl();
    _getStageDetail(result.next);
  }

  Future _finish() async {
    await widget.stopSoundUrl();
    widget.finish();
  }

  @override
  Widget build(BuildContext context) {
    if(_loaded == 1) {
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    } else if(result == null) {
      return Scaffold(
        body: Center(
          child: Text('Không thể gửi kết quả làm bài tập!'),
        ),
      );
    } else {
      String done = 'DONE';
      String gif = Utils.getRandomPassExerciseGifUrl();
      if(!result.pass) {
        done = 'FAILED';
        gif = Utils.getRandomFailedExerciseGifUrl();
      }
      Widget nextButton = Container();
      if(result != null && Utils.isEmptyString(result.next)) {
        nextButton = SizedBox(
          width: 150,
          child: RaisedButton(
            color: Colors.green,
            onPressed: _finish,
            child: Text('Kết thúc',
              style: TextStyle(
                  color: Colors.white
              ),
            ),
          ),
        );
      } else if(result != null && !Utils.isEmptyString(result.next)) {
        nextButton = SizedBox(
          width: 150,
          child: RaisedButton(
            color: Colors.green,
            onPressed: _onNextExercise,
            child: Text('Làm bài tiếp theo',
              style: TextStyle(
                  color: Colors.white
              ),
            ),
          ),
        );
      }
      return Scaffold(
        body: Container(
          alignment: AlignmentDirectional(0, 0.2),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(gif,
              ),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(done,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 50,
                  color: Colors.red,
                ),
                textAlign: TextAlign.center,
              ),
              Divider(
                color: Colors.transparent,
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: result.pass ? <Widget>[
                  SizedBox(
                    width: 150,
                    child: RaisedButton(
                      color: Colors.red,
                      onPressed: () async {
                        await _onTryAgain();
                      },
                      child: Text('Làm lại',
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                    ),
                  ),
                  CommonWidget.smallVerticalDivider,
                  nextButton,
                ] : <Widget>[
                  SizedBox(
                    width: 150,
                    child: RaisedButton(
                      color: Colors.red,
                      onPressed: () async {
                        await _onTryAgain();
                      },
                      child: Text('Làm lại',
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
  }
}