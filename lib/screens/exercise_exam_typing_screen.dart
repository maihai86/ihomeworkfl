import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamTypingScreen extends AbstractExerciseExamScreen {

//  final Stage stage;
//
//  final int questionIdx;
//
//  final Question question;
//
//  final int Function() incorrectQuestionCount;
//
//  final void Function(int questionIdx, bool correctAnswer) nextQuestion;
//
//  final void Function(String url) playSoundUrl;
//
//	final void Function(List<String> urls, int soundIndex) playSoundUrls;

  final void Function(int questionIndex, String url) playSoundUrlNew;

//  final void Function(String url) playSmallSoundUrl;
//
//  final void Function() stopSoundUrl;

  final void Function(int questionIndex) stopSoundUrlNew;

//  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

  final void Function(int index) scrollToIndex;

	final int Function() currentQuestionIndex;

	final FocusNode focusNode;

	final TextEditingController textEditingController;

  final Function() nextQuestionOrValidateAllAnswer;

  ExerciseExamTypingScreen({
    key,
    stage,
    questionIdx,
    question,
    incorrectQuestionCount,
    nextQuestion,
    playSoundUrl,
    playSoundUrls,
    playSmallSoundUrl,
    stopSoundUrl,
    updateCorrectAnswer,
    this.scrollToIndex,
		this.currentQuestionIndex,
		this.playSoundUrlNew,
		this.stopSoundUrlNew,
		this.focusNode,
		this.textEditingController,
		this.nextQuestionOrValidateAllAnswer,
  }): /*assert(stage != null),*/
//      assert(questionIdx != null),
//      assert(question != null),
//      assert(nextQuestion != null),
//      assert(playSoundUrl != null),
//      assert(playSoundUrls != null),
//      assert(playSmallSoundUrl != null),
//      assert(stopSoundUrl != null),
//      assert(updateCorrectAnswer != null),
      super(
				key: key,
				stage: stage,
				questionIdx: questionIdx,
				question: question,
				incorrectQuestionCount: incorrectQuestionCount,
				nextQuestion: nextQuestion,
				playSoundUrl: playSoundUrl,
				playSoundUrls: playSoundUrls,
				playSmallSoundUrl: playSmallSoundUrl,
				stopSoundUrl: stopSoundUrl,
				updateCorrectAnswer: updateCorrectAnswer,);

  @override
  State<StatefulWidget> createState() => ExerciseExamTypingScreenState();

}

class ExerciseExamTypingScreenState extends AbstractExerciseScreenState<ExerciseExamTypingScreen> {

  static const int ANSWER_NOT_RECORDED = 0;
  static const int ANSWER_INCORRECT = 1;
  static const int ANSWER_CORRECT = 2;

  int correctAnswer = ANSWER_NOT_RECORDED;
  int incorrectAnswerCount = 0;

	bool _showHint = false;
  Timer _timer;
  ///

  TextEditingController _controller;
	FocusNode _focusNode;

  bool answerTextFieldEnable = false;
  String answerTextFieldErrorMessage = null;
  
  @override
  void initState() {
    super.initState();

    _controller = TextEditingController();
		_focusNode = FocusNode();
		_playQuestionAudio();
  }

  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();
    _controller.dispose();
		_focusNode.dispose();

    super.dispose();
  }

	@override
	bool shouldInitFCM() {
		return false;
	}

	@override
	void afterLoadSharedPreferences() {
		super.afterLoadSharedPreferences();

		FocusScope.of(context).requestFocus(_focusNode);
	}

	Widget _buildPlayQuestionAudioButton() {
    if(widget.question == null || widget.question.questionAudio.isEmpty) {
      return CommonWidget.emptyButtonContainer;
    } else if(playSoundUrlState == PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_up,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () async {
					await widget.stopSoundUrl();
        },
      );
    } else if(playSoundUrlState != PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_off,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () {
          _playQuestionAudio();
        },
      );
    } else {
      return CommonWidget.emptyButtonContainer;
    }
  }

  Widget _buildPrecisionIcon() {
    if(correctAnswer == ANSWER_CORRECT) {
      // haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.check,
            color: Colors.green,
            size: 68,
          ),
        ],
      );
    } else if(correctAnswer == ANSWER_INCORRECT) {
      // haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.close,
            color: Colors.red,
            size: 68,
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  void _playQuestionAudio() {
  	print('$runtimeType, _playQuestionAudio:${widget.question.questionAudio}');
    if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
			widget.playSoundUrls(widget.question.questionAudio, 0);
    }
  }

  void _validateAnswer() {
		if (Utils.normalizeAnswer(_controller.text.trim().toLowerCase()) == Utils.normalizeAnswer(widget.question.answerText.trim().toLowerCase())) {
			if(_showHint) {
				setState(() {
					this._showHint = false;
					this.incorrectAnswerCount = 0;
					this.correctAnswer = ANSWER_CORRECT;
				});
			} else {
				setState(() {
					this.correctAnswer = ANSWER_CORRECT;
				});
			}
			hideKeyboard();
			widget.updateCorrectAnswer(widget.questionIdx, true);
			widget.playSmallSoundUrl(Constants.CORRECT_URLS[0]);

			Timer(Duration(milliseconds: 500), () async {
				await widget.stopSoundUrl();
				widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
			});
		} else {
			incorrectAnswerCount++;
			if (incorrectAnswerCount >= Constants.INCORRECT_ANSWER_MAX) {
				setState(() {
					_showHint = true;
					this.correctAnswer = ANSWER_INCORRECT;
				});
			} else {
				setState(() {
					this.correctAnswer = ANSWER_INCORRECT;
				});
			}
			widget.updateCorrectAnswer(widget.questionIdx, false);
			widget.playSmallSoundUrl(Constants.INCORRECT_URLS[0]);
		}
	}

	void updateAnswerStatus(bool enable, String errorMsg) {
  	setState(() {
			this.answerTextFieldEnable = enable;
			this.answerTextFieldErrorMessage = errorMsg;
  	});
	}

  ///
  /**
   * Question example:
   * {
      "id": 1,
      "type": "Typing",
      "question_picture": "/ˈɡriːnhaʊs/",
      "question": "(nc)",
      "answer_text": "greenhouse",
      "hint_text": "1",
      "question_audio": "http://media.tataenglish.com/audio_en/g/greenhouse_nc_uk.mp3"
      }
   */
  ///
  @override
  Widget build(BuildContext context) {
		final indicatorValue = (widget.questionIdx + 1) / widget.stage.questions.length;
		return Container(
				padding: EdgeInsets.all(10),
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						/// header section
						Row(
							children: <Widget>[
								Container(
									alignment: Alignment(0, 0),
									width: 32,
									height: 32,
									decoration: BoxDecoration(
											shape: BoxShape.circle,
											color: MyColors.colorPrimary
									),
									child: Text('${widget.questionIdx + 1}',
										style: TextStyle(
											color: Colors.white,
											fontWeight: FontWeight.bold,
											fontSize: 16
										),
									),
								),
								CommonWidget.smallVerticalDivider,
								Expanded(
									child: LinearProgressIndicator(
										backgroundColor: MyColors.colorPrimaryDark,
										value: indicatorValue,
									),
								),
								CommonWidget.smallVerticalDivider,
								Text('${widget.stage.questions.length - widget.incorrectQuestionCount()}/${widget.stage.questions.length}',
									style: TextStyle(
										fontWeight: FontWeight.bold,
									),
								),
							],
						),
						/// END header section
						CommonWidget.mediumHorizontalDivider,
						/// question section
						Utils.isImageUrl(widget.question.questionPicture) ?
						ConstrainedBox(
							constraints: BoxConstraints(
								maxHeight: 100
							),
							child: Image.network(widget.question.questionPicture),
						) :
						Container(
							padding: EdgeInsets.symmetric(
									horizontal: 0.0,
									vertical: 20
							),
							alignment: AlignmentDirectional(0, 0),
							child: Text(widget.question.questionPicture,
								style: TextStyle(
										fontSize: 32,
										fontWeight: FontWeight.w400
								),
							),
							decoration: BoxDecoration(
								border: Border.all(
									color: MyColors.colorPrimary,
									width: 1.0
								)
							),
						),
						CommonWidget.smallHorizontalDivider,
						Row(
							crossAxisAlignment: CrossAxisAlignment.center,
							children: <Widget>[
								Icon(FontAwesome.question,
									color: MyColors.colorPrimary,
									size: 30,
								),
								CommonWidget.smallVerticalDivider,
								Expanded(
									child: Text(widget.question.question,
										style: TextStyle(
											fontWeight: FontWeight.w600,
										),
									),
								),
//									CommonWidget.smallVerticalDivider,
//									_buildPlayQuestionAudioButton(),
							],
						),
						CommonWidget.smallHorizontalDivider,
						Row(
							crossAxisAlignment: CrossAxisAlignment.center,
							children: <Widget>[
								Icon(FontAwesome.keyboard_o,
									color: MyColors.colorPrimary,
									size: 30,
								),
								CommonWidget.smallVerticalDivider,
								Expanded(
									child: TextFormField(
										controller: _controller,
										maxLines: 1,
										keyboardType: TextInputType.text,
										autofocus: true,
										textInputAction: TextInputAction.done,
										onFieldSubmitted: (value) => _validateAnswer(),
										focusNode: _focusNode,
									),
								),
//									CommonWidget.smallVerticalDivider,
//									_buildPlayQuestionAudioButton(),
							],
						),
						/// END question section
						/// manipulation section
						CommonWidget.smallHorizontalDivider,
						_showHint && !Utils.isEmptyString(widget.question.hintText) ?
						RichText(
							textAlign: TextAlign.center,
							text: TextSpan(
								style: DefaultTextStyle.of(context).style,
								children: [
									TextSpan(
										text: 'Gợi ý: ',
									),
									TextSpan(
										text: widget.question.hintText,
										style: TextStyle(
											fontWeight: FontWeight.bold,
											color: Colors.red,
											fontSize: 20,
										),
									),
								],
							),
						) : Text('',
							style: TextStyle(
								fontSize: 20,
							),
						),
						CommonWidget.smallHorizontalDivider,
						Expanded(
							child: Column(
								children: <Widget>[
//										_buildPrecisionIcon(),
									Container(),
								],
							),
						),
						/// END manipulation section
						/// bottom section
						Row(
							mainAxisAlignment: MainAxisAlignment.spaceBetween,
							crossAxisAlignment: CrossAxisAlignment.center,
							children: <Widget>[
								_buildPlayQuestionAudioButton(),
								_buildPrecisionIcon(),
								Container(
									width: 50,
									child: FlatButton(
										textColor: MyColors.colorPrimary,
										padding: EdgeInsets.all(10.0),
										child: Text("OK",
											style: TextStyle(
												fontWeight: FontWeight.bold,
												fontSize: 18
											),
										),
										onPressed: () {
											_validateAnswer();
										},
									),
								),
							],
						),
						/// END bottom section
					],
				),
		);
  }
}