import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/widgets/pin_view.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';
import 'exercise_exam_typing_screen.dart';

class ExerciseExamOptionAllScreen extends StatefulWidget {

  final Stage stage;


  final int Function() incorrectQuestionCount;

  final void Function(int questionIdx, bool correctAnswer) nextQuestion;

  final void Function() finishQuestion;

//  final void Function(String url) playSoundUrl;
//
//  final void Function() stopSoundUrl;

  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

	final ExerciseExamPageScreenState exerciseExamPageScreenState;

	final Function() numIncorrect;

	final int secondExam;

	ExerciseExamOptionAllScreen({
    key,
    this.stage,
    this.incorrectQuestionCount,
    this.nextQuestion,
    this.finishQuestion,
//    this.playSoundUrl,
//    this.stopSoundUrl,
    this.updateCorrectAnswer,
		this.exerciseExamPageScreenState,
		this.numIncorrect,
		this.secondExam,
  }): assert(stage != null),
      assert(nextQuestion != null),
//      assert(playSoundUrl != null),
//      assert(stopSoundUrl != null),
      assert(updateCorrectAnswer != null),
      super(key: key)
  ;

  @override
  State<StatefulWidget> createState() => ExerciseExamOptionAllScreenState();

}

class ExerciseExamOptionAllScreenState extends AbstractExerciseScreenState<ExerciseExamOptionAllScreen> {

	static const int ANSWER_NOT_RECORDED = 0;
	static const int ANSWER_INCORRECT = 1;
	static const int ANSWER_CORRECT = 2;

	int correctAnswer = ANSWER_NOT_RECORDED;
	int incorrectAnswerCount = 0;

  AutoScrollController _controller;
  List<Widget> _pages;
  List<GlobalKey<_OptionItemState>> _globalKeys;
	List<List<GlobalKey<_OptionSelectionItemState>>> _globalKeySelections;
  int _currQuestionIdx = 0;
	List<_OptionAnswer> answers;

  bool _initItemsState = false;

  int _answerCount = 0;

  int getCurrQuestionIndex() {
  	return _currQuestionIdx;
  }

	/// haimt: audioPlayer
	/// in order to play remote url
	bool isLocal = false;
	PlayerMode mode = PlayerMode.MEDIA_PLAYER;

	AudioPlayer _audioPlayer;

	StreamSubscription _durationSubscription;
	StreamSubscription _positionSubscription;
	StreamSubscription _playerCompleteSubscription;
	StreamSubscription _playerErrorSubscription;
	StreamSubscription _playerStateSubscription;

	String soundUrl = null;
	List<String> soundUrls = null;
	int currSoundIndex = 0;
//	String smallSoundUrl;
	///

	/// End screen
	int _loaded = 0;
	DoExerciseResult result;
	int _submit = 0;

  @override
  void initState() {
    super.initState();

    _controller = AutoScrollController();
		_initAudioPlayer();

		if(widget.stage.type != null || widget.stage.type == StageType.ALL) {
			_playStageAudio();
		}
  }

  @override
  void dispose() {
    _controller.dispose();
    stopSoundUrl();
		_disposeAudioPlayer();

    super.dispose();
  }

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    _initExercise();
		_initItems();
  }

  @override
  bool shouldInitFCM() {
    return false;
  }

  int _buildItemCount() {
    return widget.stage.questions.length;
  }

  Future scrollToIndex(int index) async {
  	if(index >= _pages.length) {
			_currQuestionIdx = 0;
		} else {
			_currQuestionIdx = index;
		}
    await _controller.scrollToIndex(_currQuestionIdx);
		setState(() => _answerCount = _getAnsweredCount());
  }

  void _initExercise() {
    _pages = List(widget.stage == null || widget.stage.questions == null ? 0 : widget.stage.questions.length);
		_globalKeys = List(_pages.length);
		_globalKeySelections = List(_pages.length);
		answers = List.generate(_pages.length, (index) => _OptionAnswer(answer: null, selectedIndex: -1, result: null));
  }

	void _initItems() {
		for(var index = 0; index < _pages.length; index++) {
			Question question = widget.stage.questions[index];
			if (question == null) {
				_pages[index] = Container();
			} else {
				switch (question.type) {
					case QuestionType.OPTION:
						_globalKeys[index] = GlobalKey<_OptionItemState>();
						_pages[index] = AutoScrollTag(
							key: ValueKey(index),
							controller: _controller,
							index: index,
							child: _OptionItem(
								key: _globalKeys[index],
								questionIdx: index,
								question: question,
								exerciseExamOptionAllScreenState: this,
							),
						);
						break;
					default:
						_globalKeys[index] = GlobalKey();
						_pages[index] = CommonWidget.emptyScreenWithNotice('Dạng bài tập chưa được hỗ trợ: ${question.type}!!');
						break;
				}
			}
		}
		setState(() {
		  _initItemsState = true;
		});
	}

	void _initAudioPlayer() {
		AudioPlayer.logEnabled = false;
		_audioPlayer = AudioPlayer(/*mode: mode*/);

		_durationSubscription =
			_audioPlayer.onDurationChanged.listen((duration) {
			});

		_positionSubscription =
			_audioPlayer.onAudioPositionChanged.listen((p) {
			});

		_playerCompleteSubscription =
			_audioPlayer.onPlayerCompletion.listen((event) {
				_onPlaySoundUrlComplete();
			});

		_playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
			print('audioPlayer error : $msg');
			showSnackBar(msg);
			for(var key in _globalKeys) {
				if(key is GlobalKey<_OptionItemState>) {
					key.currentState?.updatePlaySoundUrlState(PlayerState.stopped);
				}
			}
			updatePlaySoundUrlState(PlayerState.stopped);
		});

		_audioPlayer.onPlayerStateChanged.listen((state) {
			if (!mounted) return;
		});
	}

	void _disposeAudioPlayer() {
//		_audioPlayer.stop();
		_audioPlayer.release();
		_durationSubscription?.cancel();
		_positionSubscription?.cancel();
		_playerCompleteSubscription?.cancel();
		_playerErrorSubscription?.cancel();
		_playerStateSubscription?.cancel();
	}

	Widget _buildPrecisionIcon() {
		if(correctAnswer == ANSWER_CORRECT) {
			// haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true'
			return Row(
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Icon(FontAwesome.check,
						color: Colors.green,
						size: 42,
					),
				],
			);
		} else if(correctAnswer == ANSWER_INCORRECT) {
			// haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
			return Row(
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Icon(FontAwesome.close,
						color: Colors.red,
						size: 42,
					),
				],
			);
		} else {
			return Container();
		}
	}

	Widget _buildBottomContainer() {
		if(_loaded == 1) {
			return Container(
				child: Center(
					child: CircularProgressIndicator(),
				),
			);
		} else if(correctAnswer == ANSWER_NOT_RECORDED) {
			return Container(
				padding: EdgeInsets.all(8.0),
				alignment: AlignmentDirectional(0, 0),
				child: Row(
					crossAxisAlignment: CrossAxisAlignment.center,
					mainAxisAlignment: MainAxisAlignment.spaceBetween,
					children: <Widget>[
						_buildPlayStageAudioButton(),
						RaisedButton(
							textColor: Colors.white,
							color: MyColors.colorPrimary,
							child: Text(_getAnsweredCount() == widget.stage.questions.length ?
							"Nộp bài (${_getAnsweredCount()}/${widget.stage.questions.length})" :
							"Tiếp tục (${_getAnsweredCount()}/${widget.stage.questions.length})"),
							padding: EdgeInsets.all(15),
							onPressed: () {
								if(!isSubmitted() && _loaded != 1) {
									nextQuestionOrValidateAllAnswer();
								}
							},
						),
						CommonWidget.emptyButtonContainer,
					],
				)
			);
		} else {
			return Container(
				padding: EdgeInsets.all(8.0),
				alignment: AlignmentDirectional(0, 0),
				child: Row(
					crossAxisAlignment: CrossAxisAlignment.center,
					mainAxisAlignment: MainAxisAlignment.spaceBetween,
					children: <Widget>[
						SizedBox(
							width: 100,
							child: FlatButton(
								onPressed: _onTryAgain,
								child: Text('Làm lại',
									style: TextStyle(
										color: Colors.red,
										fontWeight: FontWeight.bold,
									),
								),
							),
						),
						_buildPrecisionIcon(),
						result == null || Utils.isEmptyString(result.next) ?
						Container():
						SizedBox(
							width: 100,
							child: FlatButton(
								onPressed: _onNextExercise,
								child: Text('Tiếp theo',
									style: TextStyle(
										color: Colors.green,
										fontWeight: FontWeight.bold,
									),
								),
							),
						),
					],
				),
			);
		}
	}

	Widget _buildPlayStageAudioButton() {
		if(widget.stage == null || widget.stage.stageAudio == null || widget.stage.stageAudio.isEmpty) {
			return CommonWidget.emptyButtonContainer;
		} else if(playSoundUrlState == PlayerState.playing) {
			return IconButton(
				icon: Icon(FontAwesome.volume_up,
					color: MyColors.colorPrimary,
				),
				iconSize: 30,
				onPressed: () {
					stopSoundUrls();
				},
			);
		} else if(playSoundUrlState != PlayerState.playing) {
			return IconButton(
				icon: Icon(FontAwesome.volume_off,
					color: MyColors.colorPrimary,
				),
				iconSize: 30,
				onPressed: () {
					_playStageAudio();
				},
			);
		} else {
			return CommonWidget.emptyButtonContainer;
		}
	}

	void _playStageAudio() {
		print('$runtimeType, _playQuestionAudio:${widget.stage.stageAudio}');
		if(widget.stage != null && widget.stage.stageAudio != null && widget.stage.stageAudio.isNotEmpty) {
			playSoundUrls(widget.stage.stageAudio, 0);
		}
	}

	Future playSoundUrl(String url) async {
		this.soundUrl = url;
//    print('$runtimeType.playSoundUrl url:$url');
		final result = await _audioPlayer.play(this.soundUrl, volume: 2.0);
		if(_globalKeys[_currQuestionIdx] is GlobalKey<_OptionItemState>) {
			_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.playing);
		}
		updatePlaySoundUrlState(PlayerState.playing);
		return result;
	}

	Future playSoundUrlNew(int questionIndex, List<String> urls, int soundIndex) async {
		this.soundUrl = null;
		this.soundUrls = urls;
		this.currSoundIndex = soundIndex;
		this._currQuestionIdx = questionIndex;
//    print('$runtimeType.playSoundUrl url:$url');
		final result = await _audioPlayer.play(Uri.encodeFull(this.soundUrls[currSoundIndex]), volume: 2.0);
		if(_globalKeys[_currQuestionIdx] is GlobalKey<_OptionItemState>) {
			_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.playing);
		}
		updatePlaySoundUrlState(PlayerState.playing);
		return result;
	}

	Future playSmallSoundUrl(String url) async {
//		this.smallSoundUrl = url;
		final result = await _audioPlayer.play(url, volume: 2.0);
		return result;
	}

	Future playSoundUrls(List<String> urls, int soundIndex) async {
		this.soundUrl = null;
		this.soundUrls = urls;
		this.currSoundIndex = soundIndex;
		final result = await _audioPlayer.play(Uri.encodeFull(this.soundUrls[currSoundIndex]), volume: 2.0);
		updatePlaySoundUrlState(PlayerState.playing);
		return result;
	}

	Future pauseSoundUrl() async {
		final result = await _audioPlayer.pause();
		if(_globalKeys[_currQuestionIdx] is GlobalKey<_OptionItemState>) {
			_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.paused);
		}
		updatePlaySoundUrlState(PlayerState.paused);
		return result;
	}

	Future stopSoundUrl() async {
  	this.soundUrl = null;
		this.soundUrls = null;
		final result = await _audioPlayer.stop();
		if (result == 1) {
			if(_globalKeys[_currQuestionIdx] is GlobalKey<_OptionItemState> && _globalKeys[_currQuestionIdx].currentState != null && _globalKeys[_currQuestionIdx].currentState.mounted) {
				_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.stopped);
			}
			updatePlaySoundUrlState(PlayerState.stopped);
		}
		return result;
	}

	Future stopSoundUrlNew(int questionIndex) async {
  	this.soundUrl = null;
		this._currQuestionIdx = questionIndex;
		final result = await _audioPlayer.stop();
		if (result == 1) {
			if(_globalKeys[_currQuestionIdx] is GlobalKey<_OptionItemState>) {
				_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.stopped);
			}
			updatePlaySoundUrlState(PlayerState.stopped);
		}
		return result;
	}

	Future stopSoundUrls() async {
  	this.soundUrls = null;
		this.soundUrl = null;
		final result = await _audioPlayer.stop();
		if (result == 1) {
			updatePlaySoundUrlState(PlayerState.stopped);
		}
		return result;
	}

	void _onPlaySoundUrlComplete() {
		if(soundUrl != null) playSoundUrl(soundUrl);
		if(soundUrls != null && soundUrls.isNotEmpty) playSoundUrls(soundUrls, currSoundIndex == soundUrls.length -1 ? 0 : currSoundIndex + 1);
	}

	int _getAnsweredCount() {
    return answers.where((answerItem) => answerItem != null && !Utils.isEmptyString(answerItem.answer)).length;
  }

	void onSelectOption(int questionIndex, int optionIndex, bool selected, String strOption) {
		if(!isSubmitted()) {
			scrollToIndex(questionIndex);
		}
		setState(() {
			if(selected) {
				this.answers[questionIndex] = _OptionAnswer(answer: strOption, selectedIndex: optionIndex, result: null);
			} else {
				this.answers[questionIndex] = _OptionAnswer(answer: null, selectedIndex: -1, result: null);
			}
			_globalKeySelections[questionIndex].forEach((optionKey) {
				optionKey.currentState.updateSelectedIndex(optionIndex);
			});
		});
		print('$runtimeType.onSelectOption, questionIndex=$questionIndex, optionIndex=$optionIndex, selected=$selected, strOption=$strOption, answers=$answers');
	}

  void _checkAllAnswers() {
		setState(() {
			for(var index = 0; index < answers.length; index++) {
				if (Utils.normalizeAnswer(answers[index].answer.trim().toLowerCase()) == Utils.normalizeAnswer(widget.stage.questions[index].answerText.trim().toLowerCase())) {
					// dung
					widget.updateCorrectAnswer(index, true);
					this.answers[index] = _OptionAnswer(answer: answers[index].answer, selectedIndex: answers[index].selectedIndex, result: true);

				} else {
					// sai
					widget.updateCorrectAnswer(index, false);
					this.answers[index] = _OptionAnswer(answer: answers[index].answer, selectedIndex: answers[index].selectedIndex, result: false);
				}
			}

//			print('$runtimeType._checkAllAnswers: errmsg=${this.answerTextFieldErrorMessages}, enable=${this.answerTextFieldEnables}');
			_postSubmitExerciseExamResult();
		});
  }

  void _showAllResult() {
		for(var index = 0; index < _pages.length; index++) {
			if(_globalKeys[index] is GlobalKey<_OptionItemState>) {
				_globalKeySelections[index].forEach((optionKey) {
					optionKey.currentState.updateAnswerStatus(false, answers[index].result);
				});
			}
		}
	}

	void nextQuestionOrValidateAllAnswer() {
		if(_getAnsweredCount() == widget.stage.questions.length) {
			hideKeyboard();
			_setAllQuestionTextFieldEnable(false);
			_checkAllAnswers();
			stopSoundUrl();
//			widget.finishQuestion();
		} else {
			scrollToIndex(_currQuestionIdx + 1);
		}
	}

	void _setAllQuestionTextFieldEnable(bool enable) {
  	_globalKeySelections.forEach((questionKey) {
  		questionKey.forEach((optionKey) {
  			if(optionKey is GlobalKey<_OptionSelectionItemState>) {
					if(optionKey.currentState != null) optionKey.currentState.updateAnswerStatus(false, null);
				}
			});
		});
	}

	void _onTryAgain() {
		print("try again");
		stopSoundUrl();
		Navigator.pop(context);
		Navigator.push(context,
				MaterialPageRoute(
						builder: (context) => ExerciseExamPageScreen(ExerciseExamPageScreenState.TYPE_BY_STAGE, stage: widget.stage)));
	}

	void _onNextExercise() {
		print("next");
		stopSoundUrl();
		_getStageDetail(result.next);
	}

	Future _postSubmitExerciseExamResult() async {
		setState(() {
			_loaded = 1;
			_submit = 1;
		});
		Api.postSubmitExerciseExamResult(accessToken.accessToken,
				DoExerciseResultDto(stageId: widget.stage.stageId,
						numErrors: widget.numIncorrect(),
						timeDo: widget.secondExam,
						questionType: widget.stage.questions[0].type)).then((_result) {
			setState(() {
				this.result = _result;
				_loaded = 2;
			});

			if(result.pass) {
				playSmallSoundUrl(Constants.WIN_TUNE_URLS[0]);
				setState(() {
					this.correctAnswer = ANSWER_CORRECT;
				});
			} else {
				playSmallSoundUrl(Constants.FAIL_TUNE_URLS[0]);
				setState(() {
					this.correctAnswer = ANSWER_INCORRECT;
				});
			}
			_showAllResult();
		}, onError: (error) {
			print('error: $error');
			showSnackBar('Không thể gửi kết quả làm bài tập: ${error.toString()}');
			setState(() {
				_loaded = 2;
			});
		});
	}

	void _getStageDetail(String stageIdExp) async {
		setState(() {
			_loaded = 1;
		});
		await Api.getStageDetailByStageExp(accessToken.accessToken, stageIdExp).then((_stage) {
			setState(() {
				_loaded = 2;
			});
			Navigator.pop(context);
			Navigator.push(context,
					MaterialPageRoute(
							builder: (context) => ExerciseExamPageScreen(ExerciseExamPageScreenState.TYPE_BY_STAGE, stage: _stage)));
		}, onError: (error) {
			print('error: $error');
			showSnackBar(error.toString());
			setState(() {
				_loaded = 2;
			});
		});
	}

	bool isSubmitted() {
  	return _submit == 1 || _submit == 2;
	}

	bool hasStageAudio() {
  	return widget.stage.stageAudio != null && widget.stage.stageAudio.isNotEmpty;
	}

	@override
  Widget build(BuildContext context) {
		if(!_initItemsState) {
			return Container();
		}
		return Column(
			crossAxisAlignment: CrossAxisAlignment.center,
			children: <Widget>[
				Expanded(
					child: widget.stage.type != null && widget.stage.type == StageType.ONE ?
					CommonWidget.emptyScreenWithNotice('Bài tập không được hỗ trợ dạng "All"!')
					: SingleChildScrollView(
						controller: _controller,
						child: Column(
							crossAxisAlignment: CrossAxisAlignment.center,
							children: _pages,
						),
					),
				),
				_buildBottomContainer(),
			],
		);
  }
}

class _OptionAnswer {

	final String answer;

	final int selectedIndex;

	final bool result;

  _OptionAnswer({
		this.answer,
		this.selectedIndex,
		this.result
  });

}

class _OptionItem extends StatefulWidget {

	final int questionIdx;

	final Question question;

	final ExerciseExamOptionAllScreenState exerciseExamOptionAllScreenState;

  const _OptionItem({
		Key key,
		this.questionIdx,
		this.question,
		this.exerciseExamOptionAllScreenState
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OptionItemState();

}

class _OptionItemState extends State<_OptionItem> {

	/// play question audio
	PlayerState playSoundUrlState = PlayerState.stopped;

	List<Widget> _options = List();
//	List<GlobalKey<_OptionItemState>> _globalKeys;
//	String answer;

	@override
  void initState() {
    super.initState();

    _initExercise();
  }

	void _initExercise() {
		_options = List(widget.question == null || widget.question.questionOption == null ? 0 : widget.question.questionOption.length);
		widget.exerciseExamOptionAllScreenState._globalKeySelections[widget.questionIdx] = List<GlobalKey<_OptionSelectionItemState>>(_options.length);
		for(int index = 0; index < _options.length; index++) {
			widget.exerciseExamOptionAllScreenState._globalKeySelections[widget.questionIdx][index] = GlobalKey<_OptionSelectionItemState>();
			_options[index] = Container(
				padding: EdgeInsets.symmetric(vertical: 3.0),
				child: _OptionSelectionItem(
					key: widget.exerciseExamOptionAllScreenState._globalKeySelections[widget.questionIdx][index],
					questionIdx: widget.questionIdx,
					question: widget.question,
					optionIndex: index,
					prefix: Constants.PREFIXES[index],
					selected: false,
					optionItemState: this,
					exerciseExamOptionAllScreenState: widget.exerciseExamOptionAllScreenState,
					strOption: widget.question.questionOption[index],
				),
			);
		}
	}

	void _playQuestionAudio() {
		print('$runtimeType, _playQuestionAudio:${widget.question.questionAudio}');
		if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
			widget.exerciseExamOptionAllScreenState.playSoundUrlNew(widget.questionIdx, widget.question.questionAudio, 0);
		}
	}

	Widget _buildPlayQuestionAudioButton() {
		if(widget.question == null || widget.question.questionAudio.isEmpty
				|| (widget.exerciseExamOptionAllScreenState.hasStageAudio())) {
			return Container();
		} else if(playSoundUrlState == PlayerState.playing) {
			return IconButton(
				icon: Icon(FontAwesome.volume_up,
					color: MyColors.colorPrimary,
				),
				iconSize: 30,
				onPressed: () {
					widget.exerciseExamOptionAllScreenState.stopSoundUrlNew(widget.questionIdx);
				},
			);
		} else if(playSoundUrlState != PlayerState.playing) {
			return IconButton(
				icon: Icon(FontAwesome.volume_off,
					color: MyColors.colorPrimary,
				),
				iconSize: 30,
				onPressed: () {
					_playQuestionAudio();
				},
			);
		} else {
			return Container();
		}
	}

	void updatePlaySoundUrlState(PlayerState playSoundUrlState) {
		setState(() {
			this.playSoundUrlState = playSoundUrlState;
		});
	}

//	void onSelectOption(int index, bool selected, String strOption) {
//		widget.exerciseExamOptionAllScreenState.onSelectOption(index, selected, strOption);
//	}
//
//	void updateAnswerStatus(bool correct) {
//		for(int index = 0; index < _pages.length; index++) {
//		}
//	}

  @override
  Widget build(BuildContext context) {
    return Container(
			padding: EdgeInsets.all(10),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				mainAxisAlignment: MainAxisAlignment.center,
				children: <Widget>[
					Utils.isImageUrl(widget.question.questionPicture) ?
					Container(
						alignment: Alignment.center,
						child: Image.network(widget.question.questionPicture),
					) : Container(),
					Utils.isImageUrl(widget.question.questionPicture) ?
					CommonWidget.smallHorizontalDivider : Container(),
					Row(
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							Container(
								alignment: Alignment(0, 0),
								width: 32,
								height: 32,
								decoration: BoxDecoration(
										shape: BoxShape.circle,
										color: MyColors.colorPrimary
								),
								child: Text('${widget.questionIdx + 1}',
									style: TextStyle(
											color: Colors.white,
											fontWeight: FontWeight.bold,
											fontSize: 16
									),
								),
							),
							CommonWidget.smallVerticalDivider,
							Expanded(
								child: Text(widget.question.question,
									style: TextStyle(
										fontWeight: FontWeight.w600,
									),
								),
							),
							CommonWidget.tinyVerticalDivider,
							_buildPlayQuestionAudioButton(),
						],
					),
					CommonWidget.smallHorizontalDivider,
					ListView(
						shrinkWrap: true,
						addAutomaticKeepAlives: true,
						children: _options,
						physics: const NeverScrollableScrollPhysics(),
					),
				],
			),
		);
  }

}

class _OptionSelectionItem extends StatefulWidget {

	final int questionIdx;

	final Question question;

	final String prefix;

	final String strOption;

	final _OptionItemState optionItemState;

	final ExerciseExamOptionAllScreenState exerciseExamOptionAllScreenState;

	final bool selected;

	final int optionIndex;

	const _OptionSelectionItem({
		Key key,
		this.questionIdx,
		this.question,
		this.optionIndex,
		this.prefix,
		this.strOption,
		this.optionItemState,
		this.exerciseExamOptionAllScreenState,
		this.selected,
	}) : super(key: key);

	@override
	State<StatefulWidget> createState() => _OptionSelectionItemState();

}

class _OptionSelectionItemState extends State<_OptionSelectionItem> {

	bool correctAnswer = null;

	int selectedIndex = -1;

  bool enable = true;

	@override
	void initState() {
		super.initState();

		if(widget.exerciseExamOptionAllScreenState.answers[widget.questionIdx] != null) {
			setState(() {
				this.selectedIndex =
						widget.exerciseExamOptionAllScreenState.answers[widget.questionIdx]
								.selectedIndex;
			});
		}
	}

	void updateAnswerStatus(bool enable, bool correctAnswer) {
		setState(() {
			this.enable = enable;
			this.correctAnswer = correctAnswer;
		});
	}

	void updateSelectedIndex(int index) {
		setState(() {
			this.selectedIndex = index;
		});
	}

	@override
	Widget build(BuildContext context) {
//		print('$runtimeType.build, selectedIndex=$selectedIndex, widget.optionIndex=${widget.optionIndex}');
		BoxDecoration boxDecoration;
		Color textColor;
		if(this.selectedIndex == widget.optionIndex) {
			if(correctAnswer == null || correctAnswer) {
				boxDecoration = BoxDecoration(
						color: MyColors.colorPrimary,
						borderRadius: BorderRadius.all(Radius.circular(5.0)),
				);
				textColor = Colors.white;
			} else {
				boxDecoration = BoxDecoration(
					color: Colors.red,
					borderRadius: BorderRadius.all(Radius.circular(5.0)),
				);
				textColor = Colors.white;
			}
		} else {
			boxDecoration = BoxDecoration(
					color: Colors.transparent,
					borderRadius: BorderRadius.all(Radius.circular(5.0)),
					border: Border.all(
							color: MyColors.colorPrimary,
							width: 2.0
					)
			);
			textColor = MyColors.colorPrimary;
		}

		return Container(
			height: 48.0,
			alignment: Alignment.centerLeft,
			decoration: boxDecoration,
			child: InkWell(
				onTap: (!this.enable || widget.exerciseExamOptionAllScreenState.isSubmitted()) ? null : () {
					widget.exerciseExamOptionAllScreenState.onSelectOption(widget.questionIdx, widget.optionIndex, true, widget.strOption);
				},
				child: Container(
					padding: EdgeInsets.all(8.0),
					alignment: Alignment.centerLeft,
					child: Text('${widget.prefix}. ${widget.strOption}',
						style: TextStyle(
							color: textColor,
						),
						maxLines: 3,
						overflow: TextOverflow.visible,
					),
				),
			),
		);
	}
}

