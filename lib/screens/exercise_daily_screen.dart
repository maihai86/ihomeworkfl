import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';

import 'package:ihomework/domain/access_token.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'exercise_detail_screen.dart';
import 'base/abstract_state.dart';

class ExerciseDailyScreen extends StatefulWidget {

  final int menuId;

  final void Function() navigateScheduleScreen;

  ExerciseDailyScreen({
    key,
    this.menuId,
    this.navigateScheduleScreen,
  }) : super(key: key);

  @override
  _ExerciseDailyScreenState createState() => _ExerciseDailyScreenState();

}

class _ExerciseDailyScreenState extends AbstractState<ExerciseDailyScreen> {
  final GlobalKey<AnimatedCircularChartState> _chartKey = GlobalKey<AnimatedCircularChartState>();
  int chartSecDuration = 2;

  int _loaded = 0;

  StudentStat _studentStat;

  ScrollController _controller;

  int page = 1;

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    page = 1;
    _loadData();
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    page = 1;

    super.initState();
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  @override
  void dispose() {
    _controller.removeListener(_scrollListener);
    _controller.dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      print('reach the bottom');

      if(_loaded != 1) {
        page++;
        _loadData();
      }
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      print('reach the top');
    }
  }

  void _loadData() {
    if(accessToken != null) {
      setState(() {
        _loaded = 1;
      });
      Api.getListTest(accessToken.accessToken, page, widget.menuId).then((_studentStat) {
//        print('json access token: $_studentStat, ${_studentStat.listTest}');
        setState(() {
          if(this._studentStat == null) {
            this._studentStat = _studentStat;
          } else {
            if(_studentStat.diligenceScore != null) {
              this._studentStat.diligenceScore = _studentStat.diligenceScore;
            }
            if(_studentStat.usersRanking != null) {
              this._studentStat.usersRanking = _studentStat.usersRanking;
            }
            if(_studentStat.chart != null) {
              this._studentStat.chart = _studentStat.chart;
            }
            if(_studentStat.listTest != null) {
              if(_studentStat.listTest.lastTestId != null) {
                this._studentStat.listTest.lastTestId = _studentStat.listTest.lastTestId;
              }
              if(_studentStat.listTest.lastIndex != null) {
                this._studentStat.listTest.lastIndex = _studentStat.listTest.lastIndex;
              }
              if(_studentStat.listTest.tests != null) {
                this._studentStat.listTest.tests.addAll(_studentStat.listTest.tests);
              }
            }
          }
          _loaded = 2;
        });

        Timer(Duration(seconds: chartSecDuration), () {
          setState(() {
            chartSecDuration = 0;
          });
        });
      }, onError: (error) {
        print('error: $error');
        showSnackBar(error.toString());
        setState(() {
          _loaded = 2;
        });
      });
    }
  }

  void _navDetailScreen(Test test, String testDisplay) {
    if(test == null) {
      showSnackBar('Vui lòng chọn bài tập');
      return;
    }
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ExerciseDetailScreen(test: test, testDisplay: testDisplay)));
  }

  Widget _buildListTestHeader() {
    var percent = _studentStat == null || _studentStat.chart == null ?
    0 : _studentStat.chart.done / (_studentStat.chart.done + _studentStat.chart.notDone) * 100;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 40,
          decoration: BoxDecoration(
              color: MyColors.titleBackground
          ),
          alignment: AlignmentDirectional(0, 0),
          child: Text("Tỷ lệ hoàn thành bài tập".toUpperCase(),
            textAlign: TextAlign.center,
            overflow: TextOverflow.fade,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          constraints: BoxConstraints(
            maxHeight: 200,
            maxWidth: 200,
          ),
          margin: EdgeInsets.only(top: 10),
          child: AnimatedCircularChart(
            key: _chartKey,
            size: Size(200, 200),
            duration: Duration(seconds: chartSecDuration),
            initialChartData: <CircularStackEntry>[
              CircularStackEntry(
                <CircularSegmentEntry>[
                  CircularSegmentEntry(
                    _studentStat == null || _studentStat.chart == null ? 0.0 : _studentStat.chart.done.toDouble(),
                    MyColors.colorPrimary,
                    rankKey: 'completed',
                  ),
                  CircularSegmentEntry(
                    _studentStat == null || _studentStat.chart == null ? 100.0 : _studentStat.chart.notDone.toDouble(),
                    Colors.red,
                    rankKey: 'remaining',
                  ),
                ],
                rankKey: 'progress',
              ),
            ],
            chartType: CircularChartType.Radial,
            edgeStyle: SegmentEdgeStyle.round,
            percentageValues: true,
            holeLabel: '${percent.toInt()}%',
            labelStyle: TextStyle(
                color: Colors.red,
                fontWeight: FontWeight.bold,
                fontSize: 24.0
            ),
          ),
        ),
        Container(
          height: 40,
          margin: EdgeInsets.only(top: 10, left: 20, right: 20),
          alignment: AlignmentDirectional(0, 0),
          decoration: BoxDecoration(
              border: Border.all(
                  color: MyColors.colorPrimary
              ),
              borderRadius: BorderRadius.all(Radius.circular(5))
          ),
          child: Text("Xếp hạng bài tập: ${_studentStat != null && _studentStat.usersRanking != null && _studentStat.usersRanking.userRank != null ? _studentStat.usersRanking.userRank.rank : ''}",
            textAlign: TextAlign.center,
            overflow: TextOverflow.fade,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: 40,
          margin: EdgeInsets.only(top: 10, left: 20, right: 20),
          alignment: AlignmentDirectional(0, 0),
          decoration: BoxDecoration(
              border: Border.all(
                  color: MyColors.colorPrimary
              ),
              borderRadius: BorderRadius.all(Radius.circular(5))
          ),
          child: Text("Điểm chuyên cần: ${_studentStat == null || _studentStat.diligenceScore == null ?
          0 : _studentStat.diligenceScore}",
            textAlign: TextAlign.center,
            overflow: TextOverflow.clip,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: 40,
          margin: EdgeInsets.only(top: 10, left: 20, right: 20),
          alignment: AlignmentDirectional(0, 0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  child: InkWell(
                    onTap: () => widget.navigateScheduleScreen(),
                    child: Container(
                      margin: const EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:  BorderRadius.circular(5.0),
                        border: Border.all(
                            color: MyColors.colorPrimary,
                        ),
                      ),
                      child: Center(
                        child: Text('Lịch học',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: MyColors.colorPrimary
                          ),
                        ),
                      ),
                    ),
                  )
              ),
              Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:  BorderRadius.circular(5.0),
                        border: Border.all(
                          color: MyColors.colorPrimary,
                        ),
                      ),
                      child: Center(
                        child: Text('Thông báo',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: MyColors.colorPrimary
                          ),
                        ),
                      ),
                    ),
                  )
              ),
            ],
          ),
        ),
        Container(
          height: 40,
          margin: EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              color: MyColors.titleBackground
          ),
          alignment: AlignmentDirectional(0, 0),
          child: Text("Lộ trình bài tập".toUpperCase(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildListTestItem(int testIdx) {
    var test = _studentStat.listTest.tests[testIdx];
    final String testDisplay = '${test.name}' + (test.workTime == 0 ? '' : ' (${test.workTime} phút)');
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
            onTap: () {
              print('selected ${testIdx}');
              _navDetailScreen(test, testDisplay);
            },
            child: Container(
              padding: EdgeInsets.only(top: 16, bottom: 16, left: 10, right: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(testDisplay),
                  ),
                  SizedBox(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.green),
                      backgroundColor: Colors.red,
                      strokeWidth: 10,
                      value: test == null || test.testMoreInfo == null || test.testMoreInfo.successPercent == null ?
                      		0 : (test.testMoreInfo.successPercent / 100).toDouble(),
                    ),
                    height: 10.0,
                    width: 10.0,
                  )
                ],
              ),
            )
        ),
        Divider(
          color: Colors.grey,
          height: 1.0,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if(_loaded == 1) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      if(widget.menuId == 6 && _studentStat == null) {
        return new Container(
          alignment: AlignmentDirectional(0, 0),
          padding: EdgeInsets.all(5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CommonWidget.faWarningIcon,
              CommonWidget.mediumVerticalDivider,
              const Text('Bạn chưa được phép làm bài tập phần này!',
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ],
          ),
        );
      } else {
        return Container(
          child: ListView.builder(
            controller: _controller,
            itemCount: _studentStat == null || _studentStat.listTest == null || _studentStat.listTest.tests == null ? 1
                : _studentStat.listTest.tests.length + 1,
            itemBuilder: (BuildContext context, int index) {
              var testIdx = index - 1;
              switch(index) {
                case 0:
                  return _buildListTestHeader();
                default:
                  return _buildListTestItem(testIdx);
              }
            },
          ),
        );
      }
    }
  }

}