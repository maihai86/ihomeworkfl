import 'dart:async';

import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamGameScreen extends AbstractExerciseExamScreen {

  final void Function(int questionIndex, String url) playSoundUrlNew;
  
  final void Function(int questionIndex) stopSoundUrlNew;

  final int Function() getGameLive;

  final int Function() getGameScore;

  final bool Function() isGamePass;

  final void Function(int quantity) adjustGameLive;

  final void Function(int quantity) adjustGameScore;

  final void Function(bool pass) setGamePass;

	final void Function() finishQuestion;

	ExerciseExamGameScreen({
    key,
    stage,
    questionIdx,
    question,
    incorrectQuestionCount,
    nextQuestion,
    playSoundUrl,
    playSoundUrls,
    playSmallSoundUrl,
    stopSoundUrl,
    updateCorrectAnswer,
    this.playSoundUrlNew,
		this.stopSoundUrlNew,
		this.getGameLive,
		this.getGameScore,
		this.isGamePass,
		this.adjustGameLive,
		this.adjustGameScore,
		this.setGamePass,
		this.finishQuestion,
  }): super(
			key: key,
			stage: stage,
			questionIdx: questionIdx,
			question: question,
			incorrectQuestionCount: incorrectQuestionCount,
			nextQuestion: nextQuestion,
			playSoundUrl: playSoundUrl,
			playSoundUrls: playSoundUrls,
			playSmallSoundUrl: playSmallSoundUrl,
			stopSoundUrl: stopSoundUrl,
			updateCorrectAnswer: updateCorrectAnswer,
	);

  @override
	State createState() => ExerciseExamGameScreenState();

}

class ExerciseExamGameScreenState extends AbstractExerciseScreenState<ExerciseExamGameScreen> with TickerProviderStateMixin {

	static const int ANSWER_NOT_RECORDED = 0;
	static const int ANSWER_INCORRECT = 1;
	static const int ANSWER_CORRECT = 2;

	int correctAnswer = ANSWER_NOT_RECORDED;

  int selectedItemIndex = -1;

  int _itemCount = 0;

	Timer _timer;

	int nextQuestionCd = 0;

	int _initScore = 0;
	List<int> incorrectAnsIndexes = List();

	AnimationController _controller;
	bool _visible = false;
	double _bottom = 150;
	Animation<double> _opacity;
	bool _disableAll = false;

  @override
  void initState() {
    super.initState();

		_controller = AnimationController(
				duration: const Duration(milliseconds: 1000),
				vsync: this
		);
		_opacity = Tween<double>(
			begin: 0.0,
			end: 1.0,
		).animate(
			CurvedAnimation(
				parent: _controller.view,
				curve: Interval(
					0.0, 0.100,
					curve: Curves.ease,
				),
			),
		);
  }

	@override
	void dispose() {
		if (_timer != null && _timer.isActive) _timer.cancel();
		_controller.dispose();

		super.dispose();
	}

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    _initExercise();
		_playQuestionAudio();
		startTime();
  }

	@override
	bool shouldInitFCM() {
		return false;
	}

	void _initExercise() {
  	this._initScore = widget.question.score;
	}

	Widget _buildPlayQuestionAudioButton() {
		if(widget.question == null || widget.question.questionAudio.isEmpty) {
			return CommonWidget.emptyButtonContainer;
		} else if(playSoundUrlState == PlayerState.playing) {
			return IconButton(
				icon: Icon(FontAwesome.volume_up,
					color: MyColors.colorPrimary,
				),
				iconSize: 30,
				onPressed: () async {
					await widget.stopSoundUrl();
				},
			);
		} else if(playSoundUrlState != PlayerState.playing) {
			return IconButton(
				icon: Icon(FontAwesome.volume_off,
					color: MyColors.colorPrimary,
				),
				iconSize: 30,
				onPressed: () {
					_playQuestionAudio();
				},
			);
		} else {
			return CommonWidget.emptyButtonContainer;
		}
	}

	Widget _buildPrecisionIcon() {
		if(correctAnswer == ANSWER_CORRECT) {
			// haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true'
			return Row(
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Icon(FontAwesome.check,
						color: Colors.green,
						size: 48,
					),
				],
			);
		} else if(correctAnswer == ANSWER_INCORRECT) {
			// haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
			return Row(
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Icon(FontAwesome.close,
						color: Colors.red,
						size: 48,
					),
				],
			);
		} else {
			return CommonWidget.emptyButtonContainer2;
		}
	}

	void _playQuestionAudio() {
		print('$runtimeType, _playQuestionAudio:${widget.question.questionAudio}');
		if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
			widget.playSoundUrls(widget.question.questionAudio, 0);
		}
	}

	void _onSelectItem(int index, String answerItem) {
		int step = 0;
		setState(() {
			if (Utils.normalizeAnswer(widget.question.questionOption[index].trim().toLowerCase()) == Utils.normalizeAnswer(widget.question.answerText.trim().toLowerCase())) {
				this.correctAnswer = ANSWER_CORRECT;
				widget.adjustGameScore(getCurrentScore());
				step = 1;
			} else {
				this.correctAnswer = ANSWER_INCORRECT;
				this._visible = false;
				incorrectAnsIndexes.add(index);
				adjustGameLive(-1);
				step = 2;
			}
		});

		if(step == 1) {
			widget.updateCorrectAnswer(widget.questionIdx, true);
			widget.playSmallSoundUrl(Constants.CORRECT_URLS[0]);

			Timer(Duration(milliseconds: 500), () async {
				await widget.stopSoundUrl();
				widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);

				widget.setGamePass(widget.question.questionTarget);
			});
		} else if(step == 2) {
			widget.updateCorrectAnswer(widget.questionIdx, false);
			widget.playSmallSoundUrl(Constants.INCORRECT_URLS[0]);
		}
	}

	void playScoreAnimation() {
  	setState(() {
			this._disableAll = true;
			this._visible = true;
			this._bottom = 190;
			if (_timer != null && _timer.isActive) _timer.cancel();
			Timer(Duration(seconds: 1), () {
				setState(() {
					this._visible = false;
					this._bottom = 150;
				});
			});
  	});
	}

	Widget _buildGridItem(BuildContext context, int index) {
		String question = widget.question.questionOption[index];
		Color textColor = incorrectAnsIndexes.contains(index) ? Colors.red : MyColors.colorPrimary;
		final screenWidth = MediaQuery.of(context).size.width;
		final titleSize = (screenWidth - 30) / 2;
		return Material(
			child: InkWell(
				onTap: _disableAll ? null : () => _onSelectItem(index, question),
				child: Container(
					width: titleSize,
					height: titleSize,
					padding: EdgeInsets.all(15.0),
//					constraints: BoxConstraints(
//							minHeight: 112
//					),
					decoration: BoxDecoration(
							border: Border.all(
								color: MyColors.colorPrimary,
								width: 1.0,
							),
					),
					child: Center(
						child: Text(question,
							textAlign: TextAlign.center,
							style: TextStyle(
								fontWeight: FontWeight.w600,
								color: textColor,
							),
						),
					),
				),
			),
		);
	}

	void startTime() {
		if(_timer != null) {
			_timer.cancel();
		}
		_timer = Timer.periodic(Duration(milliseconds: Constants.DEFAULT_COUNTDOWN_MILLIS), (timer) {
			setState(() {
				this.nextQuestionCd += Constants.DEFAULT_COUNTDOWN_MILLIS;
				if(nextQuestionCd >= widget.question.gameTime * 1000) {
					this.nextQuestionCd = 0;
					adjustGameLive(-1);
				}
			});
		});
	}

	Future adjustGameLive(int quantity) async {
		if(widget.getGameLive() <= 0) {
			await widget.stopSoundUrl();
			widget.finishQuestion();
		} else {
			widget.adjustGameLive(-1);
		}
	}

	int getCurrentScore() {
		return (nextQuestionCd == 0 ? widget.question.score : (widget.question.score * (1 - nextQuestionCd / (widget.question.gameTime * 1000)))).floor();
	}

	final Duration _ANIMATION_DURATION = const Duration(milliseconds: 200);
	final Container emptyButtonContainer2 = Container(
		width: 68,
		height: 48,
	);

	/**
	 * Question example:
	 *
	 * {
			"id": 0,
			"type": "Game",
			"answer_text": "I am going on vacation next week.",
			"question_option": [
			"What's the problem?",
			"I am going on vacation next week.",
			"Do you have any time to meet with me on Friday?",
			"I need to buy some new clothes."
			],
			"guide": "chọn câu tiếng Anh",
			"question_audio": "http://media.tataenglish.com/VE_13_3_8_1.mp3",
			"time": 21.3,
			"question_target": false,
			"score": "20"
			}
	 */
  @override
  Widget build(BuildContext context) {
		final indicatorValue = (widget.questionIdx + 1) / widget.stage.questions.length;
		final screenWidth = MediaQuery.of(context).size.width;
		final titleSize = (screenWidth - 30) / 2;
		return Container(
			padding: EdgeInsets.all(10),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				mainAxisAlignment: MainAxisAlignment.center,
				children: <Widget>[
					/// header section
					/// END header section
					/// question section
					Container(
						alignment: AlignmentDirectional(0, 0),
						child: Text(widget.question.guide,
							style: TextStyle(
								fontWeight: FontWeight.w600,
								fontSize: 15,
							),
						),
					),
					/// END question section
					/// manipulation section
					CommonWidget.smallHorizontalDivider,
					Container(
						height: screenWidth - 20,
						alignment: AlignmentDirectional(0, 0),
						padding: EdgeInsets.all(5),
						child: Stack(
							alignment: AlignmentDirectional(0, 0),
							children: <Widget>[
								GridView.builder(
										itemCount: widget.question.questionOption.length,
										gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
										itemBuilder: (BuildContext context, int index) {
											return _buildGridItem(context, index);
										}
								),
								Container(
									width: 68.0,
									height: 68.0,
									decoration: const BoxDecoration(
										color: Colors.white,
									),
									child: CircularProgressIndicator(
										backgroundColor: MyColors.colorPrimary,
										valueColor: AlwaysStoppedAnimation<Color>(MyColors.colorAccent),
										value: nextQuestionCd / (widget.question.gameTime * 1000),
										strokeWidth: 6.0,
									),
								),
								Row(
									crossAxisAlignment: CrossAxisAlignment.center,
									mainAxisAlignment: MainAxisAlignment.center,
									children: <Widget>[
										Text('${widget.getGameLive()}X',
											style: const TextStyle(
												fontSize: 17,
												fontWeight: FontWeight.w700,
											),
										),
										Icon(FontAwesome.heart,
											color: Colors.red,
											size: 20,
										)
									],
								),
								AnimatedPositioned(
									duration: _ANIMATION_DURATION,
									bottom: _bottom,
									child: AnimatedOpacity(
										opacity: _visible ? 1.0 : 0.0,
										duration: _ANIMATION_DURATION,
										child: Text('+${getCurrentScore()}',
											style: TextStyle(
												fontSize: 48,
												fontWeight: FontWeight.bold,
												color: Colors.green,
											),
										),
									),
								),
							],
						),
					),
					Expanded(
						child: Container(),
					),
					/// END manipulation section
					/// bottom section
					Row(
						mainAxisAlignment: MainAxisAlignment.spaceBetween,
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							_buildPlayQuestionAudioButton(),
							_buildPrecisionIcon()
						],
					),
					/// END bottom section
				],
			),
		);
  }
}