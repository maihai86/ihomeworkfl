import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ihomework/utils/my_widget.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:ihomework/utils/utils.dart';
import 'home_screen.dart';
import 'exercise_main_screen.dart';
import 'package:ihomework/utils/api.dart';

import 'base/abstract_state.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();

}

class _LoginScreenState extends AbstractState<LoginScreen> {
  static const int LOG_STT_NOT = 0;
  static const int LOG_STT_LOGGING = 1;
  static const int LOG_STT_DONE = 2;

  bool bRememberMe = false;
  int bLoggingIn = LOG_STT_NOT;
  var usernameCtrl;
  var passwordCtrl;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

//    _loadSharedPreferences();

    usernameCtrl = new TextEditingController();
    passwordCtrl = new TextEditingController();
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  void _login() {
    print("Login pressed");

    if(!_validateLogin()) {
      return;
    }

    setState(() {
      this.bLoggingIn = LOG_STT_LOGGING;
    });

    Api.postLogin(usernameCtrl.text, passwordCtrl.text, prefs.getString(PrefsKey.KEY_FIREBASE_DEVICE_TOKEN)).then((accessToken) {
      print('json access token: ${jsonEncode(accessToken)}');
      prefs.setString(PrefsKey.KEY_ACCESS_TOKEN, jsonEncode(accessToken));
      prefs.setBool(PrefsKey.KEY_REMEMBER_ME, bRememberMe);
      setState(() {
        this.bLoggingIn = LOG_STT_DONE;
      });

      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => ExerciseMainScreen()));
    }, onError: (error, stacktrace) {
      setState(() {
        this.bLoggingIn = LOG_STT_DONE;
      });

      showSnackBar(error.toString());
    });
  }

  bool _validateLogin() {
    if(usernameCtrl.text == null || usernameCtrl.text.toString().trim().isEmpty) {
      showSnackBar("Bạn cần nhập Tên đăng nhập");
      return false;
    }
    if(passwordCtrl.text == null || passwordCtrl.text.toString().trim().isEmpty) {
      showSnackBar("Bạn cần nhập Mật khẩu");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('TATA English'),
        leading: Container(
          margin: EdgeInsets.all(10),
          child: Image.asset("images/kien_tri.png", width: 24, height: 24,),
        )
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Divider(
              color: Colors.transparent,
              height: 76,
            ),
            Container(
                margin: EdgeInsets.only(top: 30, left: 10, right: 50),
                child: AbsorbPointer(
                  absorbing: bLoggingIn == LOG_STT_LOGGING,
                  child: TextField(
                    controller: usernameCtrl,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: MyColors.colorPrimary,
                        )
                      ),
                      labelText: 'Tên đăng nhập',
                      icon: const Icon(Icons.person,),
                    ),
                  ),
                )
            ),
            Container(
                margin: EdgeInsets.only(top: 15, left: 10, right: 50),
                child: AbsorbPointer(
                  absorbing: bLoggingIn == LOG_STT_LOGGING,
                  child: TextField(
                    controller: passwordCtrl,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: MyColors.colorPrimary,
                        ),
                      ),
                      labelText: 'Mật khẩu',
                      icon: const Icon(Icons.lock),
                    ),
                  ),
                )
            ),
            Container(
                margin: EdgeInsets.only(top: 15, left: 20, right: 50),
                child: AbsorbPointer(
                  absorbing: bLoggingIn == LOG_STT_LOGGING,
                  child: CheckboxListTile(
                    value: bRememberMe,
                    onChanged: (bool value) => setState(() => bRememberMe = value),
                    controlAffinity: ListTileControlAffinity.leading,
                    title: Text("Nhớ mật khẩu đăng nhập",
                      style: TextStyle(
                        color: MyColors.colorPrimary,
                      ),
                    ),
                  ),
                )
            ),
            Container(
                margin: EdgeInsets.only(top: 20, left: 50, right: 50),
                child: AbsorbPointer(
                  absorbing: bLoggingIn == LOG_STT_LOGGING,
                  child: RaisedButton(
                    onPressed: _login,
                    textColor: Colors.white,
                    color: MyColors.colorPrimary,
                    child: Text("Đăng nhập".toUpperCase()),
                    padding: EdgeInsets.all(15),
                  ),
                )
            ),
            Divider(
              color: Colors.transparent,
              height: 20,
            ),
            Visibility(
              visible: bLoggingIn == LOG_STT_LOGGING,
              child: Column(
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}