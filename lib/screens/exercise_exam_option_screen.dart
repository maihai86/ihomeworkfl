import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamOptionScreen extends AbstractExerciseExamScreen {

//  final Stage stage;
//
//  final int questionIdx;
//
//  final Question question;
//
//  final int Function() incorrectQuestionCount;
//
//  final void Function(int questionIdx, bool correctAnswer) nextQuestion;
//
//  final void Function(String url) playSoundUrl;
//
//	final void Function(List<String> urls, int soundIndex) playSoundUrls;

  final void Function(int questionIndex, String url) playSoundUrlNew;

//  final void Function(String url) playSmallSoundUrl;
//
//  final void Function() stopSoundUrl;

  final void Function(int questionIndex) stopSoundUrlNew;

//  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

	ExerciseExamOptionScreen({
    key,
    stage,
    questionIdx,
    question,
    incorrectQuestionCount,
    nextQuestion,
    playSoundUrl,
    playSoundUrls,
    playSmallSoundUrl,
    stopSoundUrl,
    updateCorrectAnswer,
		this.playSoundUrlNew,
		this.stopSoundUrlNew,
  }): /*assert(stage != null),*/
//      assert(questionIdx != null),
//      assert(question != null),
//      assert(nextQuestion != null),
//      assert(playSoundUrl != null),
//      assert(playSoundUrls != null),
//      assert(playSmallSoundUrl != null),
//      assert(stopSoundUrl != null),
//      assert(updateCorrectAnswer != null),
      super(
				key: key,
				stage: stage,
				questionIdx: questionIdx,
				question: question,
				incorrectQuestionCount: incorrectQuestionCount,
				nextQuestion: nextQuestion,
				playSoundUrl: playSoundUrl,
				playSoundUrls: playSoundUrls,
				playSmallSoundUrl: playSmallSoundUrl,
				stopSoundUrl: stopSoundUrl,
				updateCorrectAnswer: updateCorrectAnswer,);

  @override
  State<StatefulWidget> createState() => ExerciseExamOptionScreenState();

}

class ExerciseExamOptionScreenState extends AbstractExerciseScreenState<ExerciseExamOptionScreen> {

  static const int ANSWER_NOT_RECORDED = 0;
  static const int ANSWER_INCORRECT = 1;
  static const int ANSWER_CORRECT = 2;

  int correctAnswer = ANSWER_NOT_RECORDED;

	bool _showHint = false;
  Timer _timer;
  ///

  TextEditingController _controller;

	List<Widget> _pages = List();
	List<GlobalKey<_OptionItemState>> _globalKeys;
  String answer;

  int selectedIndex = -1;

  Map<String, int> mapIncorrectAnswerCount;
  
  @override
  void initState() {
    super.initState();

    _controller = TextEditingController();
		_playQuestionAudio();
  }

  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();
    _controller.dispose();

    super.dispose();
  }


	@override
	void afterLoadSharedPreferences() {
		super.afterLoadSharedPreferences();

		_initExercise();
	}

	@override
	bool shouldInitFCM() {
		return false;
	}

	void _initExercise() {
		_pages = List(widget.question == null || widget.question.questionOption == null ? 0 : widget.question.questionOption.length);
		_globalKeys = List(_pages.length);
		for(int index = 0; index < _pages.length; index++) {
			_globalKeys[index] = GlobalKey();
			_pages[index] = Container(
				padding: EdgeInsets.symmetric(vertical: 3.0),
				child: _OptionItem(
					key: _globalKeys[index],
					index: index,
					prefix: Constants.PREFIXES[index],
					selected: false,
					exerciseExamOptionScreenState: this,
					strOption: widget.question.questionOption[index],
				),
			);
		}
		mapIncorrectAnswerCount = Map();
		for(String s in widget.question.questionOption) {
			mapIncorrectAnswerCount[s] = 0;
		}
	}

  Widget _buildPlayQuestionAudioButton() {
    if(widget.question == null || widget.question.questionAudio.isEmpty) {
      return CommonWidget.emptyButtonContainer;
    } else if(playSoundUrlState == PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_up,
          color: MyColors.colorPrimary,
        ),
        iconSize: 20,
        onPressed: () async {
					await widget.stopSoundUrl();
        },
      );
    } else if(playSoundUrlState != PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_off,
          color: MyColors.colorPrimary,
        ),
        iconSize: 20,
        onPressed: () {
          _playQuestionAudio();
        },
      );
    } else {
      return CommonWidget.emptyButtonContainer2;
    }
  }

  Widget _buildPrecisionIcon() {
    if(correctAnswer == ANSWER_CORRECT) {
      // haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.check,
            color: Colors.green,
            size: 48,
          ),
        ],
      );
    } else if(correctAnswer == ANSWER_INCORRECT) {
      // haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.close,
            color: Colors.red,
            size: 48,
          ),
        ],
      );
    } else {
      return CommonWidget.emptyButtonContainer2;
    }
  }

  void _playQuestionAudio() {
  	print('$runtimeType, _playQuestionAudio:${widget.question.questionAudio}');
    if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
			widget.playSoundUrls(widget.question.questionAudio, 0);
    }
  }

	void onSelectOption(int index, bool selected, String strOption) {
  	int step = 0;
  	setState(() {
			if(selected) {
				this.answer = strOption;
				this.selectedIndex = index;
				if (Utils.normalizeAnswer(answer.trim().toLowerCase()) == Utils.normalizeAnswer(widget.question.answerText.trim().toLowerCase())) {
					this._showHint = true;
					this.correctAnswer = ANSWER_CORRECT;
					step = 1;
				} else {
					this._showHint = false;
					this.correctAnswer = ANSWER_INCORRECT;
					step = 2;
					mapIncorrectAnswerCount[this.answer] = mapIncorrectAnswerCount[this.answer] + 1;
				}
			} else {
				this.answer = null;
				this.selectedIndex = -1;
			}
  	});
  	try {
			for(int i = 0; i < _globalKeys.length; i++) {
				_globalKeys[i].currentState.updateSelectedIndex(index);
			}
		} catch(e) {
			print(e);
		}

  	if(step == 1) {
			widget.updateCorrectAnswer(widget.questionIdx, true);
			widget.playSmallSoundUrl(Constants.CORRECT_URLS[0]);

			if(!_showHint || Utils.isEmptyString(widget.question.hintText)) {
				Timer(Duration(milliseconds: 500), () async {
					await widget.stopSoundUrl();
					widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
				});
			}
		} else if(step == 2) {
			widget.updateCorrectAnswer(widget.questionIdx, false);
			widget.playSmallSoundUrl(Constants.INCORRECT_URLS[0]);
		}
	}

  ///
  /**
   * Question example:
   * {
			"id": 5851835,
			"type": "Option",
			"question_picture": "",
			"question_option": "\"Choose the correct answer.\"",
			"question": "Choose the correct answer.",
			"answer_text": "C",
			"hint_text": null,
			"question_audio": "http://media.tataenglish.com/audio_se/Audio/VE_01_2_01.mp3"
			}
   */
  ///
  @override
  Widget build(BuildContext context) {
		final indicatorValue = (widget.questionIdx + 1) / widget.stage.questions.length;
		return Container(
			padding: EdgeInsets.all(10),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				mainAxisAlignment: MainAxisAlignment.center,
				children: <Widget>[
					/// header section
					Row(
						children: <Widget>[
							Container(
								alignment: Alignment(0, 0),
								width: 32,
								height: 32,
								decoration: BoxDecoration(
										shape: BoxShape.circle,
										color: MyColors.colorPrimary
								),
								child: Text('${widget.questionIdx + 1}',
									style: TextStyle(
											color: Colors.white,
											fontWeight: FontWeight.bold,
											fontSize: 16
									),
								),
							),
							CommonWidget.smallVerticalDivider,
							Expanded(
								child: LinearProgressIndicator(
									backgroundColor: MyColors.colorPrimaryDark,
									value: indicatorValue,
								),
							),
							CommonWidget.smallVerticalDivider,
							Text('${widget.stage.questions.length - widget.incorrectQuestionCount()}/${widget.stage.questions.length}',
								style: TextStyle(
									fontWeight: FontWeight.bold,
								),
							),
						],
					),
					/// END header section
					/// question section
					CommonWidget.smallHorizontalDivider,
					Expanded(
						child: Column(
							crossAxisAlignment: CrossAxisAlignment.center,
							children: <Widget>[
								Utils.isImageUrl(widget.question.questionPicture) ?
								Flexible(
										child: Image.network(widget.question.questionPicture)
								) : Container(),
								Utils.isImageUrl(widget.question.questionPicture) ?
								CommonWidget.smallHorizontalDivider : Container(),
								Row(
									crossAxisAlignment: CrossAxisAlignment.center,
									children: <Widget>[
										Icon(FontAwesome.question,
											color: MyColors.colorPrimary,
											size: 30,
										),
										CommonWidget.smallVerticalDivider,
										Expanded(
											child: Text(widget.question.question,
												style: TextStyle(
													fontWeight: FontWeight.w600,
												),
											),
										),
//									CommonWidget.smallVerticalDivider,
//									_buildPlayQuestionAudioButton(),
									],
								),
								CommonWidget.smallHorizontalDivider,
								ListView(
									shrinkWrap: true,
									children: _pages,
									physics: const NeverScrollableScrollPhysics(),
								),
								CommonWidget.smallHorizontalDivider,
								_showHint && !Utils.isEmptyString(widget.question.hintText) ?
								RichText(
									textAlign: TextAlign.start,
									text: TextSpan(
										style: DefaultTextStyle.of(context).style,
										children: [
											TextSpan(
												text: 'Giải thích: ',
											),
											TextSpan(
												text: widget.question.hintText.replaceAll('\\n', '\n'),
											),
										],
									),
								) : Container(),
//										CommonWidget.smallHorizontalDivider,
//										_buildPrecisionIcon(),
								Container(
									child: Center(
										child: Text(correctAnswer == ANSWER_INCORRECT ? '#${mapIncorrectAnswerCount[answer]} $answer' : '',
											style: TextStyle(
												color: Colors.red,
												fontWeight: FontWeight.w400,
											),
										),
									),
								),
								CommonWidget.tinyHorizontalDivider,
							],
						),
					),
					/// END manipulation section
					/// bottom section
					Row(
						mainAxisAlignment: MainAxisAlignment.spaceBetween,
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							_buildPlayQuestionAudioButton(),
							_buildPrecisionIcon(),
							_showHint && !Utils.isEmptyString(widget.question.hintText) ?
							Container(
								width: 48,
								child: FlatButton(
									textColor: MyColors.colorPrimary,
									padding: EdgeInsets.all(5.0),
									child: Text("OK",
										style: TextStyle(
												fontWeight: FontWeight.bold,
												fontSize: 18
										),
									),
									onPressed: () async {
										await widget.stopSoundUrl();
										widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
									},
								),
							) : CommonWidget.emptyButtonContainer2,
						],
					),
					/// END bottom section
				],
			),
		);
  }
}

class _OptionItem extends StatefulWidget {

	final String prefix;

	final String strOption;

	final ExerciseExamOptionScreenState exerciseExamOptionScreenState;

	final bool selected;

	final index;

  const _OptionItem(
		{Key key,
		this.index,
		this.prefix,
		this.strOption,
		this.exerciseExamOptionScreenState,
		this.selected})
		: super(key: key);

  @override
  State<StatefulWidget> createState() => _OptionItemState();

}

class _OptionItemState extends State<_OptionItem> {

  int selectedIndex = -1;

  @override
  void initState() {
    super.initState();

    setState(() {
      this.selectedIndex = widget.exerciseExamOptionScreenState.selectedIndex;
    });
  }

//	bool selected = false;

	void updateSelectedIndex(int index) {
		setState(() {
			this.selectedIndex = index;
		});
	}

	@override
	Widget build(BuildContext context) {
		if(this.selectedIndex == widget.index) {
			return Container(
				height: 48.0,
				alignment: Alignment.centerLeft,
				decoration: BoxDecoration(
					color: MyColors.colorPrimary,
					borderRadius: BorderRadius.all(Radius.circular(5.0)),
					border: Border.all(
						color: MyColors.colorPrimary,
						width: 2.0
					)
				),
				child: InkWell(
					onTap: widget.exerciseExamOptionScreenState.correctAnswer == ExerciseExamOptionScreenState.ANSWER_CORRECT ? null : () {
						widget.exerciseExamOptionScreenState.onSelectOption(widget.index, true, widget.strOption);
					},
					child: Container(
						padding: EdgeInsets.all(8.0),
						alignment: Alignment.centerLeft,
						child: Text('${widget.prefix}. ${widget.strOption}',
							style: TextStyle(
								color: Colors.white,
							),
							maxLines: 3,
							overflow: TextOverflow.visible,
						),
					)
				),
			);
		} else {
			return Container(
				height: 48.0,
				alignment: Alignment.centerLeft,
				decoration: BoxDecoration(
					color: Colors.transparent,
					borderRadius: BorderRadius.all(Radius.circular(5.0)),
					border: Border.all(
						color: MyColors.colorPrimary,
						width: 2.0
					)
				),
				child: InkWell(
					onTap: widget.exerciseExamOptionScreenState.correctAnswer == ExerciseExamOptionScreenState.ANSWER_CORRECT ? null : () {
						widget.exerciseExamOptionScreenState.onSelectOption(widget.index, true, widget.strOption);
					},
					child: Container(
						padding: EdgeInsets.all(8.0),
						alignment: Alignment.centerLeft,
						child: Text('${widget.prefix}. ${widget.strOption}',
							style: TextStyle(
								color: MyColors.colorPrimary,
							),
						),
					)
				),
			);
		}
	}
}