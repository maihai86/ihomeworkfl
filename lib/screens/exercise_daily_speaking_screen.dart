import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';

import 'package:ihomework/domain/access_token.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/utils.dart';
import 'exercise_detail_screen.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

@deprecated
class ExerciseDailySpeakingScreen extends StatefulWidget {
  @override
  _ExerciseDailySpeakingScreenState createState() => _ExerciseDailySpeakingScreenState();

}

@deprecated
class _ExerciseDailySpeakingScreenState extends AbstractState<ExerciseDailySpeakingScreen> {
  final GlobalKey<AnimatedCircularChartState> _chartKey = GlobalKey<AnimatedCircularChartState>();

  int _loaded = 0;

  StudentStat _studentStat;

  ScrollController _controller;

  int page = 1;

  double _scrollOffset = 0;

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    page = 1;
    _loadData();
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    page = 1;

    super.initState();
  }

  @override
  void dispose() {
    _controller.removeListener(_scrollListener);
    _controller.dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      print('reach the bottom');

      if(_loaded != 1) {
        page++;
        _loadData();
      }
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      print('reach the top');
    }
  }

  void _loadData() {
    if(accessToken != null) {
      setState(() {
        _loaded = 1;
      });
      Api.getListTest(accessToken.accessToken, page, 1).then((_studentStat) {
        setState(() {
          if(this._studentStat == null) {
            this._studentStat = _studentStat;
          } else {
            if(_studentStat.diligenceScore != null) {
              this._studentStat.diligenceScore = _studentStat.diligenceScore;
            }
            if(_studentStat.usersRanking != null) {
              this._studentStat.usersRanking = _studentStat.usersRanking;
            }
            if(_studentStat.chart != null) {
              this._studentStat.chart = _studentStat.chart;
            }
            if(_studentStat.listTest != null) {
              if(_studentStat.listTest.lastTestId != null) {
                this._studentStat.listTest.lastTestId = _studentStat.listTest.lastTestId;
              }
              if(_studentStat.listTest.lastIndex != null) {
                this._studentStat.listTest.lastIndex = _studentStat.listTest.lastIndex;
              }
              if(_studentStat.listTest.tests != null) {
                this._studentStat.listTest.tests.addAll(_studentStat.listTest.tests);
              }
            }
          }
          _loaded = 2;
        });
      }, onError: (error) {
        print('error: $error');
        showSnackBar(error.toString());
        setState(() {
          _loaded = 2;
        });
      });
    }
  }

  void _navDetailScreen(Test test, String testDisplay) {
    if(test == null) {
      showSnackBar('Vui lòng chọn bài tập');
      return;
    }
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ExerciseDetailScreen(test: test, testDisplay: testDisplay)));
  }

//  void _getTestDetail(Test test) async {
//    if(test == null) {
//      showSnackBar('Vui lòng chọn bài tập');
//      return;
//    }
//
//    await Api.getTestDetail(accessToken.accessToken, test.id).then((_parts) {
//      if(_parts == null || _parts.length == 0
//          || _parts[0].exercises == null || _parts[0].exercises.length == 0) {
//        showSnackBar('Không có bài tập nào!');
//        return;
//      }
//
//      Navigator.push(context,
//          MaterialPageRoute(
//              builder: (context) => ExerciseExamPageScreen(exercise: _parts[0].exercises[0])));
//    }, onError: (error) {
//      print('error: $error');
//      showSnackBar(error.toString());
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return _loaded == 1 ?
    Center(
      child: CircularProgressIndicator(),
    ) :
    Container(
      child: ListView.builder(
        /// haimt about key attribute: (framework.dart)
        /// Controls how one widget replaces another widget in the tree.
        ///
        /// If the [runtimeType] and [key] properties of the two widgets are
        /// [operator==], respectively, then the new widget replaces the old widget by
        /// updating the underlying element (i.e., by calling [Element.update] with the
        /// new widget). Otherwise, the old element is removed from the tree, the new
        /// widget is inflated into an element, and the new element is inserted into the
        /// tree.
        ///
        /// In addition, using a [GlobalKey] as the widget's [key] allows the element
        /// to be moved around the tree (changing parent) without losing state. When a
        /// new widget is found (its key and type do not match a previous widget in
        /// the same location), but there was a widget with that same global key
        /// elsewhere in the tree in the previous frame, then that widget's element is
        /// moved to the new location.
        ///
        /// Generally, a widget that is the only child of another widget does not need
        /// an explicit key.
        ///
        /// See also the discussions at [Key] and [GlobalKey].
        key: PageStorageKey('testList'), // haimt persist scroll position, perfect
        controller: _controller,
        itemCount: _studentStat == null || _studentStat.listTest == null || _studentStat.listTest.tests == null ? 1
            : _studentStat.listTest.tests.length + 1,
        itemBuilder: (context, index) {
//          var percent = _studentStat == null || _studentStat.chart == null ?
//          0 : _studentStat.chart.done / _studentStat.chart.notDone * 100;
          var testIdx = index - 1;
          switch(index) {
            case 0:
              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                        color: MyColors.titleBackground
                    ),
                    alignment: AlignmentDirectional(0, 0),
                    child: Text("Lộ trình bài tập Speaking".toUpperCase(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              );
            default:
              var test = _studentStat.listTest.tests[testIdx];
              final String testDisplay = '${test.name}' + (test.workTime == 0 ? '' : ' (${test.workTime} phút)');
              return InkWell(
                  onTap: () {
                    print('selected $index');
                    _navDetailScreen(test, testDisplay);
//                    _getTestDetail(test);
                  },
                  child: Container(
                    padding: EdgeInsets.only(top: 16, bottom: 16, left: 10, right: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Text(testDisplay),
                        ),
                        SizedBox(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Colors.green),
                            backgroundColor: Colors.red,
                            strokeWidth: 10,
                            value: test == null || test.testMoreInfo == null || test.testMoreInfo.successPercent == null ?
                            0 : (test.testMoreInfo.successPercent / 100).toDouble(),
                          ),
                          height: 10.0,
                          width: 10.0,
                        )
                      ],
                    ),
                  )
              );
          }
        },
      ),
    );
  }

}