import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamMappingScreen extends AbstractExerciseExamScreen {

//  final Stage stage;
//
//  final int questionIdx;
//
//  final Question question;
//
//  final int Function() incorrectQuestionCount;
//
//  final void Function(int questionIdx, bool correctAnswer) nextQuestion;
//
//  final void Function(String url) playSoundUrl;
//
//	final void Function(List<String> urls, int soundIndex) playSoundUrls;

  final void Function(int questionIndex, String url) playSoundUrlNew;
//
//  final void Function(String url) playSmallSoundUrl;
//
//  final void Function() stopSoundUrl;

  final void Function(int questionIndex) stopSoundUrlNew;

//  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

  ExerciseExamMappingScreen({
    key,
    stage,
    questionIdx,
    question,
    incorrectQuestionCount,
    nextQuestion,
    playSoundUrl,
    playSoundUrls,
    playSmallSoundUrl,
    stopSoundUrl,
    updateCorrectAnswer,
    this.playSoundUrlNew,
		this.stopSoundUrlNew,
  }): /*assert(stage != null),*/
//        assert(questionIdx != null),
//        assert(question != null),
//        assert(nextQuestion != null),
//        assert(playSoundUrl != null),
//        assert(playSoundUrls != null),
//        assert(playSmallSoundUrl != null),
//        assert(stopSoundUrl != null),
//        assert(updateCorrectAnswer != null),
			super(
				key: key,
				stage: stage,
				questionIdx: questionIdx,
				question: question,
				incorrectQuestionCount: incorrectQuestionCount,
				nextQuestion: nextQuestion,
				playSoundUrl: playSoundUrl,
				playSoundUrls: playSoundUrls,
				playSmallSoundUrl: playSmallSoundUrl,
				stopSoundUrl: stopSoundUrl,
				updateCorrectAnswer: updateCorrectAnswer,);

  @override
  ExerciseExamMappingScreenState createState() => ExerciseExamMappingScreenState();

}

class ExerciseExamMappingScreenState extends AbstractExerciseScreenState<ExerciseExamMappingScreen> {

	static const int ANSWER_NOT_RECORDED = 0;
	static const int ANSWER_INCORRECT = 1;
	static const int ANSWER_CORRECT = 2;

	int correctAnswer = ANSWER_NOT_RECORDED;

  int selectedItemIndex = -1;
  List<_MappingQuestionItem> _mappingQuestionItems = List();

  int _itemCount = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    _initExercise();
  }

	@override
	bool shouldInitFCM() {
		return false;
	}

	void _initExercise() {
  	setState(() {
			_mappingQuestionItems = List.generate(widget.question == null || widget.question.questionMap == null ? 0
					: widget.question.questionMap.length, (index) => _MappingQuestionItem(questionMapItem: widget.question.questionMap[index]));
  	});

	}

  int _buildGridCount() {
    return _mappingQuestionItems.length;
  }

  Widget _buildGridItem(BuildContext context, int index) {
  	QuestionMapItem questionMapItem = _mappingQuestionItems[index].questionMapItem;
    Color color;
    if(selectedItemIndex != index) {
			color = questionMapItem.background == 1 ? MyColors.greenSound : MyColors.yellowText;
		} else {
			color = MyColors.selectedTile;
		}
    return Material(
			color: color,
			child: InkWell(
				onTap: () async => _onSelectItem(index, questionMapItem),
				child: Container(
					padding: EdgeInsets.all(15.0),
					constraints: BoxConstraints(
						minHeight: 92
					),
					decoration: BoxDecoration(
						border: Border.all(
							color: MyColors.colorPrimary,
							width: 1.0,
						)
					),
					child: Center(
						child: Utils.isImageUrl(questionMapItem.question) ?
						_buildPlayQuestionAudioButton(index, questionMapItem) :
						Text(questionMapItem.question.replaceAll('\\n', '\n'),
							textAlign: TextAlign.center,
							style: TextStyle(
									fontWeight: FontWeight.w600
							),
						),
					),
				),
			),
		);
  }

  final emptyButton = Container();

	Widget _buildPlayQuestionAudioButton(int index, QuestionMapItem questionMapItem) {
//		print('$runtimeType._buildPlayQuestionAudioButton, index=$index, selectedItemIndex=$selectedItemIndex');
		if(Utils.isEmptyString(questionMapItem.question)) {
			return emptyButton;
		} else if(playSoundUrlState == PlayerState.playing && index == selectedItemIndex) {
			return Animator(
				tween: Tween<double>(begin: 0.95, end: 1.05),
				curve: Curves.elasticInOut,
				cycles: 0,
				duration: Duration(milliseconds: 200),
				builder: (anim) => Transform.scale(
						scale: anim.value,
						child: SizedBox(
							width: 15,
							height: 15,
							child: Icon(FontAwesome.volume_up,
								color: MyColors.colorPrimary,
							),
						)
				),
			);
		} else if(playSoundUrlState != PlayerState.playing || index != selectedItemIndex) {
			return SizedBox(
				width: 15,
				height: 15,
				child: Icon(FontAwesome.volume_off,
					color: MyColors.colorPrimary,
				),
			);
		} else {
			return emptyButton;
		}
	}

	void _playQuestionAudio(int index, String questionAudio) {
		print('$runtimeType, _playQuestionAudio:${questionAudio}');
		if(!Utils.isEmptyString(questionAudio)) {
			widget.playSoundUrl(questionAudio);
		}
	}

	Future _onSelectItem(int index, QuestionMapItem questionMapItem) async {
		print("onSelectItem index: $index, questionMapItem={${questionMapItem.key}, ${questionMapItem.question}}");
		int step = 0;
		if(selectedItemIndex > -1) {
			if(selectedItemIndex == index) {
				setState(() => selectedItemIndex = -1);

				if(Utils.isImageUrl(questionMapItem.question)) {
					await widget.stopSoundUrl();
				}
			} else {
				setState(() async {
					if (_mappingQuestionItems[selectedItemIndex].questionMapItem.key == _mappingQuestionItems[index].questionMapItem.key) {
						this.correctAnswer = ANSWER_CORRECT;
						int deletedKey = _mappingQuestionItems[index].questionMapItem.key;
						step = 1;
						_mappingQuestionItems.removeWhere((_mapQuestionItem) => _mapQuestionItem.questionMapItem.key == deletedKey);

						selectedItemIndex = -1;

						await widget.stopSoundUrl();

						widget.updateCorrectAnswer(widget.questionIdx, true);
						widget.playSmallSoundUrl(Constants.CORRECT_URLS[0]);
						if(_mappingQuestionItems.isEmpty) {
							widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
						}
					} else {
						if((Utils.isImageUrl(_mappingQuestionItems[selectedItemIndex].questionMapItem.question) && !Utils.isImageUrl(_mappingQuestionItems[index].questionMapItem.question))
								|| (!Utils.isImageUrl(_mappingQuestionItems[selectedItemIndex].questionMapItem.question) && Utils.isImageUrl(_mappingQuestionItems[index].questionMapItem.question))) {
							this.correctAnswer = ANSWER_INCORRECT;
							step = 2;

							selectedItemIndex = -1;

							await widget.stopSoundUrl();

							widget.updateCorrectAnswer(widget.questionIdx, false);
							widget.playSmallSoundUrl(Constants.INCORRECT_URLS[0]);
						} else {
							selectedItemIndex = index;
							step = 3;

							if(Utils.isImageUrl(questionMapItem.question)) {
								widget.stopSoundUrl();
								_playQuestionAudio(index, questionMapItem.question);
							}
						}
					}
				});


			}
		} else {
			setState(() {
				selectedItemIndex = index;

			});
			if(Utils.isImageUrl(questionMapItem.question)) {
				_playQuestionAudio(index, questionMapItem.question);
			}
		}
	}

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: GridView.builder(
          itemCount: _buildGridCount(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          itemBuilder: (context, index) {
            return _buildGridItem(context, index);
          },
        ),
      )
    );
  }
}

class _MappingQuestionItem {
	bool selected;

  final QuestionMapItem questionMapItem;

	_MappingQuestionItem({
		this.questionMapItem,
		this.selected = false,
	});
}