import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';

import 'base/abstract_state.dart';

class ScheduleScreen extends StatefulWidget {

  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();

}

/// @ref https://flutter.dev/docs/cookbook/lists/mixed-list
class _ScheduleScreenState extends AbstractState<ScheduleScreen> {

  List<Schedule> schedules = List();

  static final DateFormat _dateFormat1 = DateFormat("dd/MM");
  static final DateFormat _dateFormat2 = DateFormat("HH:mm");

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

      _getSchedules();
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  void _getSchedules() async {
    setState(() {
      loaded = AbstractState.LOAD_LOADING;
    });
    await Api.getSchedules(accessToken.accessToken).then((_schedules) {
      setState(() {
        this.schedules = _schedules;
        loaded = AbstractState.LOAD_FINISH;
      });
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        loaded = AbstractState.LOAD_FINISH;
      });
    });
  }

  void _getLeaveSchedule(int planId) async {
    setState(() {
      loaded = AbstractState.LOAD_LOADING;
    });
    await Api.getLeaveSchedule(accessToken.accessToken, planId).then((_result) {
      setState(() {
        loaded = AbstractState.LOAD_FINISH;
      });
      if(_result.status) {
        showSnackBar('Xin nghỉ thành công!');
      }
      _getSchedules();
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        loaded = AbstractState.LOAD_FINISH;
      });
      _getSchedules();
    });
  }

  TableRow _buildTableRow(Shift shift, String title, Schedule schedule) {
    return TableRow(
      children: [
        Container(
          alignment: Alignment(0, 0),
          height: 40,
          child: Text(shift == null ? title : '$title (${_dateFormat2.format(shift.dtStartTime)})'),
        ),
        Container(
          alignment: Alignment(0, 0),
          height: 40,
          decoration: shift == null ? BoxDecoration(
            color: MyColors.titleBackground,
          ): null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: shift != null && schedule.userAttendanceShift == shift.id ?
              <Widget>[
                Text(shift == null ? 'Không có ca học' : shift.name),
                const Icon(FontAwesome.check_circle,
                  color: Colors.green,
                  size: 18,
                )
              ] : <Widget>[
                Text(shift == null ? 'Không có ca học' : shift.name),
              ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if(loaded == AbstractState.LOAD_LOADING) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: 40,
              decoration: BoxDecoration(
                  color: MyColors.titleBackground
              ),
              alignment: AlignmentDirectional(0, 0),
              child: Text('Thời khóa biểu'.toUpperCase(),
                textAlign: TextAlign.center,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: schedules == null || schedules.isEmpty ?
              Center(
                child: const Text('Hiện tại chưa có lịch học',
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
              ) :
              ListView.builder(
                itemCount: schedules.length,
                itemBuilder: (context, index) {
                  final Schedule schedule = schedules[index];
                  Shift morningShift, afternoonShift, nightShift;
                  schedule.shifts.forEach((shift) {
                    if(shift.dtStartTime.hour <= 12) {
                      morningShift = shift;
                    } else if(shift.dtStartTime.hour <= 17) {
                      afternoonShift = shift;
                    } else {
                      nightShift = shift;
                    }
                  });
                  return ExpansionTile(
                    title: Text(schedule.name,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: MyColors.colorPrimary,
                      ),
                    ),
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            border: Border.all(
                                color: MyColors.darkGray,
                                width: 0.5
                            )
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Table(
                              border: const TableBorder(
                                top: const BorderSide(),
                                right: const BorderSide(),
                                left: const BorderSide(),
                              ),
                              children: [
                                TableRow(
                                  children: [
                                    Container(
                                      alignment: Alignment(0, 0),
                                      height: 40,
                                      child: Text(schedule.shifts.isEmpty ? '' :
                                          '${Utils.getDateOfWeekString(schedule.shifts[0].dtStartTime.weekday)} (${_dateFormat1.format(schedule.shifts[0].dtStartTime)})'),
                                    ),
                                  ]
                                ),
                              ],
                            ),
                            Table(
                              border: TableBorder.all(),
                              children: [
                                _buildTableRow(morningShift, 'Sáng', schedule),
                                _buildTableRow(afternoonShift, 'Chiều', schedule),
                                _buildTableRow(nightShift, 'Tối', schedule),
                              ],
                            ),
                            schedule.leftSchedule ? Container() : CommonWidget.smallHorizontalDivider,
                            schedule.leftSchedule ? Container() : AbsorbPointer(
                              absorbing: loaded == AbstractState.LOAD_LOADING,
                              child: RaisedButton(
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          content: Text('Buổi học này rất quan trọng bạn có muốn xin nghỉ không?',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: MyColors.colorPrimary,
                                            ),
                                          ),
                                          actions: <Widget>[
                                            FlatButton(
                                              child: Text('Có'),
                                              onPressed: () {
                                                Navigator.pop(context);
                                                _getLeaveSchedule(schedule.planId);
                                              },
                                            ),
                                            FlatButton(
                                              child: Text('Không',
                                                style: TextStyle(
                                                  color: MyColors.colorPrimary,
                                                ),
                                              ),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ],
                                        );
                                      }
                                  );
                                },
                                textColor: Colors.white,
                                color: MyColors.colorPrimary,
                                child: Text('Xin nghỉ'),
                                padding: EdgeInsets.all(15),
                              ),
                            ),
                            CommonWidget.smallHorizontalDivider,
                            Divider(
                              color: MyColors.darkGray,
                              height: 0.5,
                            ),
                            CommonWidget.smallHorizontalDivider,
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(padding: EdgeInsets.symmetric(horizontal: 4),
                                  child: Text('Phòng: ${schedule.room}',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Padding(padding: EdgeInsets.symmetric(horizontal: 4),
                                  child: Text('Nhóm: ${schedule.group}',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Html(
                                  data: schedule.content,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                    initiallyExpanded: index == 0 ? true : false,
                  );
                },
              ),
            ),
          ],
        ),
      );
    }
  }

}