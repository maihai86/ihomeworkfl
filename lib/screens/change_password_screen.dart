import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:http/http.dart' as http;
import 'package:ihomework/utils/my_widget.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/utils/api.dart';

import 'base/abstract_state.dart';

class ChangePasswordScreen extends StatefulWidget {

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();

}

class _ChangePasswordScreenState extends AbstractState<ChangePasswordScreen> {
  var _currPassCtrl;
  var _newPassCtrl;
  var _retypePassCtrl;

  final _formKey = GlobalKey<FormState>();

  int _loaded = 0;

  @override
  void initState() {
    super.initState();

    _currPassCtrl = new TextEditingController();
    _newPassCtrl = new TextEditingController();
    _retypePassCtrl = new TextEditingController();
  }

  @override
  bool shouldInitFCM() {
    return true;
  }
  
  void _changePassword() {
    setState(() {
      this._loaded = 1;
    });

    Api.postChangePassword(accessToken.accessToken, _currPassCtrl.text, _newPassCtrl.text, _retypePassCtrl.text).then((_changePassword) {
      setState(() {
        this._loaded = 2;
      });
      if(_changePassword != null && _changePassword.message != null && _changePassword.message.contains('successful')) {
        Navigator.pop(context, 0);
      } else {
        showSnackBar('Đổi mật khẩu không thành công');
      }
    }, onError: (error, stacktrace) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        this._loaded = 2;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: CommonWidget.appBar(accessToken),
      body: Container(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: 40,
                decoration: BoxDecoration(
                    color: MyColors.titleBackground
                ),
                alignment: AlignmentDirectional(0, 0),
                child: Text("Đổi mật khẩu".toUpperCase(),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 40, left: 40, right: 40),
                  child: AbsorbPointer(
                    absorbing: _loaded == 1,
                    child: TextFormField(
                      controller: _currPassCtrl,
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Mật khẩu hiện tại',
                      ),
                      validator: (value) {
                        if(value.isEmpty) {
                          return 'Mật khẩu hiện tại không được để trống';
                        }
                        return null;
                      },
                    ),
                  )
              ),
              Container(
                  margin: EdgeInsets.only(top: 10, left: 40, right: 40),
                  child: AbsorbPointer(
                    absorbing: _loaded == 1,
                    child: TextFormField(
                      controller: _newPassCtrl,
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Mật khẩu mới',
                      ),
                      validator: (value) {
                        if(value.isEmpty) {
                          return 'Mật khẩu mới không được để trống';
                        }
                        return null;
                      },
                    ),
                  )
              ),
              Container(
                  margin: EdgeInsets.only(top: 10, left: 40, right: 40),
                  child: AbsorbPointer(
                    absorbing: _loaded == 1,
                    child: TextFormField(
                      controller: _retypePassCtrl,
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Nhập lại mật khẩu mới',
                      ),
                      validator: (value) {
                        if(value.isEmpty) {
                          return 'Nhập lại mật khẩu mới không được để trống';
                        }
                        if(_newPassCtrl.text != value) {
                          return 'Nhập lại mật khẩu mới phải giống Mật khẩu mới';
                        }
                        return null;
                      },
                    ),
                  )
              ),
              Container(
                  margin: EdgeInsets.only(top: 20, left: 40, right: 40),
                  child: AbsorbPointer(
                    absorbing: _loaded == 1,
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.red,
                      child: Text("Thay đổi mật khẩu"),
                      padding: EdgeInsets.all(15),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          // change password

                          _changePassword();
                        }
                      },
                    ),
                  )
              ),
              Divider(
                color: Colors.transparent,
                height: 20,
              ),
              Visibility(
                visible: _loaded == 1,
                child: Column(
                  children: <Widget>[
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            ],
          ),
        )
      ),
    );
  }

}