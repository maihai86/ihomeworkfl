import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamSpeakingScreen extends AbstractExerciseExamScreen {

//	final Stage stage;
//
//	final int questionIdx;
//
//	final Question question;
//
//	final int Function() incorrectQuestionCount;
//
//	final void Function(int questionIdx, bool correctAnswer) nextQuestion;
//
//	final void Function(String url) playSoundUrl;
//
//	final void Function(List<String> urls, int soundIndex) playSoundUrls;
//
//	final void Function() stopSoundUrl;
//
//	final void Function(int questionIdx, bool correct) updateCorrectAnswer;

	ExerciseExamSpeakingScreen({
		key,
		stage,
		questionIdx,
		question,
		incorrectQuestionCount,
		nextQuestion,
		playSoundUrl,
		playSoundUrls,
		playSmallSoundUrl,
		stopSoundUrl,
		updateCorrectAnswer,
	}): /*assert(stage != null),*/
//			assert(questionIdx != null),
//			assert(question != null),
//			assert(nextQuestion != null),
//			assert(playSoundUrl != null),
//			assert(playSoundUrls != null),
//			assert(stopSoundUrl != null),
//			assert(updateCorrectAnswer != null),
			super(
				key: key,
				stage: stage,
				questionIdx: questionIdx,
				question: question,
				incorrectQuestionCount: incorrectQuestionCount,
				nextQuestion: nextQuestion,
				playSoundUrl: playSoundUrl,
				playSoundUrls: playSoundUrls,
				playSmallSoundUrl: playSmallSoundUrl,
				stopSoundUrl: stopSoundUrl,
				updateCorrectAnswer: updateCorrectAnswer,);

  @override
  State<StatefulWidget> createState() => ExerciseExamSpeakingScreenState();

}

class ExerciseExamSpeakingScreenState extends AbstractExerciseScreenState<ExerciseExamSpeakingScreen> {

  int incorrectAnswerCount = 0;

  Timer _timer;

	int nextQuestionCd;
  ///
  
  @override
  void initState() {
    super.initState();

		nextQuestionCd = 0;
    if(widget.stage.type == null || widget.stage.type == StageType.ONE) {
			_playQuestionAudio();
		}
  }

  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();

    super.dispose();
  }

	@override
	bool shouldInitFCM() {
		return false;
	}


	@override
	void afterLoadSharedPreferences() {
		super.afterLoadSharedPreferences();

		nextQuestionCountDown();
	}

  Widget _buildPlayQuestionAudioButton() {
    if(widget.question == null || widget.question.questionAudio.isEmpty) {
      return CommonWidget.emptyButtonContainer;
    } else if(playSoundUrlState == PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_up,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () async {
					await widget.stopSoundUrl();
        },
      );
    } else if(playSoundUrlState != PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_off,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: () {
          _playQuestionAudio();
        },
      );
    } else {
      return CommonWidget.emptyButtonContainer;
    }
  }

  void _playQuestionAudio() {
  	print('$runtimeType, _playQuestionAudio:${widget.question.questionAudio}');
    if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
			widget.playSoundUrls(widget.question.questionAudio, 0);
    }
  }

	void nextQuestionCountDown() {
		if(_timer != null) {
			_timer.cancel();
		}
		_timer = Timer(Duration(milliseconds: Constants.DEFAULT_COUNTDOWN_MILLIS), () {
			setState(() {
				nextQuestionCd += Constants.DEFAULT_COUNTDOWN_MILLIS;
			});
			if(nextQuestionCd >= widget.question.time * 1000) {
//				widget.stopSoundUrl();
//				widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
			} else {
				nextQuestionCountDown();
			}
		});
	}

	Widget _buildQuestionPicture() {
		if(Utils.isEmptyString(widget.question.questionPicture)) {
			return Container();
		} else if(Utils.isImageUrl(widget.question.questionPicture)) {
			return ConstrainedBox(
				constraints: BoxConstraints(
						maxHeight: 150
				),
				child: Image.network(widget.question.questionPicture),
			);
		} else {
			return Container(
				padding: EdgeInsets.symmetric(
						horizontal: 0.0,
						vertical: 25
				),
				constraints: BoxConstraints(
					maxHeight: 150,
				),
				alignment: AlignmentDirectional(0, 0),
				child: Text(widget.question.questionPicture,
					style: TextStyle(
							fontSize: 40,
							fontWeight: FontWeight.w400
					),
				),
				decoration: BoxDecoration(
						border: Border.all(
								color: MyColors.colorPrimary,
								width: 1.0
						)
				),
			);
		}
	}

  ///
  /**
   * Question example:
   * {
			"id": 1,
			"type": "Speaking",
			"question_picture": "greenhouse",
			"question": "greenhouse - /ˈɡriːnhaʊs/ - (nc)",
			"answer_text": "",
			"hint_text": "",
			"question_audio": "http://media.tataenglish.com/audio_en/g/greenhouse_nc_uk.mp3"
			}
   */
  ///
  @override
  Widget build(BuildContext context) {
		return Container(
			child: Padding(
				padding: EdgeInsets.all(10),
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						/// header section
						Row(
							children: <Widget>[
								Container(
									alignment: Alignment(0, 0),
									width: 32,
									height: 32,
									decoration: BoxDecoration(
											shape: BoxShape.circle,
											color: MyColors.colorPrimary
									),
									child: Text('${widget.questionIdx + 1}',
										style: TextStyle(
												color: Colors.white,
												fontWeight: FontWeight.bold,
												fontSize: 16
										),
									),
								),
								CommonWidget.smallVerticalDivider,
								Expanded(
									child: LinearProgressIndicator(
										backgroundColor: MyColors.colorPrimaryDark,
										value: (widget.questionIdx + 1) / widget.stage.questions.length,
									),
								),
								CommonWidget.smallVerticalDivider,
								Text('${widget.stage.questions.length - widget.incorrectQuestionCount()}/${widget.stage.questions.length}',
									style: TextStyle(
										fontWeight: FontWeight.bold,
									),
								),
							],
						),
						/// END header section
						CommonWidget.mediumHorizontalDivider,
						/// question section
						_buildQuestionPicture(),
						CommonWidget.smallHorizontalDivider,
						Row(
							crossAxisAlignment: CrossAxisAlignment.center,
							children: <Widget>[
								Icon(FontAwesome.question,
									color: MyColors.colorPrimary,
									size: 30,
								),
								CommonWidget.smallVerticalDivider,
								Expanded(
									child: Text(widget.question.question,
										style: TextStyle(
											fontWeight: FontWeight.w600,
											fontSize: 18,
										),
									),
								),
							],
						),
						/// END question section
						/// manipulation section
						CommonWidget.mediumHorizontalDivider,
						Expanded(
							child: Container(),
						),
						/// END manipulation section
						/// bottom section
						CommonWidget.smallHorizontalDivider,
						Container(
							padding: EdgeInsets.all(8),
							child: Column(
								crossAxisAlignment: CrossAxisAlignment.center,
								children: <Widget>[
									GestureDetector(
										onTap: nextQuestionCd < widget.question.time * 1000 ? null : () async {
											await widget.stopSoundUrl();
											widget.updateCorrectAnswer(widget.questionIdx, true);
											widget.nextQuestion(widget.questionIdx, true);
										},
										child: Stack(
											alignment: AlignmentDirectional(0, 0),
											children: <Widget>[
												SizedBox(
													width: 60.0,
													height: 60.0,
													child: CircularProgressIndicator(
														backgroundColor: MyColors.colorPrimary,
														valueColor: AlwaysStoppedAnimation<Color>(MyColors.colorAccent),
														value: nextQuestionCd / (widget.question.time * 1000),
														strokeWidth: 6.0,
													),
												),
												Text(nextQuestionCd < widget.question.time * 1000 ? '' : 'Next'),
											],
										),
									),
								],
							)
						),
						/// END bottom section
					],
				),
			),
		);
  }
}