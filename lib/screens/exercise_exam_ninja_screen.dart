import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/widgets/pin_view.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';
import 'exercise_exam_typing_screen.dart';

class ExerciseExamNinjaScreen extends StatefulWidget {

  final Stage stage;

  final int Function() incorrectQuestionCount;

  final void Function(int questionIdx, bool correctAnswer) nextQuestion;

  final void Function() finishQuestion;

//  final void Function(String url) playSoundUrl;
//
//  final void Function() stopSoundUrl;

  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

	final ExerciseExamPageScreenState exerciseExamPageScreenState;

	final Function() numIncorrect;

	final int secondExam;

	ExerciseExamNinjaScreen({
    key,
    this.stage,
    this.incorrectQuestionCount,
    this.nextQuestion,
    this.finishQuestion,
//    this.playSoundUrl,
//    this.stopSoundUrl,
    this.updateCorrectAnswer,
		this.exerciseExamPageScreenState,
		this.numIncorrect,
		this.secondExam,
  }): assert(stage != null),
      assert(nextQuestion != null),
//      assert(playSoundUrl != null),
//      assert(stopSoundUrl != null),
      assert(updateCorrectAnswer != null),
      super(key: key);

  @override
  State<StatefulWidget> createState() => ExerciseExamNinjaScreenState();

}

class ExerciseExamNinjaScreenState extends AbstractExerciseScreenState<ExerciseExamNinjaScreen> {

	static const int ANSWER_NOT_RECORDED = 0;
	static const int ANSWER_INCORRECT = 1;
	static const int ANSWER_CORRECT = 2;

	int correctAnswer = ANSWER_NOT_RECORDED;
	int incorrectAnswerCount = 0;

  int _currQuestionIdx = 0;
	int _currFlowIndex = 0;
	List<int> answeredQuestionIdxes = List();
//	List<TextSpan> answeredQuestions = List();
//	List<TextSpan> answeredTranslates = List();
	List<Widget> _questionOriginButtons = List();
	List<GlobalKey<_FlowItemState>> _flowItemKeys;
	List<_TextWithColor> answeredQuestion2s = List();
	List<_TextWithColor> answeredTranslate2s = List();

	ScrollController _controller;

  int getCurrQuestionIndex() {
  	return _currQuestionIdx;
  }

	/// haimt: audioPlayer
	/// in order to play remote url
	bool isLocal = false;
	PlayerMode mode = PlayerMode.MEDIA_PLAYER;

	AudioPlayer _audioPlayer;

	StreamSubscription _durationSubscription;
	StreamSubscription _positionSubscription;
	StreamSubscription _playerCompleteSubscription;
	StreamSubscription _playerErrorSubscription;
	StreamSubscription _playerStateSubscription;

	String soundUrl = null;
	List<String> soundUrls = null;
	int currSoundIndex = 0;
//	String smallSoundUrl;
	///

	/// End screen
	int _loaded = 0;
	DoExerciseResult result;
	int _submit = 0;

  @override
  void initState() {
    super.initState();

		_controller = ScrollController();
		_initAudioPlayer();
  }

  @override
  void dispose() {
		_controller.dispose();
		stopSoundUrl();
		_disposeAudioPlayer();

    super.dispose();
  }

  @override
  bool shouldInitFCM() {
    return false;
  }


	@override
	void afterLoadSharedPreferences() {
		super.afterLoadSharedPreferences();

		_initQuestionFlows();
	}

	int _buildItemCount() {
    return widget.stage.questions.length;
  }

	void _initAudioPlayer() {
		AudioPlayer.logEnabled = false;
		_audioPlayer = AudioPlayer(/*mode: mode*/);

		_durationSubscription =
			_audioPlayer.onDurationChanged.listen((duration) {
			});

		_positionSubscription =
			_audioPlayer.onAudioPositionChanged.listen((p) {
			});

		_playerCompleteSubscription =
			_audioPlayer.onPlayerCompletion.listen((event) {
				_onPlaySoundUrlComplete();
			});

		_playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
			print('audioPlayer error : $msg');
			showSnackBar(msg);
			updatePlaySoundUrlState(PlayerState.stopped);
		});

		_audioPlayer.onPlayerStateChanged.listen((state) {
			if (!mounted) return;
		});
	}

	void _disposeAudioPlayer() {
//		_audioPlayer.stop();
		_audioPlayer.release();
		_durationSubscription?.cancel();
		_positionSubscription?.cancel();
		_playerCompleteSubscription?.cancel();
		_playerErrorSubscription?.cancel();
		_playerStateSubscription?.cancel();
	}

	Widget _buildPrecisionIcon() {
		if(correctAnswer == ANSWER_CORRECT) {
			// haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true'
			return Row(
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Icon(FontAwesome.check,
						color: Colors.green,
						size: 42,
					),
				],
			);
		} else if(correctAnswer == ANSWER_INCORRECT) {
			// haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
			return Row(
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Icon(FontAwesome.close,
						color: Colors.red,
						size: 42,
					),
				],
			);
		} else {
			return CommonWidget.emptyButtonContainer;
		}
	}

	Future playSoundUrl(String url) async {
		this.soundUrl = url;
//    print('$runtimeType.playSoundUrl url:$url');
		final result = await _audioPlayer.play(this.soundUrl, volume: 2.0);
		updatePlaySoundUrlState(PlayerState.playing);
		return result;
	}

	Future playSoundUrlNew(int questionIndex, String url) async {
		this.soundUrl = url;
		this._currQuestionIdx = questionIndex;
//    print('$runtimeType.playSoundUrl url:$url');
		final result = await _audioPlayer.play(this.soundUrl, volume: 2.0);
		updatePlaySoundUrlState(PlayerState.playing);
		return result;
	}

	Future playSmallSoundUrl(String url) async {
//		this.smallSoundUrl = url;
		final result = await _audioPlayer.play(url, volume: 2.0);
		return result;
	}

	Future playSoundUrls(List<String> urls, int soundIndex) async {
		this.soundUrl = null;
		this.soundUrls = urls;
		this.currSoundIndex = soundIndex;
		final result = await _audioPlayer.play(Uri.encodeFull(this.soundUrls[currSoundIndex]), volume: 2.0);
		updatePlaySoundUrlState(PlayerState.playing);
		return result;
	}

	Future pauseSoundUrl() async {
		final result = await _audioPlayer.pause();
		updatePlaySoundUrlState(PlayerState.paused);
		return result;
	}

	Future stopSoundUrl() async {
  	this.soundUrl = null;
		this.soundUrls = null;
		final result = await _audioPlayer.stop();
		if (result == 1) {
			updatePlaySoundUrlState(PlayerState.stopped);
		}
		return result;
	}

	Future stopSoundUrlNew(int questionIndex) async {
  	this.soundUrl = null;
		this._currQuestionIdx = questionIndex;
		final result = await _audioPlayer.stop();
		if (result == 1) {
			updatePlaySoundUrlState(PlayerState.stopped);
		}
		return result;
	}

	Future stopSoundUrls() async {
  	this.soundUrls = null;
		this.soundUrl = null;
		final result = await _audioPlayer.stop();
		if (result == 1) {
			updatePlaySoundUrlState(PlayerState.stopped);
		}
		return result;
	}

	void _onPlaySoundUrlComplete() {
		if(soundUrl != null) playSoundUrl(soundUrl);
		if(soundUrls != null && soundUrls.isNotEmpty) playSoundUrls(soundUrls, currSoundIndex == soundUrls.length -1 ? 0 : currSoundIndex + 1);
	}

	void _getStageDetail(String stageIdExp) async {
		setState(() {
			_loaded = 1;
		});
		await Api.getStageDetailByStageExp(accessToken.accessToken, stageIdExp).then((_stage) {
			setState(() {
				_loaded = 2;
			});
			Navigator.pop(context);
			Navigator.push(context,
					MaterialPageRoute(
							builder: (context) => ExerciseExamPageScreen(ExerciseExamPageScreenState.TYPE_BY_STAGE, stage: _stage)));
		}, onError: (error) {
			print('error: $error');
			showSnackBar(error.toString());
			setState(() {
				_loaded = 2;
			});
		});
	}

	bool isSubmitted() {
  	return _submit == 1 || _submit == 2;
	}

	bool hasStageAudio() {
  	return widget.stage.stageAudio != null && widget.stage.stageAudio.isNotEmpty;
	}

	final BoxDecoration boxDecoration = BoxDecoration(
			color: Colors.transparent,
			borderRadius: BorderRadius.all(Radius.circular(5.0)),
			border: Border.all(
					color: Colors.black87,
					width: 2.0
			)
	);
	final Color textColor1 = Colors.black87;
	final Color textColor2 = Colors.green;

	final BoxDecoration selectedBoxDecoration = BoxDecoration(
			color: Colors.transparent,
			borderRadius: BorderRadius.all(Radius.circular(5.0)),
			border: Border.all(
					color: Colors.indigo,
					width: 2.0
			)
	);
	final Color selectedTextColor1 = Colors.indigo;
	final Color selectedTextColor2 = Colors.green;

	final BoxDecoration wrongBoxDecoration = BoxDecoration(
			color: Colors.transparent,
			borderRadius: BorderRadius.all(Radius.circular(5.0)),
			border: Border.all(
					color: Colors.red,
					width: 2.0
			)
	);
	final Color wrongTextColor1 = Colors.red;

	void _initQuestionFlows() {
		Question question = widget.stage.questions[_currQuestionIdx];
		_questionOriginButtons = List(question.questionOrigin.length);
		_flowItemKeys = List(question.questionOrigin.length);
		for(int index = 0; index < question.questionOrigin.length; index++) {
			_flowItemKeys[index] = GlobalKey();
			_questionOriginButtons[index] = _FlowItem(
				key: _flowItemKeys[index],
				exerciseExamNinjaScreenState: this,
				flow: question.questionFlow[index],
				origin: question.questionOrigin[index],
				flowIndex: index,
			);
		}
	}

	void onSelectFlow(int flowIndex, String origin) {
		Question question = widget.stage.questions[_currQuestionIdx];
		QuestionFlowItem flow = question.questionFlow.firstWhere((flow) => flow.key == _currFlowIndex);
		if(flow != null && Utils.normalizeAnswer(flow.value.trim().toLowerCase()) == Utils.normalizeAnswer(origin.trim().toLowerCase())) {
			// answer correct
			widget.updateCorrectAnswer(_currQuestionIdx, true);
			playSmallSoundUrl(Constants.CORRECT_URLS[0]);

			setState(() {
				_flowItemKeys[flowIndex].currentState.updateCorrectAnswer(ANSWER_CORRECT);
				this.correctAnswer = ANSWER_CORRECT;
				if(this._currFlowIndex == 0) {
					this.answeredTranslate2s = List();
				}
			  this.answeredTranslate2s.add(_TextWithColor(text: question.questionTranslate[_currFlowIndex], color: flow.color));
			  if(_currFlowIndex >= widget.stage.questions[_currQuestionIdx].questionFlow.length - 1) {
			  	// next question
					for(int i = 0; i <= _currFlowIndex; i++) {
						if(i < _currFlowIndex) {
							this.answeredQuestion2s.add(_TextWithColor(text: question.questionOrigin[i], color: question.questionFlow[i].color));
						} else {
							this.answeredQuestion2s.add(_TextWithColor(text: question.questionOrigin[i] + '\n', color: question.questionFlow[i].color));
						}
						_controller.animateTo(_controller.position.maxScrollExtent, duration: const Duration(milliseconds: 200), curve: Curves.ease);
					}
					this._currFlowIndex = 0;
					if(this._currQuestionIdx >= widget.stage.questions.length - 1) {
						// finish
						_questionOriginButtons = new List();
						widget.finishQuestion();
					} else {
						this._currQuestionIdx = _currQuestionIdx + 1;
						_initQuestionFlows();
					}
				} else {
					this._currFlowIndex = _currFlowIndex+1;
				}
			});
		} else {
			widget.updateCorrectAnswer(_currQuestionIdx, false);
			playSmallSoundUrl(Constants.INCORRECT_URLS[0]);
			setState(() {
				this.correctAnswer = ANSWER_INCORRECT;
				_flowItemKeys[flowIndex].currentState.updateCorrectAnswer(ANSWER_INCORRECT);
			});
		}
	}

	List<TextSpan> _buildTextSpan(List<_TextWithColor> answeredQuestion2s) {
		List<TextSpan> result = List();
		answeredQuestion2s.forEach((textColor) {
			result.add(TextSpan(
				text: textColor.text,
				style: TextStyle(
						color: textColor.color == 1 ? selectedTextColor1 : selectedTextColor2
				),
			));
		});
		return result;
	}

	///
	/**
	 * {
			"id": 6001542,
			"type": "ninja",
			"question_picture": null,
			"question_origin": [
			"I've never ",
			" had ",
			"this ",
			" problem",
			" before, ",
			" but ",
			" I've just ",
			" bought ",
			" a ",
			"new ",
			" smartphone."
			],
			"question_flow": [
			{
			"key": 0,
			"value": "I've never ",
			"color": 1
			},
			{
			"key": 1,
			"value": " had ",
			"color": 1
			},
			{
			"key": 2,
			"value": " problem",
			"color": 2
			},
			{
			"key": 3,
			"value": "this  before, ",
			"color": 2
			},
			{
			"key": 4,
			"value": " but ",
			"color": 1
			},
			{
			"key": 5,
			"value": " I've just ",
			"color": 1
			},
			{
			"key": 6,
			"value": " bought ",
			"color": 1
			},
			{
			"key": 7,
			"value": " a ",
			"color": 1
			},
			{
			"key": 8,
			"value": " smartphone.",
			"color": 2
			},
			{
			"key": 9,
			"value": "new ",
			"color": 2
			}
			],
			"question_translate": [
			"Tôi chưa bao giờ ",
			" gặp ",
			"vấn đề ",
			" này",
			" trước đây, ",
			" nhưng ",
			" tôi vừa ",
			" mua ",
			" một ",
			"chiếc điện thoại thông minh ",
			" mới."
			],
			"question": null,
			"answer_text": null,
			"hint_text": null,
			"question_audio": null
			}
	 */
	///
	@override
  Widget build(BuildContext context) {
		final indicatorValue = (_currQuestionIdx + 1) / widget.stage.questions.length;
		return Container(
			padding: EdgeInsets.all(10),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				mainAxisAlignment: MainAxisAlignment.center,
				children: <Widget>[
					/// header section
					Flexible(
						flex: 1,
						child: Column(
							crossAxisAlignment: CrossAxisAlignment.center,
							children: <Widget>[
								Row(
									children: <Widget>[
										Container(
											alignment: Alignment(0, 0),
											width: 32,
											height: 32,
											decoration: BoxDecoration(
													shape: BoxShape.circle,
													color: MyColors.colorPrimary
											),
											child: Text('${currSoundIndex + 1}',
												style: TextStyle(
														color: Colors.white,
														fontWeight: FontWeight.bold,
														fontSize: 16
												),
											),
										),
										CommonWidget.smallVerticalDivider,
										Expanded(
											child: LinearProgressIndicator(
												backgroundColor: MyColors.colorPrimaryDark,
												value: indicatorValue,
											),
										),
										CommonWidget.smallVerticalDivider,
										Text('${widget.stage.questions.length - widget.incorrectQuestionCount()}/${widget.stage.questions.length}',
											style: TextStyle(
												fontWeight: FontWeight.bold,
											),
										),
									],
								),
								/// END header section
								CommonWidget.mediumHorizontalDivider,
								/// question section
								Expanded(
									child: Container(
										alignment: Alignment.topLeft,
										child: SingleChildScrollView(
											controller: _controller,
											child: RichText(
												textAlign: TextAlign.start,
												text: TextSpan(
													style: const TextStyle(
														fontSize: 17,
													),
													children: _buildTextSpan(answeredQuestion2s),
												),
											),
										),
									)
								),
							],
						)
					),
					CommonWidget.mediumHorizontalDivider,
					Container(
						padding: EdgeInsets.all(10),
						alignment: AlignmentDirectional(0, 0),
						constraints: BoxConstraints(
								minHeight: 100
						),
						decoration: BoxDecoration(
								border: Border.all(
										color: MyColors.colorPrimary,
										width: 1.0
								)
						),
						child: RichText(
							textAlign: TextAlign.start,
							text: TextSpan(
								style: const TextStyle(
									fontSize: 17,
								),
								children: _buildTextSpan(answeredTranslate2s),
							),
						),
					),
					/// manipulation section
					CommonWidget.mediumHorizontalDivider,
					Container(
						alignment: Alignment.topLeft,
						constraints: BoxConstraints(
							minHeight: 104
						),
						child: Wrap(
							direction: Axis.horizontal,
							alignment:  WrapAlignment.start,
							runAlignment: WrapAlignment.start,
							crossAxisAlignment: WrapCrossAlignment.start,
							spacing: 4,
							runSpacing: 4,
							children: _questionOriginButtons,
						),
					),
					/// END manipulation section
					/// bottom section
					CommonWidget.mediumHorizontalDivider,
					SizedBox(
						height: 48,
						child: Row(
							mainAxisAlignment: MainAxisAlignment.center,
							crossAxisAlignment: CrossAxisAlignment.center,
							children: <Widget>[
								_buildPrecisionIcon(),
							],
						),
					)
					/// END bottom section
				],
			),
		);
  }
}

class _TextWithColor {

	final String text;
	final int color;

	_TextWithColor({this.text, this.color});

}

class _FlowItem extends StatefulWidget {

	final ExerciseExamNinjaScreenState exerciseExamNinjaScreenState;

	final int flowIndex;

	final QuestionFlowItem flow;

  final String origin;

  const _FlowItem({
		Key key,
		this.exerciseExamNinjaScreenState,
		this.flowIndex,
		this.flow,
		this.origin
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FlowItemState();
}

class _FlowItemState extends State<_FlowItem> {

	int correctAnswer = ExerciseExamNinjaScreenState.ANSWER_NOT_RECORDED;

  @override
  Widget build(BuildContext context) {
  	BoxDecoration decoration;
  	Color textColor;
  	if(correctAnswer == ExerciseExamNinjaScreenState.ANSWER_NOT_RECORDED) {
			decoration = widget.exerciseExamNinjaScreenState.boxDecoration;
			textColor = widget.exerciseExamNinjaScreenState.textColor1;
		} else if(correctAnswer == ExerciseExamNinjaScreenState.ANSWER_INCORRECT) {
			decoration = widget.exerciseExamNinjaScreenState.wrongBoxDecoration;
			textColor = widget.exerciseExamNinjaScreenState.wrongTextColor1;
		} else {
			decoration = widget.exerciseExamNinjaScreenState.selectedBoxDecoration;
			textColor = widget.flow.color == 1 ? widget.exerciseExamNinjaScreenState.selectedTextColor1 : widget.exerciseExamNinjaScreenState.selectedTextColor2;
		}
    return InkWell(
			onTap: correctAnswer == ExerciseExamNinjaScreenState.ANSWER_CORRECT ? null : () => widget.exerciseExamNinjaScreenState.onSelectFlow(widget.flowIndex, widget.origin),
			child: SizedBox(
					height: 48,
					child: Container(
						decoration: decoration,
						padding: EdgeInsets.symmetric(
								vertical: 14,
								horizontal: widget.origin.length <= 4 ? 10.0 : 5.0,
						),
						child: Text(widget.origin,
							style: TextStyle(
								color: textColor,
								fontWeight: FontWeight.w600,
							),
						),
					)
			),
		);
  }

  void updateCorrectAnswer(int selected) {
  	setState(() {
  	  this.correctAnswer = selected;
  	});
	}

}