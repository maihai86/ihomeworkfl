import 'package:flutter/material.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';

class ExerciseExamStartScreen extends StatefulWidget {
  final Stage stage;

  final void Function() startQuestion;

  final Future<dynamic> Function() checkPermission;

  ExerciseExamStartScreen({
    key,
    this.stage,
    this.startQuestion,
    this.checkPermission
  }) : super(key: key);

  @override
  ExerciseExamStartScreenState createState() => ExerciseExamStartScreenState();
}

class ExerciseExamStartScreenState extends State<ExerciseExamStartScreen> {

  bool hasPronunciationExercise() {
    if(this.widget.stage == null || widget.stage.questions == null) return false;
    for (Question q in widget.stage.questions) {
      if(q.type == QuestionType.PRONUNCIATION) {
        return true;
      }
    }
    return false;
  }

  bool hasAlphaExercise() {
    if(this.widget.stage == null || widget.stage.questions == null) return false;
    for (Question q in widget.stage.questions) {
      if(q.type == QuestionType.ALPHA) {
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 5,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('Bài: ${widget.stage.name}',
                    style: TextStyle(
                      fontSize: 25
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const Divider(
                    color: Colors.transparent,
                    height: 20,
                  ),
                  Text(widget.stage.guide == null ? '' : widget.stage.guide,
                    style: TextStyle(
                      fontSize: 20,
                      fontStyle: FontStyle.italic
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const Divider(
                    color: Colors.transparent,
                    height: 20,
                  ),
                  hasAlphaExercise() ?
                  Text('Chú ý: Với bài tập Alpha bạn phải để nguôn ngữ bàn phím là tiếng Anh.',
                    style: TextStyle(
                      color: Colors.red,
                      fontStyle: FontStyle.italic,
                      fontSize: 16,
                    ),
                  ) :
                  Container()
                ],
              )
            ),
          ),
          CommonWidget.largeHorizontalDivider,
          this.widget.stage == null || widget.stage.questions == null || widget.stage.questions.isEmpty ?
          Text('Hiện tại không có bài tập!',
            style: TextStyle(
              fontSize: 21
            ),
            textAlign: TextAlign.center,
          ) :
          Expanded(
            flex: 5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                IconButton(
                  iconSize: 120,
                  icon: Icon(FontAwesome.play_circle,
                    color: MyColors.colorAccent,
                  ),
                  onPressed: () async {
                    if(!hasPronunciationExercise()) {
                      widget.startQuestion();
                    } else if (await widget.checkPermission()) {
                      widget.startQuestion();
                    }
                  }
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}