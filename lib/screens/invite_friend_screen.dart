import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:http/http.dart' as http;
import 'package:ihomework/domain/access_token.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/utils/api.dart';

import 'base/abstract_state.dart';

class InviteFriendScreen extends StatefulWidget {

  final List<TataCenter> centers;

  const InviteFriendScreen({Key key, this.centers})
    : super(key: key);

  @override
  State createState() => _InviteFriendScreenState();

}

class _InviteFriendScreenState extends AbstractState<InviteFriendScreen> {
  TextEditingController _nameCtrl;
  TextEditingController _mobileCtrl;
  TextEditingController _universityCtrl;
  TextEditingController _areaCtrl;
  TextEditingController _introduceCtrl;
  TextEditingController _difficultCtrl;
  TextEditingController _expectationCtrl;
  int _job;
  String _comform;
  String _excited;

  final _formKey = GlobalKey<FormState>();

  int _centerId;

  SubmitIntroduceResult _result;

  @override
  void initState() {
    super.initState();

    _nameCtrl = new TextEditingController();
    _mobileCtrl = new TextEditingController();
    _universityCtrl = new TextEditingController();
    _areaCtrl = new TextEditingController();
    _introduceCtrl = new TextEditingController();
    _difficultCtrl = new TextEditingController();
    _expectationCtrl = new TextEditingController();
  }

  @override
  bool shouldInitFCM() {
    return true;
  }

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();
  }

  List<DropdownMenuItem<int>> _buildCenters() {
    return List.generate(widget.centers.length, (index) =>
      DropdownMenuItem<int>(
        value: widget.centers[index].id,
        child: Text(widget.centers[index].name),
      )
    );
  }

  void _postSubmitIntroduce() {
    setState(() {
      loaded = AbstractState.LOAD_LOADING;
    });
    Api.postSubmitIntroduce(accessToken.accessToken,
        SubmitIntroduceDto(
          fullName: _nameCtrl.text,
          phoneNumber: _mobileCtrl.text,
          centerTarget: _centerId,
          job: _job,
          school: _universityCtrl.text,
          liveArea: _areaCtrl.text,
          introducedText: _introduceCtrl.text,
          acceptable: _excited,
          trouble: _difficultCtrl.text,
          percentRegis: _comform
        )).then((_result) {
      setState(() {
        this._result = _result;
        loaded = AbstractState.LOAD_FINISH;
      });
      if(_result.status != 1) {
        showSnackBar('Không thành công: ${_result.message}');
      } else {
        showSnackBar('Thành công! Xin cảm ơn bạn!');
				Navigator.pop(context, _result.status);
      }
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        loaded = AbstractState.LOAD_FINISH;
      });
    });
  }

	String _emptyValidator<T>(T value) {
  	if(value == null) {
			return 'Không được để trống';
		} else if(value is String) {
			if(value == null || value.isEmpty) {
				return 'Không được để trống';
			}
		} else if(value is int) {
			if(value == null) {
				return 'Không được để trống';
			}
		}
		return null;
	}

  final _margin = const EdgeInsets.only(top: 20, left: 20, right: 20);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: CommonWidget.appBar(accessToken),
      body: loaded == AbstractState.LOAD_LOADING ?
      const Center(
        child: CircularProgressIndicator(),
      ) :
      Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: 40,
              decoration: BoxDecoration(
                  color: MyColors.titleBackground
              ),
              alignment: AlignmentDirectional(0, 0),
              child: Text(('Giới thiệu bạn bè').toUpperCase(),
                textAlign: TextAlign.center,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Form(
                    key: _formKey,
                    child: Padding(
											padding: EdgeInsets.symmetric(horizontal: 20),
											child: Column(
												crossAxisAlignment: CrossAxisAlignment.start,
												children: <Widget> [
													CommonWidget.smallHorizontalDivider,
													Text('1. Họ và tên bạn của em'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: TextFormField(
																	controller: _nameCtrl,
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																),
															)
													),
													CommonWidget.smallHorizontalDivider,
													Text('2. Số điện thoại của bạn ấy'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: TextFormField(
																	controller: _mobileCtrl,
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	keyboardType: TextInputType.number,
																	inputFormatters: <TextInputFormatter>[
																		WhitelistingTextInputFormatter.digitsOnly
																	],
																	validator: _emptyValidator,
																),
															)
													),
													CommonWidget.smallHorizontalDivider,
													Text('3. Công việc'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: DropdownButtonFormField<int>(
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																	value: _job,
																	items: [
																		DropdownMenuItem<int>(
																			value: 1,
																			child: const Text('Đi học',),
																		),
																		DropdownMenuItem<int>(
																			value: 2,
																			child: const Text('Đi làm',),
																		),
																	],
																	onChanged: (value) {
																		setState(() {
																			_job = value;
																		});
																	},
																),
															)
													),
													_job == 1 ? CommonWidget.smallHorizontalDivider : Container(),
													_job == 1 ? Text('3.1. Trường đang theo học') : Container(),
													_job == 1 ? Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: TextFormField(
																	controller: _universityCtrl,
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																),
															)
													) : Container(),
													CommonWidget.smallHorizontalDivider,
													Text('4. Bạn ấy muốn học cơ sở nào'),
													Container(
															child: Row(
																crossAxisAlignment: CrossAxisAlignment.center,
																children: <Widget>[
																	Expanded(
																			child: AbsorbPointer(
																				absorbing: loaded == AbstractState.LOAD_LOADING,
																				child: DropdownButtonFormField(
																					decoration: InputDecoration(
																						border: OutlineInputBorder(),
																					),
																					validator: _emptyValidator,
																					value: _centerId,
																					items: _buildCenters(),
																					onChanged: (value) {
																						setState(() {
																							_centerId = value;
																						});
																					},
																				),
																			)
																	),
																	_centerId == null ? Container() : CommonWidget.tinyVerticalDivider,
																	_centerId == null ? Container() : IconButton(
																			icon: Icon(FontAwesome.map,
																				color: MyColors.colorPrimary,
																				size: 28,
																			),
																			onPressed: () {
																				final _centers = widget.centers.where((center) => center.id == _centerId).toList();
																				if(_centers.isNotEmpty) {
																					launchURL(_centers[0].mapUrl);
																				} else {
																					showSnackBar('Bạn phải chọn cơ sở muốn học!');
																				}
																			}
																	),
																],
															)
													),
													CommonWidget.smallHorizontalDivider,
													Text('5. Bạn ấy hiện đang ở khu vực nào'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: TextFormField(
																	controller: _areaCtrl,
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																),
															)
													),
													CommonWidget.smallHorizontalDivider,
													Text('6. Em đã giới thiệu những gì về Tata cho bạn ấy rồi\n(Càng giới thiệu kỹ thì bạn ấy sẽ càng hiểu và dễ đăng ký hơn)'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: TextFormField(
																	controller: _introduceCtrl,
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																),
															)
													),
													CommonWidget.smallHorizontalDivider,
													Text('7. Theo em thì bạn ấy có phù hợp với 4 nét văn hóa của TATA không?'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: DropdownButtonFormField<String>(
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																	value: _comform,
																	items: [
																		DropdownMenuItem<String>(
																			value: 'Có',
																			child: const Text('Có',),
																		),
																		DropdownMenuItem<String>(
																			value: 'Không',
																			child: const Text('Không',),
																		),
																	],
																	onChanged: (value) {
																		setState(() {
																			_comform = value;
																		});
																	},
																),
															)
													),
													CommonWidget.smallHorizontalDivider,
													Text('8. Bạn ấy đang gặp khó khăn gì với môn tiếng Anh?'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: TextFormField(
																	controller: _difficultCtrl,
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																),
															)
													),
													CommonWidget.smallHorizontalDivider,
													Text('9. Bạn ấy kỳ vọng (mong đợi) điều gì khi học ở TATA?'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: TextFormField(
																	controller: _expectationCtrl,
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																),
															)
													),
													CommonWidget.smallHorizontalDivider,
													Text('10. Theo em mức độ hứng thú của bạn ấy về TATA là ở mức nào?'),
													Container(
															child: AbsorbPointer(
																absorbing: loaded == AbstractState.LOAD_LOADING,
																child: DropdownButtonFormField<String>(
																	decoration: const InputDecoration(
																		border: const OutlineInputBorder(),
																	),
																	validator: _emptyValidator,
																	value: _excited,
																	items: [
																		DropdownMenuItem<String>(
																			value: 'Rất thích',
																			child: const Text('Rất thích',),
																		),
																		DropdownMenuItem<String>(
																			value: 'Thích',
																			child: const Text('Thích',),
																		),
																		DropdownMenuItem<String>(
																			value: 'Bình thường',
																			child: const Text('Bình thường',),
																		),
																	],
																	onChanged: (value) {
																		setState(() {
																			_excited = value;
																		});
																	},
																),
															)
													),
												],
											),
										)
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: AbsorbPointer(
                        absorbing: loaded == AbstractState.LOAD_LOADING,
                        child: RaisedButton(
                          textColor: Colors.white,
                          color: MyColors.colorPrimary,
                          child: Text("OK"),
                          padding: EdgeInsets.all(15),
                          onPressed: () {
														if (_formKey.currentState.validate()) {
															_postSubmitIntroduce();
														}
													},
                        ),
                      ),
                    ),
                    CommonWidget.smallVerticalDivider,
                    Expanded(
                      child: AbsorbPointer(
                        absorbing: loaded == AbstractState.LOAD_LOADING,
                        child: RaisedButton(
                          child: Text("Đóng"),
                          padding: EdgeInsets.all(15),
                          onPressed: () => Navigator.pop(context),
                        ),
                      )
                    )
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}
