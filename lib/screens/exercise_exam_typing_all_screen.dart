import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:ihomework/widgets/pin_view.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';
import 'exercise_exam_typing_screen.dart';

class ExerciseExamTypingAllScreen extends StatefulWidget {

  final Stage stage;


  final int Function() incorrectQuestionCount;

  final void Function(int questionIdx, bool correctAnswer) nextQuestion;

  final void Function() finishQuestion;

//  final void Function(String url) playSoundUrl;
//
//  final void Function() stopSoundUrl;

  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

	final ExerciseExamPageScreenState exerciseExamPageScreenState;

	final Function() numIncorrect;

	final int secondExam;

  ExerciseExamTypingAllScreen({
    key,
    this.stage,
    this.incorrectQuestionCount,
    this.nextQuestion,
    this.finishQuestion,
//    this.playSoundUrl,
//    this.stopSoundUrl,
    this.updateCorrectAnswer,
		this.exerciseExamPageScreenState,
		this.numIncorrect,
		this.secondExam,
  }): assert(stage != null),
      assert(nextQuestion != null),
//      assert(playSoundUrl != null),
//      assert(stopSoundUrl != null),
      assert(updateCorrectAnswer != null),
      super(key: key)
  ;

  @override
  State<StatefulWidget> createState() => ExerciseExamTypingAllScreenState();

}

class ExerciseExamTypingAllScreenState extends AbstractExerciseScreenState<ExerciseExamTypingAllScreen> {

	static const int ANSWER_NOT_RECORDED = 0;
	static const int ANSWER_INCORRECT = 1;
	static const int ANSWER_CORRECT = 2;

	int correctAnswer = ANSWER_NOT_RECORDED;
	int incorrectAnswerCount = 0;

  AutoScrollController _controller;
  List<Widget> _pages;
  List<GlobalKey<_TypingItemState>> _globalKeys;
  List<FocusNode> _focusNodes;
  List<TextEditingController> _textEditingControllers;
	List<bool> answerTextFieldEnables;
	List<String> answerTextFieldErrorMessages;
  int _currQuestionIdx = 0;

  bool _initItemsState = false;

  int _answerCount = 0;

  int getCurrQuestionIndex() {
  	return _currQuestionIdx;
  }


	/// haimt: audioPlayer
	/// in order to play remote url
	bool isLocal = false;
	PlayerMode mode = PlayerMode.MEDIA_PLAYER;

	AudioPlayer _audioPlayer;

	StreamSubscription _durationSubscription;
	StreamSubscription _positionSubscription;
	StreamSubscription _playerCompleteSubscription;
	StreamSubscription _playerErrorSubscription;
	StreamSubscription _playerStateSubscription;

	String soundUrl = null;
	List<String> soundUrls = null;
	int currSoundIndex = 0;
//	String smallSoundUrl;
	///

	/// End screen
	int _loaded = 0;
	DoExerciseResult result;
	int _submit = 0;

  @override
  void initState() {
    super.initState();

    _controller = AutoScrollController();
		_initAudioPlayer();
  }

  @override
  void dispose() {
    _controller.dispose();
    stopSoundUrl();
		_disposeAudioPlayer();

		if(_focusNodes != null) {
			for(FocusNode fn in _focusNodes) {
				if(fn != null) {
					fn.dispose();
				}
			}
		}

		if(_textEditingControllers != null) {
			for(TextEditingController tec in _textEditingControllers) {
				if(tec != null) {
          tec.dispose();
				}
			}
		}

    super.dispose();
  }

  @override
  void afterLoadSharedPreferences() {
    super.afterLoadSharedPreferences();

    _initExercise();
		_initItems();
  }

  @override
  bool shouldInitFCM() {
    return false;
  }

  int _buildItemCount() {
    return widget.stage.questions.length;
  }

  Future scrollToIndex(int index) async {
  	if(index >= _focusNodes.length) {
			_currQuestionIdx = 0;
		} else {
			_currQuestionIdx = index;
		}
    await _controller.scrollToIndex(_currQuestionIdx);
		FocusScope.of(context).requestFocus(_focusNodes[_currQuestionIdx]);
		setState(() => _answerCount = _getAnsweredCount());

		if(_globalKeys[index] is GlobalKey<_TypingItemState>) {
			_globalKeys[index].currentState._playQuestionAudio();
		}
  }

  void _initExercise() {
    _pages = List(widget.stage == null || widget.stage.questions == null ? 0 : widget.stage.questions.length);
		_globalKeys = List(_pages.length);
		_focusNodes = List(_pages.length);
    _textEditingControllers = List(_pages.length);
		answerTextFieldEnables = List.generate(_pages.length, (index) => true);
		answerTextFieldErrorMessages = List.generate(_pages.length, (index) => null);
  }

	void _initItems() {
		for(var index = 0; index < _pages.length; index++) {
			Question question = widget.stage.questions[index];
			if (question == null) {
				_pages[index] = Container();
			} else {
				switch (question.type) {
					case QuestionType.TYPING:
						_globalKeys[index] = GlobalKey<_TypingItemState>();
						_focusNodes[index] = FocusNode();
						_focusNodes[index].addListener(() {
							if(_focusNodes[index].hasFocus) {
								if(!isSubmitted()) {
									scrollToIndex(index);
								} else {
									hideKeyboard();
								}
							}
						});
            _textEditingControllers[index] = TextEditingController();
						_pages[index] = AutoScrollTag(
							key: ValueKey(index),
							controller: _controller,
							index: index,
							child: _TypingItem(
								key: _globalKeys[index],
								questionIdx: index,
								question: question,
								exerciseExamTypingAllScreenState: this,
							),
//							child: ExerciseExamTypingScreen(
//								key: _globalKeys[index],
//								stage: widget.stage,
//								questionIdx: index,
//								question: question,
//								currentQuestionIndex: getCurrQuestionIndex,
//								incorrectQuestionCount: widget.incorrectQuestionCount,
//								nextQuestion: widget.nextQuestion,
//								playSoundUrl: playSoundUrl,
//								playSmallSoundUrl: playSmallSoundUrl,
//								stopSoundUrl: stopSoundUrl,
//								updateCorrectAnswer: widget.updateCorrectAnswer,
//								scrollToIndex: scrollToIndex,
//								focusNode: _focusNodes[index],
//                textEditingController: _textEditingControllers[index],
//								nextQuestionOrValidateAllAnswer: nextQuestionOrValidateAllAnswer
//							),
						);
						break;
					default:
						_globalKeys[index] = GlobalKey();
						_pages[index] = CommonWidget.emptyScreenWithNotice('Dạng bài tập chưa được hỗ trợ: ${question.type}!!');
						break;
				}
			}
		}
		setState(() {
		  _initItemsState = true;
		});
	}

	void _initAudioPlayer() {
		AudioPlayer.logEnabled = false;
		_audioPlayer = AudioPlayer(/*mode: mode*/);

		_durationSubscription =
			_audioPlayer.onDurationChanged.listen((duration) {
			});

		_positionSubscription =
			_audioPlayer.onAudioPositionChanged.listen((p) {
			});

		_playerCompleteSubscription =
			_audioPlayer.onPlayerCompletion.listen((event) {
				_onPlaySoundUrlComplete();
			});

		_playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
			print('audioPlayer error : $msg');
			showSnackBar(msg);
			for(var key in _globalKeys) {
				if(key is GlobalKey<_TypingItemState>) {
					key.currentState?.updatePlaySoundUrlState(PlayerState.stopped);
				}
			}
			updatePlaySoundUrlState(PlayerState.stopped);
		});

		_audioPlayer.onPlayerStateChanged.listen((state) {
			if (!mounted) return;
		});
	}

	void _disposeAudioPlayer() {
//		_audioPlayer.stop();
		_audioPlayer.release();
		_durationSubscription?.cancel();
		_positionSubscription?.cancel();
		_playerCompleteSubscription?.cancel();
		_playerErrorSubscription?.cancel();
		_playerStateSubscription?.cancel();
	}

	Widget _buildPrecisionIcon() {
		if(correctAnswer == ANSWER_CORRECT) {
			// haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true'
			return Row(
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Icon(FontAwesome.check,
						color: Colors.green,
						size: 42,
					),
				],
			);
		} else if(correctAnswer == ANSWER_INCORRECT) {
			// haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
			return Row(
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Icon(FontAwesome.close,
						color: Colors.red,
						size: 42,
					),
				],
			);
		} else {
			return Container();
		}
	}

	Widget _buildBottomContainer() {
		if(_loaded == 1) {
			return Container(
				child: Center(
					child: CircularProgressIndicator(),
				),
			);
		} else if(correctAnswer == ANSWER_NOT_RECORDED) {
			return Container(
				padding: EdgeInsets.all(8.0),
				alignment: AlignmentDirectional(0, 0),
				child: RaisedButton(
					textColor: Colors.white,
					color: MyColors.colorPrimary,
					child: Text(_getAnsweredCount() == widget.stage.questions.length ?
					"Nộp bài (${_getAnsweredCount()}/${widget.stage.questions.length})" :
					"Tiếp tục (${_getAnsweredCount()}/${widget.stage.questions.length})"),
					padding: EdgeInsets.all(15),
					onPressed: () {
						if(!isSubmitted() && _loaded != 1) {
							nextQuestionOrValidateAllAnswer();
						}
					},
				),
			);
		} else {
			return Container(
				padding: EdgeInsets.all(8.0),
				alignment: AlignmentDirectional(0, 0),
				child: Row(
					crossAxisAlignment: CrossAxisAlignment.center,
					mainAxisAlignment: MainAxisAlignment.spaceBetween,
					children: <Widget>[
						SizedBox(
							width: 100,
							child: FlatButton(
								onPressed: _onTryAgain,
								child: Text('Làm lại',
									style: TextStyle(
										color: Colors.red,
										fontWeight: FontWeight.bold,
									),
								),
							),
						),
						_buildPrecisionIcon(),
						result == null || Utils.isEmptyString(result.next) ?
						Container():
						SizedBox(
							width: 100,
							child: FlatButton(
								onPressed: _onNextExercise,
								child: Text('Tiếp theo',
									style: TextStyle(
										color: Colors.green,
										fontWeight: FontWeight.bold,
									),
								),
							),
						),
					],
				),
			);
		}
	}

	Future playSoundUrl(String url) async {
		this.soundUrl = url;
//    print('$runtimeType.playSoundUrl url:$url');
		final result = await _audioPlayer.play(this.soundUrl, volume: 2.0);
		if(_globalKeys[_currQuestionIdx] is GlobalKey<_TypingItemState>) {
			_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.playing);
		}
		updatePlaySoundUrlState(PlayerState.playing);
		return result;
	}

	Future playSoundUrlNew(int questionIndex, List<String> urls, int soundIndex) async {
		this.soundUrl = null;
		this.soundUrls = urls;
		this.currSoundIndex = soundIndex;
		this._currQuestionIdx = questionIndex;
//    print('$runtimeType.playSoundUrl url:$url');
		final result = await _audioPlayer.play(Uri.encodeFull(this.soundUrls[currSoundIndex]), volume: 2.0);
		if(_globalKeys[_currQuestionIdx] is GlobalKey<_TypingItemState>) {
			_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.playing);
		}
		updatePlaySoundUrlState(PlayerState.playing);
		return result;
	}

	Future playSmallSoundUrl(String url) async {
//		this.smallSoundUrl = url;
		final result = await _audioPlayer.play(url, volume: 2.0);
		return result;
	}

	Future pauseSoundUrl() async {
		final result = await _audioPlayer.pause();
		if(_globalKeys[_currQuestionIdx] is GlobalKey<_TypingItemState>) {
			_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.paused);
		}
		updatePlaySoundUrlState(PlayerState.paused);
		return result;
	}

	Future stopSoundUrl() async {
  	this.soundUrl = null;
		this.soundUrls = null;
		final result = await _audioPlayer.stop();
		if (result == 1) {
			if(_globalKeys[_currQuestionIdx] is GlobalKey<_TypingItemState> && _globalKeys[_currQuestionIdx].currentState != null && _globalKeys[_currQuestionIdx].currentState.mounted) {
				_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.stopped);
			}
			updatePlaySoundUrlState(PlayerState.stopped);
		}
		return result;
	}

	Future stopSoundUrlNew(int questionIndex) async {
  	this.soundUrl = null;
		this._currQuestionIdx = questionIndex;
		final result = await _audioPlayer.stop();
		if (result == 1) {
			if(_globalKeys[_currQuestionIdx] is GlobalKey<_TypingItemState>) {
				_globalKeys[_currQuestionIdx].currentState?.updatePlaySoundUrlState(PlayerState.stopped);
			}
			updatePlaySoundUrlState(PlayerState.stopped);
		}
		return result;
	}

	void _onPlaySoundUrlComplete() {
		if(soundUrl != null) playSoundUrl(soundUrl);
	}

	int _getAnsweredCount() {
    return _textEditingControllers.where((_controller) => !Utils.isEmptyString(_controller.text)).length;
  }

  void _checkAllAnswers() {
		setState(() {
			for(var index = 0; index < _textEditingControllers.length; index++) {
				if (Utils.normalizeAnswer(_textEditingControllers[index].text.trim().toLowerCase()) == Utils.normalizeAnswer(widget.stage.questions[index].answerText.trim().toLowerCase())) {
					// dung
					widget.updateCorrectAnswer(index, true);
					if(_globalKeys[index] is GlobalKey<_TypingItemState>) {
						_globalKeys[index].currentState.updateAnswerStatus(false, null);
					}
				} else {
					// sai
					widget.updateCorrectAnswer(index, false);
					if(_globalKeys[index] is GlobalKey<_TypingItemState>) {
						_globalKeys[index].currentState.updateAnswerStatus(false, 'Chưa chính xác');
					}
					this.answerTextFieldErrorMessages[index] = 'Chưa chính xác.';
				}
			}

//			print('$runtimeType._checkAllAnswers: errmsg=${this.answerTextFieldErrorMessages}, enable=${this.answerTextFieldEnables}');
			_postSubmitExerciseExamResult();
		});
  }

	void nextQuestionOrValidateAllAnswer() {
		if(_getAnsweredCount() == widget.stage.questions.length) {
			hideKeyboard();
			_setAllQuestionTextFieldEnable(false);
			_checkAllAnswers();
			stopSoundUrl();
//			widget.finishQuestion();
		} else {
			scrollToIndex(_currQuestionIdx + 1);
		}
	}

	void _setAllQuestionTextFieldEnable(bool enable) {
  	_globalKeys.forEach((key) {
			if(key is GlobalKey<_TypingItemState>) {
				key.currentState.updateAnswerStatus(enable, null);
			}
		});
		setState(() {
			for(int index = 0; index < answerTextFieldEnables.length; index++) {
				this.answerTextFieldEnables[index] = enable;
			}
		});
	}

	void _onTryAgain() {
		print("try again");
		stopSoundUrl();
		Navigator.pop(context);
		Navigator.push(context,
				MaterialPageRoute(
						builder: (context) => ExerciseExamPageScreen(ExerciseExamPageScreenState.TYPE_BY_STAGE, stage: widget.stage)));
	}

	void _onNextExercise() {
		print("next");
		stopSoundUrl();
		_getStageDetail(result.next);
	}

	Future _postSubmitExerciseExamResult() async {
		setState(() {
			_loaded = 1;
			_submit = 1;
		});
		Api.postSubmitExerciseExamResult(accessToken.accessToken,
				DoExerciseResultDto(stageId: widget.stage.stageId,
						numErrors: widget.numIncorrect(),
						timeDo: widget.secondExam,
						questionType: widget.stage.questions[0].type)).then((_result) {
			setState(() {
				this.result = _result;
				_loaded = 2;
			});

			if(result.pass) {
				playSmallSoundUrl(Constants.WIN_TUNE_URLS[0]);
				setState(() {
					this.correctAnswer = ANSWER_CORRECT;
				});
			} else {
				playSmallSoundUrl(Constants.FAIL_TUNE_URLS[0]);
				setState(() {
					this.correctAnswer = ANSWER_INCORRECT;
				});
			}
		}, onError: (error) {
			print('error: $error');
			showSnackBar('Không thể gửi kết quả làm bài tập: ${error.toString()}');
			setState(() {
				_loaded = 2;
			});
		});
	}

	void _getStageDetail(String stageIdExp) async {
		setState(() {
			_loaded = 1;
		});
		await Api.getStageDetailByStageExp(accessToken.accessToken, stageIdExp).then((_stage) {
			setState(() {
				_loaded = 2;
			});
			Navigator.pop(context);
			Navigator.push(context,
					MaterialPageRoute(
							builder: (context) => ExerciseExamPageScreen(ExerciseExamPageScreenState.TYPE_BY_STAGE, stage: _stage)));
		}, onError: (error) {
			print('error: $error');
			showSnackBar(error.toString());
			setState(() {
				_loaded = 2;
			});
		});
	}

	bool isSubmitted() {
  	return _submit == 1 || _submit == 2;
	}

	@override
  Widget build(BuildContext context) {
//    return ListView.builder(
//      controller: _controller,
//      itemCount: _buildItemCount(),
//      itemBuilder: (context, index) {
//        return _buildItem(index);
//      }
//    );
		if(!_initItemsState) {
			return Container();
		}
		return Column(
			crossAxisAlignment: CrossAxisAlignment.center,
			children: <Widget>[
				Expanded(
					child: widget.stage.type != null && widget.stage.type == StageType.ONE ?
					CommonWidget.emptyScreenWithNotice('Bài tập không được hỗ trợ dạng "All"!')
					: SingleChildScrollView(
						child: Column(
							crossAxisAlignment: CrossAxisAlignment.center,
							children: _pages,
						),
					),
//					: ListView(
//						controller: _controller,
//						children: _pages,
//						addAutomaticKeepAlives: true,
//					),
				),
				_buildBottomContainer(),
			],
		);
  }
}

class _TypingItem extends StatefulWidget {

	final int questionIdx;

	final Question question;

	final ExerciseExamTypingAllScreenState exerciseExamTypingAllScreenState;

  const _TypingItem(
		{Key key,
		this.questionIdx,
		this.question,
		this.exerciseExamTypingAllScreenState,
	}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TypingItemState();

}

class _TypingItemState extends State<_TypingItem> {

	/// play question audio
	PlayerState playSoundUrlState = PlayerState.stopped;

	bool answerTextFieldEnable = true;
	String answerTextFieldErrorMessage = null;

	void _playQuestionAudio() {
		print('$runtimeType, _playQuestionAudio:${widget.question.questionAudio}');
		if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
			widget.exerciseExamTypingAllScreenState.playSoundUrlNew(widget.questionIdx, widget.question.questionAudio, 0);
		}
	}

	Widget _buildPlayQuestionAudioButton() {
		if(widget.question == null || widget.question.questionAudio.isEmpty) {
			return Container();
		} else if(playSoundUrlState == PlayerState.playing) {
			return IconButton(
				icon: Icon(FontAwesome.volume_up,
					color: MyColors.colorPrimary,
				),
				iconSize: 30,
				onPressed: () {
					widget.exerciseExamTypingAllScreenState.stopSoundUrlNew(widget.questionIdx);
				},
			);
		} else if(playSoundUrlState != PlayerState.playing) {
			return IconButton(
				icon: Icon(FontAwesome.volume_off,
					color: MyColors.colorPrimary,
				),
				iconSize: 30,
				onPressed: () {
					_playQuestionAudio();
				},
			);
		} else {
			return Container();
		}
	}

	void updatePlaySoundUrlState(PlayerState playSoundUrlState) {
		setState(() {
			this.playSoundUrlState = playSoundUrlState;
		});
	}

	void updateAnswerStatus(bool enable, String errorMsg) {
		setState(() {
			this.answerTextFieldEnable = enable;
			this.answerTextFieldErrorMessage = errorMsg;
		});
	}

  @override
  Widget build(BuildContext context) {
//		print('$runtimeType.build: questionId=${widget.questionIdx}, enable=${answerTextFieldEnable}, '
//				'errmsg=${answerTextFieldErrorMessage}, submit=${widget.exerciseExamTypingAllScreenState.isSubmitted()}');
		return Container(
			padding: EdgeInsets.all(10),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.center,
				children: <Widget>[
					Utils.isImageUrl(widget.question.questionPicture) ?
					ConstrainedBox(
						constraints: BoxConstraints(
							maxHeight: 100
						),
						child: Image.network(widget.question.questionPicture),
					) :
					Container(
						padding: EdgeInsets.symmetric(
							horizontal: 8.0,
							vertical: 20
						),
						alignment: AlignmentDirectional(0, 0),
						child: Text(widget.question.questionPicture,
							style: TextStyle(
								fontSize: 32,
								fontWeight: FontWeight.w400
							),
						),
						decoration: BoxDecoration(
							border: Border.all(
								color: MyColors.colorPrimary,
								width: 1.0
							)
						),
					),
					CommonWidget.smallHorizontalDivider,
					Row(
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							Container(
								alignment: Alignment(0, 0),
								width: 32,
								height: 32,
								decoration: BoxDecoration(
									shape: BoxShape.circle,
									color: MyColors.colorPrimary
								),
								child: Text('${widget.questionIdx + 1}',
									style: TextStyle(
										color: Colors.white,
										fontWeight: FontWeight.bold,
										fontSize: 16
									),
								),
							),
							CommonWidget.smallVerticalDivider,
							Expanded(
								child: Text(widget.question.question,
									style: TextStyle(
										fontWeight: FontWeight.w600,
									),
								),
							),
							CommonWidget.smallVerticalDivider,
							_buildPlayQuestionAudioButton(),
						],
					),
					CommonWidget.smallHorizontalDivider,
					Row(
						crossAxisAlignment: CrossAxisAlignment.center,
						children: <Widget>[
							Icon(FontAwesome.keyboard_o,
								color: MyColors.colorPrimary,
								size: 30,
							),
							CommonWidget.smallVerticalDivider,
							Expanded(
								child: AbsorbPointer(
									absorbing: !answerTextFieldEnable || widget.exerciseExamTypingAllScreenState.isSubmitted(),
									child: TextFormField(
										controller: widget.exerciseExamTypingAllScreenState._textEditingControllers[widget.questionIdx],
										maxLines: 1,
										autofocus: true,
										keyboardType: TextInputType.text,
										textInputAction: TextInputAction.done,
										focusNode: widget.exerciseExamTypingAllScreenState._focusNodes[widget.questionIdx],
										onFieldSubmitted: (value) {
											if(!widget.exerciseExamTypingAllScreenState.isSubmitted()) {
												widget.exerciseExamTypingAllScreenState.nextQuestionOrValidateAllAnswer();
											}
										},
										decoration: Utils.isEmptyString(answerTextFieldErrorMessage) ?
										InputDecoration() :
										InputDecoration(
											errorText: answerTextFieldErrorMessage,
										),
									),
								),
							),
						],
					),
				],
			),
		);
  }

}