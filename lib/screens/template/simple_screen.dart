import 'package:flutter/material.dart';
import 'package:ihomework/utils/utils.dart';

class DummyScreen extends StatefulWidget {

  @override
  _DummyScreenState createState() => _DummyScreenState();

}

class _DummyScreenState extends State<DummyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dummy"),
      ),
      body: Center(
        child: Text("iHomework"),
      ),
    );
  }

}