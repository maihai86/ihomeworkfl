import 'dart:async';
import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:ihomework/domain/exercise.dart';
import 'package:ihomework/utils/api.dart';
import 'package:ihomework/utils/utils.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:animator/animator.dart';
import 'package:ihomework/utils/my_widget.dart';
import 'package:flutter_svg/svg.dart';

import 'base/abstract_exercise_exam_screen.dart';
import 'base/abstract_exercise_screen_state.dart';
import 'base/abstract_state.dart';
import 'exercise_exam_page_screen.dart';
import 'package:recorder_wav/recorder_wav.dart';
import 'package:audioplayers/audioplayers.dart';

class ExerciseExamPronunciationScreen extends AbstractExerciseExamScreen {

//  final Stage stage;
//
//  final int questionIdx;
//
//  final Question question;
//
//  final int Function() incorrectQuestionCount;
//
//  final void Function(int questionIdx, bool correctAnswer) nextQuestion;
//
//  final bool Function() checkSpeechRecognitionAvailable;
//
//  final void Function() startRecognition;
//
//  final void Function() stopRecognition;
//
//  final void Function() clearTranscription;
//
//  final void Function(String url) playSoundUrl;
//
//  final void Function(List<String> urls, int soundIndex) playSoundUrls;
//
//  final void Function(String url) playSmallSoundUrl;
//
//  final void Function() stopSoundUrl;
//
//  final void Function(int questionIdx, bool correct) updateCorrectAnswer;

//  final void Function() startPlaySpeaking;

  ExerciseExamPronunciationScreen({
    key,
    stage,
    questionIdx,
    question,
    incorrectQuestionCount,
    nextQuestion,
    playSoundUrl,
    playSoundUrls,
    playSmallSoundUrl,
    stopSoundUrl,
    updateCorrectAnswer,
//    this.checkSpeechRecognitionAvailable,
//    this.startRecognition,
//    this.stopRecognition,
//    this.clearTranscription,
//    this.startPlaySpeaking,
  }): /*assert(stage != null),*/
//      assert(questionIdx != null),
//      assert(question != null),
//      assert(nextQuestion != null),
//      assert(checkSpeechRecognitionAvailable != null),
//      assert(startRecognition != null),
//      assert(stopRecognition != null),
//      assert(clearTranscription != null),
//      assert(playSoundUrl != null),
//      assert(playSoundUrls != null),
//      assert(playSmallSoundUrl != null),
//      assert(stopSoundUrl != null),
//      assert(updateCorrectAnswer != null),
//      assert(startPlaySpeaking != null),
      super(
        key: key,
        stage: stage,
        questionIdx: questionIdx,
        question: question,
        incorrectQuestionCount: incorrectQuestionCount,
        nextQuestion: nextQuestion,
        playSoundUrl: playSoundUrl,
        playSoundUrls: playSoundUrls,
        playSmallSoundUrl: playSmallSoundUrl,
        stopSoundUrl: stopSoundUrl,
        updateCorrectAnswer: updateCorrectAnswer,
      );

  @override
  ExerciseExamPronunciationScreenState createState() => ExerciseExamPronunciationScreenState();

}

class ExerciseExamPronunciationScreenState extends AbstractExerciseScreenState<ExerciseExamPronunciationScreen> {

  static const int ANSWER_NOT_RECORDED = 0;
  static const int ANSWER_INCORRECT = 1;
  static const int ANSWER_CORRECT = 2;

  /// for speech recognition
  static const int STATUS_NOT_SPEECH = 0;
  static const int STATUS_RECORDING_VOICE = 1;
  static const int STATUS_FINISH_RECORDING = 2;
  static const int STATUS_UPLOADING_API = 3;
  static const int STATUS_GET_RESULT = 4;
  static const int STATUS_GET_RESULT_ERROR = 5;

  /// play question audio
//  bool speechRecognitionAvailable = false;
//  bool speechRecognitionListening = false;
  int speechStatus = STATUS_NOT_SPEECH; // not speech
  String transcription = '';
  DateTime transcriptTime;
  int correctAnswer = ANSWER_NOT_RECORDED;
  var incorrectAnswerCount = 0;
  Timer _timer;
  String currFilePath;
  AudioPlayer _localAudioPlayer;
  PlayerState playLocalSoundState = PlayerState.stopped;
//  RestartableTimer _setAnswerTimer;
  /// END play question audio
  ///

//  /// sound record
//  String recordedFilePath = '';
//  int playSpeakingState = 0;
//  /// END sound record
//
//  /// improve answer
//  String finalAnswer = '';
//  /// improve answer

  int _loaded = 0;

  Timer _nextQuestiontimer;
  int nextQuestionCd = 0;
  bool showNextQuestionCountDown = false;

  @override
  void initState() {
    super.initState();

    _localAudioPlayer = AudioPlayer();
    _playQuestionAudio();
//    _checkSpeechRecognitionAvailable();
  }

  @override
  void dispose() {
//    if (_setAnswerTimer != null && _setAnswerTimer.isActive) _setAnswerTimer.cancel();
    if (_timer != null && _timer.isActive) _timer.cancel();
    if (_nextQuestiontimer != null && _nextQuestiontimer.isActive) _nextQuestiontimer.cancel();
    _localAudioPlayer.dispose();

    super.dispose();
  }

	@override
	bool shouldInitFCM() {
		return false;
	}

  Widget _buildSpeechRecognitionButton() {
    if(showNextQuestionCountDown) {
      return Container(
          padding: EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 60.0,
                height: 60.0,
                child: CircularProgressIndicator(
                  backgroundColor: MyColors.colorPrimary,
                  valueColor: AlwaysStoppedAnimation<Color>(MyColors.colorAccent),
                  value: nextQuestionCd / (5 * 1000), // TODO test
                  strokeWidth: 6.0,
                ),
              ),
            ],
          )
      );
    }
    /*if (!speechRecognitionAvailable) {
      return Container(
        alignment: Alignment(0, 0),
        width: 80,
        height: 80,
        child: IconButton(
          icon: Icon(FontAwesome.microphone,
            color: Colors.black12,
            size: 36,
          ),
          onPressed: null,
        ),
      );
    } else */if (speechStatus != STATUS_RECORDING_VOICE) {
      return AbsorbPointer(
        absorbing: _loaded == 1,
        child: Container(
            alignment: Alignment(0, 0),
            width: 80,
            height: 80,
            child: IconButton(
              icon: Icon(FontAwesome.microphone,
                color: Colors.red,
                size: 36,
              ),
              onPressed: correctAnswer == ANSWER_CORRECT ? null : () async {
                await startRecording();
              },
            )
        ),
      );
    } else {
      return AbsorbPointer(
        absorbing: _loaded == 1,
        child: InkWell(
          onTap: speechStatus == STATUS_RECORDING_VOICE? () async {
            await stopRecording();
          } : null,
          child: Stack(
            alignment: Alignment(0, 0),
            children: <Widget>[
              Animator(
                tween: Tween<double>(begin: 0.95, end: 1.05),
                curve: Curves.elasticInOut,
                cycles: 0,
                duration: Duration(milliseconds: 100),
                builder: (anim) => Transform.scale(
                    scale: anim.value,
                    child: Container(
                      width: 80,
                      height: 80,
                      decoration: BoxDecoration(
                        color: Colors.red,
                        shape: BoxShape.circle,
                      ),
                    )
                ),
              ),
              Icon(FontAwesome.microphone,
                color: Colors.white,
                size: 36,
              ),
            ],
          ),
        ),
      );
    }
  }

  Widget _buildPrecisionIcon() {
    if(_loaded == 1) {
      return CircularProgressIndicator();
    }
    if(speechStatus == STATUS_NOT_SPEECH) {
      // haimt neu trang thai ghi am dang la 'Co ket qua' hoac 'Ket thuc' thi ko hien icon check
      return Container();
    } else if(speechStatus == STATUS_GET_RESULT && transcription == null || transcription.trim().isEmpty) {
      // haimt ghi am ko co transcription thi ko hien icon check
      return Container();
    } else if(correctAnswer == ANSWER_CORRECT) {
      // haimt neu ghi am nhan dien giong voi answerText thi hien icon check 'true' & stop ghi am
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.check,
            color: Colors.green,
            size: 68,
          ),
//          CommonWidget.smallVerticalDivider,
//          speechStatus != STATUS_FINISH
//              && recordedFilePath.isNotEmpty ?
//          IconButton(
//            icon: SvgPicture.asset('images/hearing.svg',
//              color: MyColors.colorPrimary,
//              width: 38,
//              height: 38,
//            ),
//            onPressed: playSpeakingState == 0 || playSpeakingState == 2 ? widget.startPlaySpeaking : null,
//          ) : Container(),
        ],
      );
    } else if(correctAnswer == ANSWER_INCORRECT) {
      // haimt neu ghi am nhan dien khac voi answerText thi hien icon check 'false'
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(FontAwesome.close,
            color: Colors.red,
            size: 68,
          ),
//          CommonWidget.smallVerticalDivider,
//          speechStatus != STATUS_FINISH
//              && recordedFilePath.isNotEmpty ?
//          IconButton(
//            icon: SvgPicture.asset('images/hearing.svg',
//              color: MyColors.colorPrimary,
//              width: 38,
//              height: 38,
//            ),
//            onPressed: () {
//              widget.startPlaySpeaking();
//            },
//          ) : Container(),
        ],
      );
    } else {
      return Container();
    }
  }

  Widget _buildPlayQuestionAudioButton() {
    if(widget.question == null || widget.question.questionAudio.isEmpty) {
      return CommonWidget.emptyButtonContainer;
    } else if(playSoundUrlState == PlayerState.playing) {
      return AbsorbPointer(
        absorbing: _loaded == 1,
        child: IconButton(
          icon: Icon(FontAwesome.volume_up,
            color: MyColors.colorPrimary,
          ),
          iconSize: 30,
          onPressed: () async {
            await widget.stopSoundUrl();
          },
        ),
      );
    } else if(playSoundUrlState != PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.volume_off,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: speechStatus == STATUS_RECORDING_VOICE ? null: () {
          _playQuestionAudio();
        },
      );
    } else {
      return CommonWidget.emptyButtonContainer;
    }
  }

//  Widget _buildNextQuestionButton() {
//    return incorrectAnswerCount >= Constants.INCORRECT_ANSWER_MAX || correctAnswer == ANSWER_CORRECT ?
//    IconButton(
//      icon: Icon(FontAwesome.arrow_right,
//        color: MyColors.colorPrimary,
//      ),
//      iconSize: 30,
//      onPressed: () {
//        widget.stopSoundUrl();
//        stopRecognition();
//        clearTranscription();
//        widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
//      },
//    ) : CommonWidget.emptyButtonContainer;
//  }

  void _playQuestionAudio() {
    if(widget.question != null && widget.question.questionAudio.isNotEmpty) {
      widget.playSoundUrls(widget.question.questionAudio, 0);
    }
  }

  Widget _buildReplayRecordedButton() {
    if(currFilePath == null || currFilePath.isEmpty) {
      return CommonWidget.emptyButtonContainer;
    } else if(playLocalSoundState == PlayerState.playing) {
      return AbsorbPointer(
        absorbing: _loaded == 1,
        child: IconButton(
          icon: Icon(FontAwesome.stop,
            color: MyColors.colorPrimary,
          ),
          iconSize: 30,
          onPressed: () {
            _localAudioPlayer.stop();
            setState(() {
              this.playLocalSoundState = PlayerState.stopped;
            });
          },
        ),
      );
    } else if(playLocalSoundState != PlayerState.playing) {
      return IconButton(
        icon: Icon(FontAwesome.assistive_listening_systems,
          color: MyColors.colorPrimary,
        ),
        iconSize: 30,
        onPressed: speechStatus == STATUS_RECORDING_VOICE ? null: () {
          _localAudioPlayer.play(currFilePath, isLocal: true, volume: 2.0);
          setState(() {
            this.playLocalSoundState = PlayerState.playing;
          });
        },
      );
    } else {
      return CommonWidget.emptyButtonContainer;
    }
  }

//  void _checkSpeechRecognitionAvailable() {
//    speechRecognitionAvailable = widget.checkSpeechRecognitionAvailable();
//    print('$runtimeType._checkSpeechRecognitionAvailable, speechRecognitionAvailable: $speechRecognitionAvailable');
//  }
//
//  void updateSpeechRecognitionListeningAndStatus(bool speechRecognitionListening, int speechStatus) {
//    if(this.speechStatus == STATUS_NOT_SPEECH) return;
//    if(this.speechRecognitionListening != speechRecognitionListening || this.speechStatus != speechStatus)
//    setState(() {
//      this.speechRecognitionListening = speechRecognitionListening;
//      this.speechStatus = speechStatus;
//    });
//  }
//
//  void updateSpeechRecognitionAvailable(bool speechRecognitionAvailable) {
//    if(this.speechRecognitionAvailable != speechRecognitionAvailable)
//    setState(() {
//      this.speechRecognitionAvailable = speechRecognitionAvailable;
//    });
//  }
//
//  void onRecognitionStarted() {
//    setState(() {
//      this.speechStatus = STATUS_START_RECOGNIZE;
//      this.speechRecognitionListening = true;
//      this.correctAnswer = ANSWER_NOT_RECORDED;
//      this.finalAnswer = '';
//    });
//  }
//
//  void onRecognitionComplete(String answer) {
//    setState(() {
//      this.speechRecognitionListening = false;
//      this.finalAnswer = answer;
//
//      if(finalAnswer == null || finalAnswer.trim().isEmpty) return;
//      bool _correct = false;
//      for(String answer in widget.question.answers) {
//        print('$runtimeType, left=${Utils.normalizeAnswer(answer.trim().toLowerCase())}, right=${Utils.normalizeAnswer(finalAnswer.trim().toLowerCase())}, result=${Utils.normalizeAnswer(answer.trim().toLowerCase()) == Utils.normalizeAnswer(finalAnswer.trim().toLowerCase())}');
//        if(Utils.normalizeAnswer(answer.trim().toLowerCase()) == Utils.normalizeAnswer(finalAnswer.trim().toLowerCase())) {
//          _correct = true;
//          break;
//        }
//      }
//      this.correctAnswer = _correct ? ANSWER_CORRECT : ANSWER_INCORRECT;
//      if(!_correct) {
//        widget.updateCorrectAnswer(widget.questionIdx, false);
//        this.incorrectAnswerCount = incorrectAnswerCount + 1;
//      } else {
//        widget.updateCorrectAnswer(widget.questionIdx, true);
//        widget.playSmallSoundUrl(Constants.CORRECT_URLS[0]);
//      }
//
//      this.speechStatus = STATUS_GET_RESULT;
//    });
//  }

  void onRecognitionResult(/*String transcription*/) {
    print('$runtimeType, onRecognitionResult:$transcription, speechStatus:$speechStatus');
//    if(this.speechStatus == STATUS_NOT_SPEECH || this.speechStatus == STATUS_FINISH_RECOGNIZE
//        || (this.correctAnswer == ANSWER_CORRECT && this.speechStatus == STATUS_GET_RESULT)) return;
//    if(this.transcription != transcription) {
//      setState(() {
//        this.correctAnswer = ANSWER_NOT_RECORDED;
//        this.transcription = transcription;
//        this.speechStatus = STATUS_SPEAKING;
//      });
//      _timerCheckCorrectAnswer();
//    }

    setState(() {
//      this.speechRecognitionListening = false;

      if(transcription == null || transcription.trim().isEmpty) return;
      bool _correct = false;
      for(String answer in widget.question.answers) {
        print('$runtimeType, left=${Utils.normalizeAnswer(answer.trim().toLowerCase())}, right=${Utils.normalizeAnswer(transcription.trim().toLowerCase())}, result=${Utils.normalizeAnswer(answer.trim().toLowerCase()) == Utils.normalizeAnswer(transcription.trim().toLowerCase())}');
        if(Utils.normalizeAnswer(answer.trim().toLowerCase()) == Utils.normalizeAnswer(transcription.trim().toLowerCase())) {
          _correct = true;
          break;
        }
      }
      this.correctAnswer = _correct ? ANSWER_CORRECT : ANSWER_INCORRECT;
      if(!_correct) {
        widget.updateCorrectAnswer(widget.questionIdx, false);
        this.incorrectAnswerCount += 1;

        if(this.incorrectAnswerCount >= 3) {
          // next question
          _loaded = 1;
          Timer(Duration(milliseconds: 500), () async {
            await widget.stopSoundUrl();
            widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
          });
        }
      } else {
        widget.updateCorrectAnswer(widget.questionIdx, true);
        widget.playSmallSoundUrl(Constants.CORRECT_URLS[0]);

        _loaded = 1;
        Timer(Duration(milliseconds: 500), () async {
          await widget.stopSoundUrl();
          widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
        });
      }

//      this.speechStatus = STATUS_GET_RESULT;
    });
  }

//  void clearTranscription() {
//    setState(() {
//      this.transcription = '';
//    });
//  }
//
//  void _timerCheckCorrectAnswer() {
//    if(_setAnswerTimer != null) {
//      _setAnswerTimer.cancel();
//    } else {
//
//    }
//    _setAnswerTimer = RestartableTimer(Duration(milliseconds: Constants.ANSWER_DELAY), () {
//      stopRecording();
//    });
//  }
//
//  void updateRecordedFilePath(String recordedFilePath) {
//    setState(() {
//      this.recordedFilePath = recordedFilePath;
//    });
//  }

  Future startRecording() async {
//    clearTranscription();
    await widget.stopSoundUrl();

    RecorderWav.startRecorder();

    String _filePath = currFilePath;
    setState(() {
      this.speechStatus = STATUS_RECORDING_VOICE;
//      this.speechRecognitionListening = true;
      this.correctAnswer = ANSWER_NOT_RECORDED;
      this.transcription = '';
      this.currFilePath = '';
    });
    if(_filePath != null && _filePath.isNotEmpty) {
      File(_filePath).delete();
    }

    _timer = Timer(Duration(seconds: widget.question.time), () async {
      await stopRecording();
    });
  }

  Future stopRecording() async {
  	if(_timer != null) {
			_timer.cancel();
		}

    currFilePath = await RecorderWav.StopRecorder();
//    updateSpeechRecognitionListeningAndStatus(true, STATUS_FINISH_RECOGNIZE);
    print('$runtimeType, recorded filePath=$currFilePath');
    var file = File(currFilePath);
    print('$runtimeType length=${file.lengthSync()}');

    setState(() {
      this.speechStatus = STATUS_FINISH_RECORDING;
    });

    return await _postUploadRecordedAudio(currFilePath);
  }

  Future _postUploadRecordedAudio(String filePath) async {
    setState(() {
      _loaded = 1;
      this.speechStatus = STATUS_UPLOADING_API;
    });
    await Api.postUploadRecordedAudio(filePath).then((_transcription) {

      print('$runtimeType, postUploadRecordedAudio:$_transcription');
      setState(() {
        _loaded = 2;
        this.speechStatus = STATUS_GET_RESULT;
        this.transcription = _transcription;
      });
      onRecognitionResult(/*_transcription*/);
    }, onError: (error) {
      print('error: $error');
      showSnackBar(error.toString());
      setState(() {
        _loaded = 2;
        this.speechStatus = STATUS_GET_RESULT_ERROR;
        this.transcription = '';
      });
    });
  }

  void nextQuestionCountDown() {
    if(_nextQuestiontimer != null) {
      _nextQuestiontimer.cancel();
    }
    _nextQuestiontimer = Timer.periodic(Duration(/*seconds: 1*/milliseconds: Constants.DEFAULT_COUNTDOWN_MILLIS), (Timer timer) async {
      setState(() {
        showNextQuestionCountDown = true;
        nextQuestionCd += Constants.DEFAULT_COUNTDOWN_MILLIS;
      });
      if(nextQuestionCd >= 5 * 1000) { // TODO test
        _nextQuestiontimer.cancel();
        await widget.stopSoundUrl();
        widget.nextQuestion(widget.questionIdx, correctAnswer == ANSWER_CORRECT);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
//    print('$runtimeType.build');
    final indicatorValue = (widget.questionIdx + 1) / widget.stage.questions.length;
    return Container(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            /// header section
            Row(
              children: <Widget>[
                Container(
                  alignment: Alignment(0, 0),
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: MyColors.colorPrimary
                  ),
                  child: Text('${widget.questionIdx + 1}',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 16
                    ),
                  ),
                ),
                CommonWidget.smallVerticalDivider,
                Expanded(
                  child: LinearProgressIndicator(
                    backgroundColor: MyColors.colorPrimaryDark,
                    value: indicatorValue,
                  ),
                ),
                CommonWidget.smallVerticalDivider,
                Text('${widget.stage.questions.length - widget.incorrectQuestionCount()}/${widget.stage.questions.length}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            /// END header section
            CommonWidget.mediumHorizontalDivider,
            /// question section
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(FontAwesome.question,
                            color: MyColors.colorPrimary,
                            size: 30,
                          ),
                          CommonWidget.smallVerticalDivider,
                          Flexible(
                            child: Text(widget.question.question,
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                              overflow: TextOverflow.visible,
                              softWrap: true,
                            ),
                          ),
                        ],
                      ),
                      CommonWidget.smallHorizontalDivider,
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SvgPicture.asset('images/speak.svg',
                            color: MyColors.colorAccent,
                            width: 30,
                            height: 30,
                          ),
                          CommonWidget.smallVerticalDivider,
                          Expanded(
                            child: Text(widget.question.sayIt,
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: 18,
                                color: MyColors.colorAccent,
                              ),
                              overflow: TextOverflow.visible,
                              softWrap: true,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
//                CommonWidget.smallVerticalDivider,
//                _buildPlayQuestionAudioButton(),
              ],
            ),
            /// END question section
            /// manipulation section
            Expanded(
              child: Padding(padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: Container(
                        alignment: AlignmentDirectional(0, 0.7),
                        child: Text(transcription,
                          style: TextStyle(
                            fontSize: 24,
//                          color: _buildSpeechResultColor(),
                            fontWeight: FontWeight.bold
                          ),
                          textAlign: TextAlign.center,
                        )
                      )
                    ),
                    Expanded(
                      flex: 6,
                      child: Container(
                        alignment: AlignmentDirectional(0, -0.5),
                        child: _buildPrecisionIcon(),
                      ),
                    ),
                  ],
                ),
              )
            ),
            /// END manipulation section
            /// bottom section
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _buildReplayRecordedButton(),
//                CommonWidget.emptyButtonContainer,
                _buildSpeechRecognitionButton(),
//                _buildNextQuestionButton(),
                _buildPlayQuestionAudioButton(),
              ],
            ),
            /// END bottom section
          ],
        ),
      ),
    );
  }
}