class AccessToken {
  final String accessToken;
  final String name;
  final int menuId;

  AccessToken({
    this.accessToken,
    this.name,
    this.menuId,
  }) : assert(accessToken != null && accessToken.isNotEmpty);

  factory AccessToken.fromJson(Map<String, dynamic> json) => AccessToken(
    accessToken: json['access_token'],
    name: json['name'],
    menuId: json['menu_id'],
  );

  Map<String, dynamic> toJson() => {
    'access_token': accessToken,
    'name': name,
    'menu_id': menuId,
  };
}

class ChangePassword {
  final String message;

  ChangePassword({
    this.message,
  });

  factory ChangePassword.fromJson(Map<String, dynamic> json) => ChangePassword(
    message: json['message'],
  );

  Map<String, dynamic> toJson() => {
    'message': message,
  };
}