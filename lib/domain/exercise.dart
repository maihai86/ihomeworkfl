import 'package:ihomework/utils/utils.dart';

class StudentStat {
  ListTest listTest;
  int diligenceScore;
	AllUserRankings usersRanking;
  TestChart chart;

  StudentStat({this.listTest, this.diligenceScore, this.usersRanking, this.chart});

  factory StudentStat.fromJson(Map<String, dynamic> json) => StudentStat(
    listTest: json['list_test'] == null ? null : ListTest.fromJson(json['list_test']),
    diligenceScore: json['diligence_score'],
    usersRanking: json['users_ranking'] == null ? null : AllUserRankings.fromJson(json['users_ranking']),
    chart: json['chart'] == null ? null : TestChart.fromJson(json['chart']),
  );

  Map<String, dynamic> toJson() => {
    'list_test': listTest,
    'diligence_score': diligenceScore,
    'users_ranking': usersRanking,
    'chart': chart,
  };
}

class ListTest {
  List<Test> tests;
  int lastTestId;
  String lastIndex;

  ListTest({this.tests, this.lastTestId, this.lastIndex});

  factory ListTest.fromJson(Map<String, dynamic> json) => ListTest(
    tests: (json['tests'] as List).map((item) => Test.fromJson(item)).toList(),
    lastTestId: json['last_test_id'],
    lastIndex: json['last_index'],
  );

  Map<String, dynamic> toJson() => {
    'tests': tests,
    'last_test_id': lastTestId,
    'last_index': lastIndex,
  };
}

class AllUserRankings {
	Map<String, UserRanking> topDone;
	UserRanking userRank;

	AllUserRankings({this.topDone, this.userRank});

	factory AllUserRankings.fromJson(Map<String, dynamic> json) => AllUserRankings(
		topDone: json['top_done'] == null ? Map() : (json['top_done'] as Map<String, dynamic>).map((String key, dynamic value) => MapEntry(key, UserRanking.fromJson(value))),
		userRank: json['user_rank'] == null ? null : UserRanking.fromJson(json['user_rank']),
	);

	Map<String, dynamic> toJson() => {
		'top_done': topDone,
		'user_rank': userRank,
	};
}

class UserRanking {
	int rank;
	String name;
	bool ownRank;
	int numDone;

	UserRanking({this.rank, this.name, this.ownRank, this.numDone});

	factory UserRanking.fromJson(Map<String, dynamic> json) => UserRanking(
		rank: json['rank'],
		name: json['name'],
		ownRank: json['own_rank'],
		numDone: json['num_done'],
	);

	Map<String, dynamic> toJson() => {
		'rank': rank,
		'name': name,
		'own_rank': ownRank,
		'num_done': numDone,
	};
}

class Test {
  int id;
  String name;
  TestInfo testMoreInfo;
  int workTime;

  Test({this.id, this.name, this.testMoreInfo, this.workTime});

  factory Test.fromJson(Map<String, dynamic> json) => Test(
    id: json['id'],
    name: json['name'],
    testMoreInfo: json['test-more-info'] == null ? null: TestInfo.fromJson(json['test-more-info']),
    workTime: json['work_time'],
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'test-more-info': testMoreInfo,
    'work_time': workTime,
  };
}

class TestInfo {
  int successPercent;
  int notSuccessPercent;

  TestInfo({this.successPercent, this.notSuccessPercent});

  factory TestInfo.fromJson(Map<String, dynamic> json) => TestInfo(
    successPercent: json['success_percent'],
    notSuccessPercent: json['not_success_percent'],
  );

  Map<String, dynamic> toJson() => {
    'success_percent': successPercent,
    'not_success_percent': notSuccessPercent,
  };
}

class TestChart {
  int done;
  int notDone;

  TestChart({this.done, this.notDone});

  factory TestChart.fromJson(Map<String, dynamic> json) => TestChart(
    done: json['done'],
    notDone: json['not_done'],
  );

  Map<String, dynamic> toJson() => {
    'done': done,
    'not_done': notDone,
  };
}

class Part {
  int partId;
  String partName;
  String partImportant;
  List<Exercise> exercises;

  Part({this.partId, this.partName, this.partImportant, this.exercises});

  factory Part.fromJson(Map<String, dynamic> json) => Part(
    partId: json['part_id'],
    partName: json['part_name'],
    partImportant: json['part_important'],
    exercises: (json['exercises'] as List).map((item) => Exercise.fromJson(item)).toList(),
  );

  Map<String, dynamic> toJson() => {
    'part_id': partId,
    'part_name': partName,
    'part_important': partImportant,
    'exercises': exercises,
  };
}

class Exercise {
  int exerciseId;
  String exerciseType;
  String exerciseName;
  String exerciseNo;
  String routeType;
  String exerciseImportant;
  bool exerciseDone;
  bool canDo;
  dynamic workTime;

  Exercise({this.exerciseId, this.exerciseType, this.exerciseName,
      this.exerciseNo, this.routeType, this.exerciseImportant,
      this.exerciseDone, this.canDo, this.workTime});

  factory Exercise.fromJson(Map<String, dynamic> json) => Exercise(
    exerciseId: json['exercise_id'],
    exerciseType: json['exercise_type'],
    exerciseName: json['exercise_name'],
    exerciseNo: json['exercise_no'],
    routeType: json['route_type'],
    exerciseImportant: json['exercise_important'],
    exerciseDone: json['exercise_done'],
    canDo: json['can_do'],
    workTime: json['work_time'],
  );

  Map<String, dynamic> toJson() => {
    'exercise_id': exerciseId,
    'exercise_type': exerciseType,
    'exercise_name': exerciseName,
    'exercise_no': exerciseNo,
    'route_type': routeType,
    'exercise_important': exerciseImportant,
    'exercise_done': exerciseDone,
    'can_do': canDo,
    'work_time': workTime,
  };
}

class Stage {
  int stageId;
  String name;
  String guide;
  List<Question> questions;
  String type;
  List<String> stageAudio;
  int gameLive;

  Stage({this.stageId, this.name, this.guide, this.type, this.questions, this.stageAudio, this.gameLive});

  factory Stage.fromJson(Map<String, dynamic> json) => Stage(
    stageId: json['stage_id'],
    name: json['name'],
    guide: json['guide'],
    type: json['type'],
    questions: (json['questions'] as List).map((item) => Question.fromJson(item)).toList(),
    stageAudio: json['stage_audio'] == null ? List() : ((json['stage_audio'] as List).map((item) => item.toString()).toList()),
    gameLive: json['game_live'],
  );

  Map<String, dynamic> toJson() => {
    'stage_id': stageId,
    'name': name,
    'guide': guide,
    'type': type,
    'questions': questions,
    'stage_audio': stageAudio,
    'game_live': gameLive,
  };
}

class Question {
//  int id;
  String type;
  String questionPicture;
  String questionText;
  String question;
  List<String> questionOption;
  String sayIt;
  String answerText;
  List<String> answers = List<String>();
  String hintText;
  List<String> questionAudio = List();
  String fullQuestion;
  int time;
  List<QuestionMapItem> questionMap;
  List<Map<String, String>> questionGroup;
  List<Map<String, String>> answerGroup;
  bool showHint;
  Map<String, List<String>> mapAnswerGroup = Map();
  List<String> questionOrigin = List<String>();
  List<String> questionTranslate = List<String>();
  List<QuestionFlowItem> questionFlow = List<QuestionFlowItem>();
  String guide;
  double gameTime;
  bool questionTarget;
  int score;

  Question({/*this.id, */this.type, this.questionPicture, this.questionText, this.question, this.questionOption, this.sayIt,
      this.answerText, this.hintText, this.questionAudio, this.fullQuestion, this.time, this.questionMap, this.questionGroup,
      this.answerGroup, this.showHint, this.questionOrigin, this.questionTranslate, this.questionFlow, this.guide, this.gameTime,
      this.questionTarget, this.score});

  factory Question.fromJson(Map<String, dynamic> json) {
    final question = Question(
//      id: json['id'],
      type: json['type'],
      questionPicture: json['question_picture'],
      questionText: json['question_text'],
      question: json['question'],
      questionOption: json['question_option'] == null ? List() : (json['question_option'] as List).map((item) => item.toString()).toList(),
      sayIt: json['say_it'],
      answerText: json['answer_text'],
      hintText: json['hint_text'],
      questionAudio: json['question_audio'] == null ? List() : (json['question_audio'] as List).map((item) => item.toString()).toList(),
      fullQuestion: json['full_question'],
      time: json['time'],
      questionMap: json['question_map'] == null ? List() : (json['question_map'] as List).map((item) => QuestionMapItem.fromJson(item)).toList(),
      questionGroup: json['question_group'] == null ? List() : ((json['question_group'] as List<dynamic>).map((item) {
        var _item = item as Map<String, dynamic>;
        Map<String, String> result = Map();
        result[_item.keys.toList()[0]] =_item.values.toList()[0].toString();
        return result;
      }).toList()),
      showHint: json['show_hint'],
      answerGroup: json['answer_group'] == null ? List() : ((json['answer_group'] as List<dynamic>).map((item) {
        var _item = item as Map<String, dynamic>;
        Map<String, String> result = Map();
        result[_item.keys.toList()[0]] =_item.values.toList()[0].toString();
        return result;
      }).toList()),
      questionOrigin: json['question_origin'] == null ? List() : (json['question_origin'] as List).map((item) => item.toString()).toList(),
      questionTranslate: json['question_translate'] == null ? List() : (json['question_translate'] as List).map((item) => item.toString()).toList(),
      questionFlow: json['question_flow'] == null ? List() : (json['question_flow'] as List).map((item) => QuestionFlowItem.fromJson(item)).toList(),
      guide: json['guide'],
      gameTime: json['game_time'] is int ? (json['game_time'] as int).toDouble() : json['game_time'],
      questionTarget: json['question_target'],
      score: json['score'],
    );
    if(question.answerText != null) {
      question.answers = question.answerText.split('/');
    }
    if(question.answerGroup != null) {
      question.answerGroup.forEach((Map<String, String> ag) {
        List<String> answers = List();
        ag.values.toList()[0].split('/').forEach((val) => answers.add(Utils.normalizeAnswerTypingSub(val.trim().toUpperCase())));
        question.mapAnswerGroup[ag.keys.toList()[0]] = answers;
      });
    }

    return question;
  }

  Map<String, dynamic> toJson() => {
//    'id': id,
    'type': type,
    'question_picture': questionPicture,
    'question_text': questionText,
    'question': question,
    'question_option': questionOption,
    'say_it': sayIt,
    'answer_text': answerText,
    'hint_text': hintText,
    'question_audio': questionAudio,
    'full_question': fullQuestion,
    'time': time,
    'question_map': questionMap,
    'guide': guide,
    'game_time': gameTime,
    'question_target': questionTarget,
    'score': score,
  };
}

class QuestionMapItem {
  int key;
  String question;
  int background;

  QuestionMapItem({this.key, this.question, this.background});

  factory QuestionMapItem.fromJson(Map<String, dynamic> json) => QuestionMapItem(
    key: json['key'],
    question: json['question'],
    background: json['background'],
  );

  Map<String, dynamic> toJson() => {
    'key': key,
    'question': question,
    'background': background,
  };
}

class QuestionFlowItem {
  int key;
  String value;
  int color;

  QuestionFlowItem({this.key, this.value, this.color});

  factory QuestionFlowItem.fromJson(Map<String, dynamic> json) => QuestionFlowItem(
    key: json['key'],
    value: json['value'],
    color: json['color'],
  );

  Map<String, dynamic> toJson() => {
    'key': key,
    'value': value,
    'color': color,
  };
}

class DoExerciseResultDto {
  int stageId;
  int numErrors;
  int timeDo;
  String questionType;
  int gameScore;
  bool gamePass;

  DoExerciseResultDto({this.stageId, this.numErrors, this.timeDo, this.questionType, this.gameScore, this.gamePass});

  factory DoExerciseResultDto.fromJson(Map<String, dynamic> json) => DoExerciseResultDto(
    stageId: json['stage_id'],
    numErrors: json['num_errors'],
    timeDo: json['time_do'],
    questionType: json['question_type'],
    gameScore: json['game_score'],
    gamePass: json['game_pass'],
  );

  Map<String, dynamic> toJson() => {
    'stage_id': stageId,
    'num_errors': numErrors,
    'time_do': timeDo,
    'question_type': questionType,
    'game_score': gameScore,
    'game_pass': gamePass,
  };
}

class DoExerciseResult {
  bool pass;
  String next;
  int numErrors;
  int numAllowed;

  DoExerciseResult({this.pass, this.next, this.numErrors, this.numAllowed});

  factory DoExerciseResult.fromJson(Map<String, dynamic> json) => DoExerciseResult(
    pass: json['pass'],
    next: json['next'],
    numErrors: json['num_errors'],
    numAllowed: json['num_allowed'],
  );

  Map<String, dynamic> toJson() => {
    'pass': pass,
    'next': next,
    'num_errors': numErrors,
    'num_allowed': numAllowed,
  };
}

class Schedule {
  int planId;
  String name;
  String content;
  List<Shift> shifts;
  int userAttendanceShift;
  bool leftSchedule;
  String room;
  String group;

  Schedule({this.planId, this.name, this.content, this.shifts,
    this.userAttendanceShift, this.leftSchedule, this.room, this.group});

  factory Schedule.fromJson(Map<String, dynamic> json) => Schedule(
    planId: json['plan_id'],
    name: json['name'],
    content: json['content'],
    shifts: (json['shifts'] as List).map((item) => Shift.fromJson(item)).toList(),
    userAttendanceShift: json['user_attendance_shift'],
    leftSchedule: json['left-schedule'],
    room: json['room'],
    group: json['group'],
  );

  Map<String, dynamic> toJson() => {
    'plan_id': planId,
    'name': name,
    'content': content,
    'shifts': shifts,
    'user_attendance_shift': userAttendanceShift,
    'left-schedule': leftSchedule,
    'room': room,
    'group': group,
  };
}

class ScheduleLeftResult {
  bool status;

  ScheduleLeftResult({this.status});

  factory ScheduleLeftResult.fromJson(Map<String, dynamic> json) => ScheduleLeftResult(
    status: json['status'],
  );

  Map<String, dynamic> toJson() => {
    'status': status,
  };
}

class Shift {
  int id;
  String name;
  String startTime;
  DateTime dtStartTime;

  Shift({this.id, this.name, this.startTime});

  factory Shift.fromJson(Map<String, dynamic> json) {
    Shift shift = Shift(
        id: json['id'],
        name: json['name'],
        startTime: json['start_time'],
    );
    shift.dtStartTime = json['start_time'] == null ? null : Utils.dateFormat1.parse(json['start_time']);
    return shift;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'start_time': startTime,
  };
}

class Introduce {
  List<UserIntroduce> introduces;
  List<TataCenter> centers;

  Introduce({this.introduces, this.centers});

  factory Introduce.fromJson(Map<String, dynamic> json) => Introduce(
    introduces: json['introduces'] == null ? List<UserIntroduce>():  (json['introduces'] as List).map((item) => UserIntroduce.fromJson(item)).toList(),
    centers: (json['centers'] as List).map((item) => TataCenter.fromJson(item)).toList(),
  );

  Map<String, dynamic> toJson() => {
    'introduces': introduces,
    'centers': centers,
  };
}

class UserIntroduce {
  String phoneNumber;
  String name;
  String status;

  UserIntroduce({this.phoneNumber, this.name, this.status});

  factory UserIntroduce.fromJson(Map<String, dynamic> json) => UserIntroduce(
    phoneNumber: json['phone_number'],
    name: json['name'],
    status: json['status'],
  );

  Map<String, dynamic> toJson() => {
    'phone_number': phoneNumber,
    'name': name,
    'status': status,
  };
}

class TataCenter {
  int id;
  String name;
  String mapUrl;

  TataCenter({this.id, this.name, this.mapUrl});

  factory TataCenter.fromJson(Map<String, dynamic> json) => TataCenter(
    id: json['id'],
    name: json['name'],
    mapUrl: json['map_url'],
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'map_url': mapUrl,
  };
}

class SubmitIntroduceDto {
  String fullName;
  String phoneNumber;
  int centerTarget;
  int job;
  String school;
  String liveArea;
  String introducedText;
  String acceptable;
  String trouble;
  String percentRegis;

  SubmitIntroduceDto({this.fullName, this.phoneNumber, this.centerTarget, this.job, this.school, this.liveArea,
      this.introducedText, this.acceptable, this.trouble, this.percentRegis});

  factory SubmitIntroduceDto.fromJson(Map<String, dynamic> json) => SubmitIntroduceDto(
    fullName: json['full_name'],
    phoneNumber: json['phone_number'],
    centerTarget: json['center_target'],
    job: json['job'],
    school: json['school'],
    liveArea: json['live_area'],
    introducedText: json['introduced-text'],
    acceptable: json['acceptable'],
    trouble: json['trouble'],
    percentRegis: json['percent-regis'],
  );

  Map<String, dynamic> toJson() => {
    'full_name': fullName,
    'phone_number': phoneNumber,
    'center_target': centerTarget,
    'job': job,
    'school': school,
    'live_area': liveArea,
    'introduced-text': introducedText,
    'acceptable': acceptable,
    'trouble': trouble,
    'percent-regis': percentRegis,
  };
}

class SubmitIntroduceResult {
  final int status;
  final String message;
  final String fullName;
  final String phoneNumber;

  SubmitIntroduceResult({this.status, this.message, this.fullName, this.phoneNumber});

  factory SubmitIntroduceResult.fromJson(Map<String, dynamic> json) => SubmitIntroduceResult(
    status: json['status'],
    message: json['message'],
    fullName: json['full_name'],
    phoneNumber: json['phone_number'],
  );

  Map<String, dynamic> toJson() => {
    'status': status,
    'message': message,
    'full_name': fullName,
    'phone_number': phoneNumber,
  };
}