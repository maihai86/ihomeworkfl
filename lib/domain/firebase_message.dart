class FirebaseMessage {
  int from;
  String collapseKey;
  FirebaseNotification notification;

  FirebaseMessage({this.from, this.collapseKey, this.notification});

  factory FirebaseMessage.fromJson(Map<String, dynamic> json) => FirebaseMessage(
    from: json['from'],
    collapseKey: json['collapse_key'],
    notification: json['notification'] == null ? null: FirebaseNotification.fromJson(json['notification']),
  );

  Map<String, dynamic> toJson() => {
    'from': from,
    'collapse_key': collapseKey,
    'notification': notification,
  };
}

class FirebaseNotification {
  String body;
  String title;
  int e;
  String tag;

  FirebaseNotification({this.body, this.title, this.e, this.tag});

  factory FirebaseNotification.fromJson(Map<String, dynamic> json) => FirebaseNotification(
    body: json['body'],
    title: json['title'],
    e: json['e'],
    tag: json['tag'],
  );

  Map<String, dynamic> toJson() => {
    'body': body,
    'title': title,
    'e': e,
    'tag': tag,
  };
}