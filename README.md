# ihomework
# Environtment:
,,,
Flutter 1.5.4-hotfix.2 • channel stable • https://github.com/flutter/flutter.git
Framework • revision 7a4c33425d (5 weeks ago) • 2019-04-29 11:05:24 -0700
Engine • revision 52c7a1e849
Tools • Dart 2.3.0 (build 2.3.0-dev.0.5 a1668566e5)
,,,

iHomework

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

# Speech recognition:
https://github.com/rxlabz/speech_recognition/tree/master/example
change log latest: https://pub.dev/documentation/speech_recognition/latest/
https://github.com/maihai86/speech_recognition (now)
https://stackoverflow.com/questions/13670378/voice-recognition-stops-listening-after-a-few-seconds

# Thu vien widgets:
https://flutter.dev/docs/reference/widgets

Try running your application with "flutter run"

# multiple fragment 1 navigation drawer 1 main screen:
https://github.com/flutter-tuts/drawer_demo

# Row/Column cheat sheet:
https://medium.com/jlouage/flutter-row-column-cheat-sheet-78c38d242041

# donut chart:
https://github.com/xqwzts/flutter_circular_chart

# vector icons
https://github.com/pd4d10/flutter-vector-icons

# launcher icons
https://github.com/fluttercommunity/flutter_launcher_icons

# http package
https://pub.dev/packages/http

# scroll in listview
https://medium.com/@diegoveloper/flutter-lets-know-the-scrollcontroller-and-scrollnotification-652b2685a4ac

# layout cheat sheet
https://medium.com/flutter-community/flutter-layout-cheat-sheet-5363348d037e

# launch web url:
https://pub.dev/packages/url_launcher

# Firebase:
https://firebase.google.com/docs/flutter/setup

https://console.firebase.google.com/project/ihomework-flutter-2/overview

# Firebase message
chưa tích hợp iOS đc do chưa có cert
https://github.com/flutter/plugins/tree/master/packages/firebase_messaging

# flutter sound
https://github.com/dooboolab/flutter_sound

# Animation tutorial:
https://medium.com/flutter-community/flutter-animation-has-never-been-easier-part-1-e378e82b2508

# svg render
https://github.com/dnfield/flutter_svg

# Build:
https://github.com/flutter/flutter/issues/26279

```
Please run "flutter build ios --release" before archiving(It will generate proper Generated.xcconfig for you).
In fact, all those problems are resulted from improper Generated.xcconfig configuration like TRACK_WIDGET_CREATION,FLUTTER_FRAMEWORK_DIR,FLUTTER_BUILD_MODE
In archive mode(flutter's release mode)

FLUTTER_FRAMEWORK_DIR=/Users/kylewong/Codes/Flutter/alibaba-flutter/flutter/bin/cache/artifacts/engine/ios-release #refer to your flutter repo
TRACK_WIDGET_CREATION=true #invalid in release mode.
```

# play mp3
https://github.com/luanpotter/audioplayers
https://github.com/rxlabz/audioplayer
```
If you get a MissingPluginException, try to `flutter build apk` on Android, or `flutter build ios`
to use the plugin in a ObjC iOS project, add 'use_frameworks!' to your podfile cf. example
```

# communication between widgets
https://medium.com/flutter-community/flutter-communication-between-widgets-f5590230df1e
https://github.com/diegoveloper/flutter-samples

# pull to refresh (thu vien Tau)
https://pub.dev/packages/pull_to_refresh

# CD 
https://flutter.dev/docs/deployment/cd
Hướng dẫn build release distribution nhanh: 
```
Running deployment locally
Build the release mode app.
Android: flutter build apk --release.
iOS: flutter build ios --release --no-codesign. No need to sign now since fastlane will sign when archiving.
Run the Fastfile script on each platform.
Android cd android then: fastlane beta.
iOS cd ios then: fastlane beta.
```

# runtime permission (Android)
https://github.com/BaseflowIT/flutter-permission-handler

# pin view
https://pub.dev/packages/pin_view

# release key password keystore: ihomework

# record wav
https://pub.dev/packages/recorder_wav

# webview
https://pub.dev/packages/webview_flutter

